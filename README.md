This file describes steps required for setting up automation suite for CE automation on your local machine.

What is this repository for?
CE Portal Automation
How do I get set up?
Download and install jdk 1.8 and latest version of maven
Set appropriate path and classpath as discribed @ https://www.mkyong.com/maven/how-to-install-maven-in-windows/
Checkout the code base on your machine
Run following commands from the location where pom.xml resides: mvn clean install -DskipTests=true -DCE_AUTOMATION_VERSION=CE_AUTOMATION_1.0.0 mvn idea:clean idea:idea (If you use idea intellij as your IDE) mvn eclipse:clean eclipse:idea (If you use idea intellij as your IDE)
Some files will be created in your root directory
Click on the project file
Who do I talk to?
Shaishav Shah shaishav.s@sigmainfo.net Automation Lead-QA