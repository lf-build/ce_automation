package net.sigmainfo.lf.automation.common;

import com.mongodb.*;
import net.sigmainfo.lf.automation.api.function.ApiPropertiesReader;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.api.config.ApiConfig;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.portal.config.PortalConfig;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.function.PortalPropertiesReader;
import net.sigmainfo.lf.automation.portal.function.UIObjPropertiesReader;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.testng.Assert;
import org.testng.annotations.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by           : Thangaraj on 18-04-2018.
 * Test class           : AbstractTests.java
 * Description          : Drives automation suite and delegates testng annotations
 * Includes             : 1. Setup and quit method for browser opening and closing for portal cases
 *                        2. Initializes test data and test property files
 *                        3. Custom Reporting methods
 */

@ContextConfiguration(classes = {PortalConfig.class, ApiConfig.class})
@TestExecutionListeners(inheritListeners = false, listeners = {
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@WebAppConfiguration
//@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {

    private Logger logger = LoggerFactory.getLogger(AbstractTests.class);

    static Server automationServer = new Server(9096);

    static boolean setupDone = false;
    public static boolean ifFileExist=false;
    public static long auto_start = 0;
    public static String Execution_start_time = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss").format(new Date());
    public static long auto_finish = 0;
    public static String sResBackUp = "res/TestReport.txt";
    public enum browser_list {
        IE, FF, Chrome;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriver driver;
    public static int profile_delay = 20;
    public static int ShortSleep = 20;
    public static File casefile,portal_scenfile,portal_feafile,api_scenfile,api_feafile,feafile;
    String months[] = {"Jan", "Feb", "Mar", "Apr","May", "Jun", "Jul", "Aug", "Sep","Oct", "Nov", "Dec"};

    @Autowired
    ApiParam apiParam;

    @Autowired
    ApiPropertiesReader apiPropertiesReader;

    @Autowired
    public
    PortalParam portalParam;

    @Autowired
    public
    UIObjParam uiObjParam;

    @Autowired
    UIObjPropertiesReader uiPropertiesReader;

    @Autowired
    public
    PortalFuncUtils portalFuncUtils;

    @Autowired
    PortalPropertiesReader portalPropertiesReader;

    @BeforeSuite(alwaysRun = true)
    public void setUpOnce() throws Exception {
        logger.info("======================== Before Suite Invoked ==============================");
        setupJetty("CE-automation", automationServer);
        logger.info("Jetty Server Started..........");
    }



    @AfterSuite(alwaysRun = true)
    public void tearDown() throws Exception {
        logger.info("======================== After Suite Invoked. ==============================");
        automationServer.getServer().stop();
        logger.info("Jetty Server Stopped..........");
    }

    @PostConstruct
    private void postConstruct() throws SQLException,Exception {
        if (!setupDone) {
            logger.info("======================== Post Construct Invoked. ==============================");
            setupDBParams();
            setupUIObjects();
            setupDone = true;
        }
    }

    public void setupUIObjects() {
        logger.info("--------------- READING UI OBJECTS ----------------");
        uiObjParam.backoffice_loginPage_usernameTextBox = uiPropertiesReader.getBackoffice_loginPage_usernameTextBox();
        logger.info("uiObjParam.backoffice_loginPage_usernameTextBox : "+uiObjParam.backoffice_loginPage_usernameTextBox);
        uiObjParam.backoffice_loginPage_passwordTextBox = uiPropertiesReader.getBackoffice_loginPage_passwordTextBox();
        logger.info("uiObjParam.backoffice_loginPage_passwordTextBox : "+uiObjParam.backoffice_loginPage_passwordTextBox);
        uiObjParam.backoffice_loginPage_loginButton = uiPropertiesReader.getBackoffice_loginPage_loginButton();
        logger.info("uiObjParam.backoffice_loginPage_loginButton : "+uiObjParam.backoffice_loginPage_loginButton);
        uiObjParam.borrower_loanAmountpage_howMuchDoYouNeedLabel = uiPropertiesReader.getBorrower_loanAmountpage_howMuchDoYouNeedLabel();
        logger.info("uiObjParam.borrower_loanAmountpage_howMuchDoYouNeedLabel : "+uiObjParam.borrower_loanAmountpage_howMuchDoYouNeedLabel);
        uiObjParam.borrower_loanAmountpage_loanDetailsMenu = uiPropertiesReader.getBorrower_loanAmountpage_loanDetailsMenu();
        logger.info("uiObjParam.borrower_loanAmountpage_loanDetailsMenu : "+uiObjParam.borrower_loanAmountpage_loanDetailsMenu);
        uiObjParam.borrower_loanAmountpage_nextButton = uiPropertiesReader.getBorrower_loanAmountpage_nextButton();
        logger.info("uiObjParam.borrower_loanAmountpage_nextButton : "+uiObjParam.borrower_loanAmountpage_nextButton);
        uiObjParam.borrower_loanAmountpage_amountTextBox = uiPropertiesReader.getBorrower_loanAmountpage_amountTextBox();
        logger.info("uiObjParam.borrower_loanAmountpage_amountTextBox : "+uiObjParam.borrower_loanAmountpage_amountTextBox);
        uiObjParam.borrower_loanPurposepage_needPurposeLabel = uiPropertiesReader.getBorrower_loanPurposepage_needPurposeLabel();
        logger.info("uiObjParam.borrower_loanPurposepage_needPurposeLabel : "+uiObjParam.borrower_loanPurposepage_needPurposeLabel);
        uiObjParam.borrower_loanPurposepage_travelButton = uiPropertiesReader.getBorrower_loanPurposepage_travelButton();
        logger.info("uiObjParam.borrower_loanPurposepage_travelButton : "+uiObjParam.borrower_loanPurposepage_travelButton);
        uiObjParam.borrower_loanPurposepage_vehiclePurposeButton = uiPropertiesReader.getBorrower_loanPurposepage_vehiclePurposeButton();
        logger.info("uiObjParam.borrower_loanPurposepage_vehiclePurposeButton : "+uiObjParam.borrower_loanPurposepage_vehiclePurposeButton);
        uiObjParam.borrower_loanPurposepage_medicalButton = uiPropertiesReader.getBorrower_loanPurposepage_medicalButton();
        logger.info("uiObjParam.borrower_loanPurposepage_medicalButton : "+uiObjParam.borrower_loanPurposepage_medicalButton);
        uiObjParam.borrower_loanPurposepage_loanRefinanceButton = uiPropertiesReader.getBorrower_loanPurposepage_loanRefinanceButton();
        logger.info("uiObjParam.borrower_loanPurposepage_loanRefinanceButton : "+uiObjParam.borrower_loanPurposepage_loanRefinanceButton);
        uiObjParam.borrower_loanPurposepage_weddingButton = uiPropertiesReader.getBorrower_loanPurposepage_weddingButton();
        logger.info("uiObjParam.borrower_loanPurposepage_weddingButton : "+uiObjParam.borrower_loanPurposepage_weddingButton);
        uiObjParam.borrower_loanPurposepage_homeImprovementButton = uiPropertiesReader.getBorrower_loanPurposepage_homeImprovementButton();
        logger.info("uiObjParam.borrower_loanPurposepage_homeImprovementButton : "+uiObjParam.borrower_loanPurposepage_homeImprovementButton);
        uiObjParam.borrower_loanPurposepage_nextButton = uiPropertiesReader.getBorrower_loanPurposepage_nextButton();
        logger.info("uiObjParam.borrower_loanPurposepage_nextButton : "+uiObjParam.borrower_loanPurposepage_nextButton);
        uiObjParam.borrower_profilePage_personalInfoLabel = uiPropertiesReader.getBorrower_profilePage_personalInfoLabel();
        logger.info("uiObjParam.borrower_profilePage_personalInfoLabel : "+uiObjParam.borrower_profilePage_personalInfoLabel);
        uiObjParam.borrower_profilePage_mrTitleButton = uiPropertiesReader.getBorrower_profilePage_mrTitleButton();
        logger.info("uiObjParam.borrower_profilePage_mrTitleButton : "+uiObjParam.borrower_profilePage_mrTitleButton);
        uiObjParam.borrower_profilePage_msTitleButton = uiPropertiesReader.getBorrower_profilePage_msTitleButton();
        logger.info("uiObjParam.borrower_profilePage_msTitleButton : "+uiObjParam.borrower_profilePage_msTitleButton);
        uiObjParam.borrower_profilePage_mrsTitleButton = uiPropertiesReader.getBorrower_profilePage_mrsTitleButton();
        logger.info("uiObjParam.borrower_profilePage_mrsTitleButton : "+uiObjParam.borrower_profilePage_mrsTitleButton);
        uiObjParam.borrower_profilePage_maritalStatusSingleButton = uiPropertiesReader.getBorrower_profilePage_maritalStatusSingleButton();
        logger.info("uiObjParam.borrower_profilePage_maritalStatusSingleButton : "+uiObjParam.borrower_profilePage_maritalStatusSingleButton);
        uiObjParam.borrower_profilePage_maritalStatusMarriedButton = uiPropertiesReader.getBorrower_profilePage_maritalStatusMarriedButton();
        logger.info("uiObjParam.borrower_profilePage_maritalStatusMarriedButton : "+uiObjParam.borrower_profilePage_maritalStatusMarriedButton);
        uiObjParam.borrower_profilePage_firstNameTextBox = uiPropertiesReader.getBorrower_profilePage_firstNameTextBox();
        logger.info("uiObjParam.borrower_profilePage_firstNameTextBox : "+uiObjParam.borrower_profilePage_firstNameTextBox);
        uiObjParam.borrower_profilePage_middleNameTextBox = uiPropertiesReader.getBorrower_profilePage_middleNameTextBox();
        logger.info("uiObjParam.borrower_profilePage_middleNameTextBox : "+uiObjParam.borrower_profilePage_middleNameTextBox);
        uiObjParam.borrower_profilePage_lastNameTextBox = uiPropertiesReader.getBorrower_profilePage_lastNameTextBox();
        logger.info("uiObjParam.borrower_profilePage_lastNameTextBox : "+uiObjParam.borrower_profilePage_lastNameTextBox);
        uiObjParam.borrower_profilePage_emailIdTextBox = uiPropertiesReader.getBorrower_profilePage_emailIdTextBox();
        logger.info("uiObjParam.borrower_profilePage_emailIdTextBox : "+uiObjParam.borrower_profilePage_emailIdTextBox);
        uiObjParam.borrower_profilePage_nextButton = uiPropertiesReader.getBorrower_profilePage_nextButton();
        logger.info("uiObjParam.borrower_profilePage_nextButton : "+uiObjParam.borrower_profilePage_nextButton);
        uiObjParam.borrower_accountCreationpage_mobileVerificationLabel = uiPropertiesReader.getBorrower_accountCreationpage_mobileVerificationLabel();
        logger.info("uiObjParam.borrower_accountCreationpage_mobileVerificationLabel : "+uiObjParam.borrower_accountCreationpage_mobileVerificationLabel);
        uiObjParam.borrower_accountCreationpage_mobileNumberTextBox = uiPropertiesReader.getBorrower_accountCreationpage_mobileNumberTextBox();
        logger.info("uiObjParam.borrower_accountCreationpage_mobileNumberTextBox : "+uiObjParam.borrower_accountCreationpage_mobileNumberTextBox);
        uiObjParam.borrower_accountCreationpage_nextButton = uiPropertiesReader.getBorrower_accountCreationpage_nextButton();
        logger.info("uiObjParam.borrower_accountCreationpage_nextButton : "+uiObjParam.borrower_accountCreationpage_nextButton);
        uiObjParam.borrower_accountCreationpage_otpSentLabel = uiPropertiesReader.getBorrower_accountCreationpage_otpSentLabel();
        logger.info("uiObjParam.borrower_accountCreationpage_otpSentLabel : "+uiObjParam.borrower_accountCreationpage_otpSentLabel);
        uiObjParam.borrower_accountCreationpage_otpTextBox = uiPropertiesReader.getBorrower_accountCreationpage_otpTextBox();
        logger.info("uiObjParam.borrower_accountCreationpage_otpTextBox : "+uiObjParam.borrower_accountCreationpage_otpTextBox);
        uiObjParam.borrower_accountCreationpage_otpNextButton = uiPropertiesReader.getBorrower_accountCreationpage_otpNextButton();
        logger.info("uiObjParam.borrower_accountCreationpage_otpNextButton : "+uiObjParam.borrower_accountCreationpage_otpNextButton);
        uiObjParam.borrower_employmentDetailspage_legalBusinessTextBox = uiPropertiesReader.getBorrower_employmentDetailspage_legalBusinessTextBox();
        logger.info("uiObjParam.borrower_employmentDetailspage_legalBusinessTextBox : "+uiObjParam.borrower_employmentDetailspage_legalBusinessTextBox);
        uiObjParam.borrower_employmentDetailspage_takeHomeSalaryTextBox = uiPropertiesReader.getBorrower_employmentDetailspage_takeHomeSalaryTextBox();
        logger.info("uiObjParam.borrower_employmentDetailspage_takeHomeSalaryTextBox : "+uiObjParam.borrower_employmentDetailspage_takeHomeSalaryTextBox);
        uiObjParam.borrower_employmentDetailspage_officialEmailIdTextBox = uiPropertiesReader.getBorrower_employmentDetailspage_officialEmailIdTextBox();
        logger.info("uiObjParam.borrower_employmentDetailspage_officialEmailIdTextBox : "+uiObjParam.borrower_employmentDetailspage_officialEmailIdTextBox);
        uiObjParam.borrower_employmentDetailspage_nextButton = uiPropertiesReader.getBorrower_employmentDetailspage_nextButton();
        logger.info("uiObjParam.borrower_employmentDetailspage_nextButton : "+uiObjParam.borrower_employmentDetailspage_nextButton);
        uiObjParam.borrower_identificationDetails_dateDropdown = uiPropertiesReader.getBorrower_identificationDetails_dateDropdown();
        logger.info("uiObjParam.borrower_identificationDetails_dateDropdown : "+uiObjParam.borrower_identificationDetails_dateDropdown);
        uiObjParam.borrower_identificationDetails_monthDropdown = uiPropertiesReader.getBorrower_identificationDetails_monthDropdown();
        logger.info("uiObjParam.borrower_identificationDetails_monthDropdown : "+uiObjParam.borrower_identificationDetails_monthDropdown);
        uiObjParam.borrower_identificationDetails_yearDropdown = uiPropertiesReader.getBorrower_identificationDetails_yearDropdown();
        logger.info("uiObjParam.borrower_identificationDetails_yearDropdown : "+uiObjParam.borrower_identificationDetails_yearDropdown);
        uiObjParam.borrower_identificationDetails_idDetailsLabel = uiPropertiesReader.getBorrower_identificationDetails_idDetailsLabel();
        logger.info("uiObjParam.borrower_identificationDetails_idDetailsLabel : "+uiObjParam.borrower_identificationDetails_idDetailsLabel);
        uiObjParam.borrower_identificationDetails_aadharNumberTextBox = uiPropertiesReader.getBorrower_identificationDetails_aadharNumberTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_aadharNumberTextBox : "+uiObjParam.borrower_identificationDetails_aadharNumberTextBox);
        uiObjParam.borrower_identificationDetails_panNumberTextBox = uiPropertiesReader.getBorrower_identificationDetails_panNumberTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_panNumberTextBox : "+uiObjParam.borrower_identificationDetails_panNumberTextBox);
        uiObjParam.borrower_identificationDetails_address1TextBox = uiPropertiesReader.getBorrower_identificationDetails_address1TextBox();
        logger.info("uiObjParam.borrower_identificationDetails_address1TextBox : "+uiObjParam.borrower_identificationDetails_address1TextBox);
        uiObjParam.borrower_identificationDetails_address2TextBox = uiPropertiesReader.getBorrower_identificationDetails_address2TextBox();
        logger.info("uiObjParam.borrower_identificationDetails_address2TextBox : "+uiObjParam.borrower_identificationDetails_address2TextBox);
        uiObjParam.borrower_identificationDetails_localityTextBox = uiPropertiesReader.getBorrower_identificationDetails_localityTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_localityTextBox : "+uiObjParam.borrower_identificationDetails_localityTextBox);
        uiObjParam.borrower_identificationDetails_pincodeTextBox = uiPropertiesReader.getBorrower_identificationDetails_pincodeTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_pincodeTextBox : "+uiObjParam.borrower_identificationDetails_pincodeTextBox);
        uiObjParam.borrower_identificationDetails_cityTextBox = uiPropertiesReader.getBorrower_identificationDetails_cityTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_cityTextBox : "+uiObjParam.borrower_identificationDetails_cityTextBox);
        uiObjParam.borrower_identificationDetails_stateTextBox = uiPropertiesReader.getBorrower_identificationDetails_stateTextBox();
        logger.info("uiObjParam.borrower_identificationDetails_stateTextBox : "+uiObjParam.borrower_identificationDetails_stateTextBox);
        uiObjParam.borrower_identificationDetails_bothAddressSameButton = uiPropertiesReader.getBorrower_identificationDetails_bothAddressSameButton();
        logger.info("uiObjParam.borrower_identificationDetails_bothAddressSameButton : "+uiObjParam.borrower_identificationDetails_bothAddressSameButton);
        uiObjParam.borrower_identificationDetails_bothAddressDifferentButton = uiPropertiesReader.getBorrower_identificationDetails_bothAddressDifferentButton();
        logger.info("uiObjParam.borrower_identificationDetails_bothAddressDifferentButton : "+uiObjParam.borrower_identificationDetails_bothAddressDifferentButton);
        uiObjParam.borrower_identificationDetails_nextButton = uiPropertiesReader.getBorrower_identificationDetails_nextButton();
        logger.info("uiObjParam.borrower_identificationDetails_nextButton : "+uiObjParam.borrower_identificationDetails_nextButton);
        uiObjParam.borrower_reviewPage_reviewApplicationLabel = uiPropertiesReader.getBorrower_reviewPage_reviewApplicationLabel();
        logger.info("uiObjParam.borrower_reviewPage_reviewApplicationLabel : "+uiObjParam.borrower_reviewPage_reviewApplicationLabel);
        uiObjParam.borrower_reviewPage_nextButton = uiPropertiesReader.getBorrower_reviewPage_nextButton();
        logger.info("uiObjParam.borrower_reviewPage_nextButton : "+uiObjParam.borrower_reviewPage_nextButton);
        uiObjParam.borrower_reviewPage_agreementCheckbox = uiPropertiesReader.getBorrower_reviewPage_agreementCheckbox();
        logger.info("uiObjParam.borrower_reviewPage_agreementCheckbox : "+uiObjParam.borrower_reviewPage_agreementCheckbox);
        uiObjParam.borrower_employmentDetailspage_whatAboutWorkLabel = uiPropertiesReader.getBorrower_employmentDetailspage_whatAboutWorkLabel();
        logger.info("uiObjParam.borrower_employmentDetailspage_whatAboutWorkLabel : "+uiObjParam.borrower_employmentDetailspage_whatAboutWorkLabel);
        uiObjParam.borrower_expensesPage_residenseTypeDropdown = uiPropertiesReader.getBorrower_expensesPage_residenseTypeDropdown();
        logger.info("uiObjParam.borrower_expensesPage_residenseTypeDropdown : "+uiObjParam.borrower_expensesPage_residenseTypeDropdown);
        uiObjParam.borrower_expensesPage_nextButton = uiPropertiesReader.getBorrower_expensesPage_nextButton();
        logger.info("uiObjParam.borrower_expensesPage_nextButton : "+uiObjParam.borrower_expensesPage_nextButton);
        uiObjParam.borrower_expensesPage_expensesLabel = uiPropertiesReader.getBorrower_expensesPage_expensesLabel();
        logger.info("uiObjParam.borrower_expensesPage_expensesLabel : "+uiObjParam.borrower_expensesPage_expensesLabel);
        uiObjParam.borrower_eligibilityPage_greetLabel = uiPropertiesReader.getBorrower_eligibilityPage_greetLabel();
        logger.info("uiObjParam.borrower_eligibilityPage_greetLabel : "+uiObjParam.borrower_eligibilityPage_greetLabel);
        uiObjParam.borrower_eligibilityPage_applicationNumberLabel = uiPropertiesReader.getBorrower_eligibilityPage_applicationNumberLabel();
        logger.info("uiObjParam.borrower_eligibilityPage_applicationNumberLabel : "+uiObjParam.borrower_eligibilityPage_applicationNumberLabel);
        uiObjParam.borrower_eligibilityPage_nextButton = uiPropertiesReader.getBorrower_eligibilityPage_nextButton();
        logger.info("uiObjParam.borrower_eligibilityPage_nextButton : "+uiObjParam.borrower_eligibilityPage_nextButton);
        uiObjParam.borrower_bankVerificationPage_connectAccountRadioButton = uiPropertiesReader.getBorrower_bankVerificationPage_connectAccountRadioButton();
        logger.info("uiObjParam.borrower_bankVerificationPage_connectAccountRadioButton : "+uiObjParam.borrower_bankVerificationPage_connectAccountRadioButton);
        uiObjParam.borrower_bankVerificationPage_uploadFileRadioButton = uiPropertiesReader.getBorrower_bankVerificationPage_uploadFileRadioButton();
        logger.info("uiObjParam.borrower_bankVerificationPage_uploadFileRadioButton : "+uiObjParam.borrower_bankVerificationPage_uploadFileRadioButton);
        uiObjParam.borrower_bankVerificationPage_continueButton = uiPropertiesReader.getBorrower_bankVerificationPage_continueButton();
        logger.info("uiObjParam.borrower_bankVerificationPage_continueButton : "+uiObjParam.borrower_bankVerificationPage_continueButton);
        uiObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox = uiPropertiesReader.getBorrower_bankVerificationPage_uploadBankStatement_bankTextBox();
        logger.info("uiObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox : "+uiObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox);
        uiObjParam.borrower_bankVerificationPage_uploadBankStatement_uploadFileArea = uiPropertiesReader.getBorrower_bankVerificationPage_uploadBankStatement_uploadFileArea();
        logger.info("uiObjParam.borrower_bankVerificationPage_uploadBankStatement_uploadFileArea : "+uiObjParam.borrower_bankVerificationPage_uploadBankStatement_uploadFileArea);
        uiObjParam.borrower_bankVerificationPage_uploadBankStatement_nextButton = uiPropertiesReader.getBorrower_bankVerificationPage_uploadBankStatement_nextButton();
        logger.info("uiObjParam.borrower_bankVerificationPage_uploadBankStatement_nextButton : "+uiObjParam.borrower_bankVerificationPage_uploadBankStatement_nextButton);
        uiObjParam.borrower_educationPage_educationLevelDropdown = uiPropertiesReader.getBorrower_educationPage_educationLevelDropdown();
        logger.info("uiObjParam.borrower_educationPage_educationLevelDropdown : "+uiObjParam.borrower_educationPage_educationLevelDropdown);
        uiObjParam.borrower_educationPage_educationalInstituteTextBox = uiPropertiesReader.getBorrower_educationPage_educationalInstituteTextBox();
        logger.info("uiObjParam.borrower_educationPage_educationalInstituteTextBox : "+uiObjParam.borrower_educationPage_educationalInstituteTextBox);
        uiObjParam.borrower_educationPage_nextButton = uiPropertiesReader.getBorrower_educationPage_nextButton();
        logger.info("uiObjParam.borrower_educationPage_nextButton : "+uiObjParam.borrower_educationPage_nextButton);
        uiObjParam.borrower_employmentPage_employerName = uiPropertiesReader.getBorrower_employmentPage_employerName();
        logger.info("uiObjParam.borrower_employmentPage_employerName : "+uiObjParam.borrower_employmentPage_employerName);
        uiObjParam.borrower_employmentPage_designationTextBox = uiPropertiesReader.getBorrower_employmentPage_designationTextBox();
        logger.info("uiObjParam.borrower_employmentPage_designationTextBox : "+uiObjParam.borrower_employmentPage_designationTextBox);
        uiObjParam.borrower_employmentPage_workExperienceDropdown = uiPropertiesReader.getBorrower_employmentPage_workExperienceDropdown();
        logger.info("uiObjParam.borrower_employmentPage_workExperienceDropdown : "+uiObjParam.borrower_employmentPage_workExperienceDropdown);
        uiObjParam.borrower_employmentPage_address1TextBox = uiPropertiesReader.getBorrower_employmentPage_address1TextBox();
        logger.info("uiObjParam.borrower_employmentPage_address1TextBox : "+uiObjParam.borrower_employmentPage_address1TextBox);
        uiObjParam.borrower_employmentPage_localityTextBox = uiPropertiesReader.getBorrower_employmentPage_localityTextBox();
        logger.info("uiObjParam.borrower_employmentPage_localityTextBox : "+uiObjParam.borrower_employmentPage_localityTextBox);
        uiObjParam.borrower_employmentPage_pincodeTextBox = uiPropertiesReader.getBorrower_employmentPage_pincodeTextBox();
        logger.info("uiObjParam.borrower_employmentPage_pincodeTextBox : "+uiObjParam.borrower_employmentPage_pincodeTextBox);
        uiObjParam.borrower_employmentPage_nextButton = uiPropertiesReader.getBorrower_employmentPage_nextButton();
        logger.info("uiObjParam.borrower_employmentPage_nextButton : "+uiObjParam.borrower_employmentPage_nextButton);
        uiObjParam.borrower_socialPage_skipButton = uiPropertiesReader.getBorrower_socialPage_skipButton();
        logger.info("uiObjParam.borrower_socialPage_skipButton : "+uiObjParam.borrower_socialPage_skipButton);
        uiObjParam.borrower_appComplete_message = uiPropertiesReader.getBorrower_appComplete_message();
        logger.info("uiObjParam.borrower_appComplete_message : "+uiObjParam.borrower_appComplete_message);
        uiObjParam.borrower_appComplete_getUniqueReferralButton = uiPropertiesReader.getBorrower_appComplete_getUniqueReferralButton();
        logger.info("uiObjParam.borrower_appComplete_getUniqueReferralButton : "+uiObjParam.borrower_appComplete_getUniqueReferralButton);
        uiObjParam.borrower_logoutLink = uiPropertiesReader.getBorrower_logoutLink();
        logger.info("uiObjParam.borrower_logoutLink : "+uiObjParam.borrower_logoutLink);
        uiObjParam.borrower_socialPage_verifyMeImage = uiPropertiesReader.getBorrower_socialPage_verifyMeImage();
        logger.info("uiObjParam.borrower_socialPage_verifyMeImage : "+uiObjParam.borrower_socialPage_verifyMeImage);

        uiObjParam.backoffice_searchPage_profileCircle = uiPropertiesReader.getBackoffice_searchPage_profileCircle();
        logger.info("uiObjParam.backoffice_searchPage_profileCircle : "+uiObjParam.backoffice_searchPage_profileCircle);
        uiObjParam.backoffice_searchPage_applicationNumberTextBox = uiPropertiesReader.getBackoffice_searchPage_applicationNumberTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicationNumberTextBox : "+uiObjParam.backoffice_searchPage_applicationNumberTextBox);
        uiObjParam.backoffice_searchPage_searchButton = uiPropertiesReader.getBackoffice_searchPage_searchButton();
        logger.info("uiObjParam.backoffice_searchPage_searchButton : "+uiObjParam.backoffice_searchPage_searchButton);
        uiObjParam.backoffice_searchPage_resultData_appId = uiPropertiesReader.getBackoffice_searchPage_resultData_appId();
        logger.info("uiObjParam.backoffice_searchPage_resultData_appId : "+uiObjParam.backoffice_searchPage_resultData_appId);
        uiObjParam.backoffice_searchPage_resultData_appDate = uiPropertiesReader.getBackoffice_searchPage_resultData_appDate();
        logger.info("uiObjParam.backoffice_searchPage_resultData_appDate : "+uiObjParam.backoffice_searchPage_resultData_appDate);
        uiObjParam.backoffice_searchPage_resultData_applicantName = uiPropertiesReader.getBackoffice_searchPage_resultData_applicantName();
        logger.info("uiObjParam.backoffice_searchPage_resultData_applicantName : "+uiObjParam.backoffice_searchPage_resultData_applicantName);
        uiObjParam.backoffice_searchPage_resultData_phoneNumber = uiPropertiesReader.getBackoffice_searchPage_resultData_phoneNumber();
        logger.info("uiObjParam.backoffice_searchPage_resultData_phoneNumber : "+uiObjParam.backoffice_searchPage_resultData_phoneNumber);
        uiObjParam.backoffice_searchPage_resultData_loanAmount = uiPropertiesReader.getBackoffice_searchPage_resultData_loanAmount();
        logger.info("uiObjParam.backoffice_searchPage_resultData_loanAmount : "+uiObjParam.backoffice_searchPage_resultData_loanAmount);
        uiObjParam.backoffice_searchPage_resultData_employerName = uiPropertiesReader.getBackoffice_searchPage_resultData_employerName();
        logger.info("uiObjParam.backoffice_searchPage_resultData_employerName : "+uiObjParam.backoffice_searchPage_resultData_employerName);
        uiObjParam.backoffice_searchPage_resultData_expiryDate = uiPropertiesReader.getBackoffice_searchPage_resultData_expiryDate();
        logger.info("uiObjParam.backoffice_searchPage_resultData_expiryDate : "+uiObjParam.backoffice_searchPage_resultData_expiryDate);
        uiObjParam.backoffice_searchPage_resultData_source = uiPropertiesReader.getBackoffice_searchPage_resultData_source();
        logger.info("uiObjParam.backoffice_searchPage_resultData_source : "+uiObjParam.backoffice_searchPage_resultData_source);
        uiObjParam.backoffice_searchPage_resultData_detailsButton = uiPropertiesReader.getBackoffice_searchPage_resultData_detailsButton();
        logger.info("uiObjParam.backoffice_searchPage_resultData_detailsButton : "+uiObjParam.backoffice_searchPage_resultData_detailsButton);
        uiObjParam.backoffice_appDetailsPage_headerStatus = uiPropertiesReader.getBackoffice_appDetailsPage_headerStatus();
        logger.info("uiObjParam.backoffice_appDetailsPage_headerStatus : "+uiObjParam.backoffice_appDetailsPage_headerStatus);
        uiObjParam.backoffice_appDetailsPage_verificationDashboardTab = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboardTab();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboardTab : "+uiObjParam.backoffice_appDetailsPage_verificationDashboardTab);
        uiObjParam.backoffice_appDetailsPage_actionButton = uiPropertiesReader.getBackoffice_appDetailsPage_actionButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actionButton : "+uiObjParam.backoffice_appDetailsPage_actionButton);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOfferButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOfferButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOfferButton : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOfferButton);
        uiObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_presentFinalOfferButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton : "+uiObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_termDropdown();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_emiTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_addManualOffer_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_submitButton : "+uiObjParam.backoffice_appDetailsPage_actions_addManualOffer_submitButton);
        uiObjParam.backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton : "+uiObjParam.backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton);
        uiObjParam.backoffice_searchPage_profile_logoutButton = uiPropertiesReader.getBackoffice_searchPage_profile_logoutButton();
        logger.info("uiObjParam.backoffice_searchPage_profile_logoutButton : "+uiObjParam.backoffice_searchPage_profile_logoutButton);
        uiObjParam.borrower_loginPage_mobileNumberTextBox = uiPropertiesReader.getBorrower_loginPage_mobileNumberTextBox();
        logger.info("uiObjParam.borrower_loginPage_mobileNumberTextBox : "+uiObjParam.borrower_loginPage_mobileNumberTextBox);
        uiObjParam.borrower_loginPage_otpTextBox = uiPropertiesReader.getBorrower_loginPage_otpTextBox();
        logger.info("uiObjParam.borrower_loginPage_otpTextBox : "+uiObjParam.borrower_loginPage_otpTextBox);
        uiObjParam.borrower_finalOfferPage_loanRadioButton = uiPropertiesReader.getBorrower_finalOfferPage_loanRadioButton();
        logger.info("uiObjParam.borrower_finalOfferPage_loanRadioButton : "+uiObjParam.borrower_finalOfferPage_loanRadioButton);
        uiObjParam.borrower_finalOfferPage_loanAmountLabel = uiPropertiesReader.getBorrower_finalOfferPage_loanAmountLabel();
        logger.info("uiObjParam.borrower_finalOfferPage_loanAmountLabel : "+uiObjParam.borrower_finalOfferPage_loanAmountLabel);
        uiObjParam.borrower_finalOfferPage_durationLabel = uiPropertiesReader.getBorrower_finalOfferPage_durationLabel();
        logger.info("uiObjParam.borrower_finalOfferPage_durationLabel : "+uiObjParam.borrower_finalOfferPage_durationLabel);
        uiObjParam.borrower_finalOfferPage_emiLabel = uiPropertiesReader.getBorrower_finalOfferPage_emiLabel();
        logger.info("uiObjParam.borrower_finalOfferPage_emiLabel : "+uiObjParam.borrower_finalOfferPage_emiLabel);
        uiObjParam.borrower_finalOfferPage_processingFeeLabel = uiPropertiesReader.getBorrower_finalOfferPage_processingFeeLabel();
        logger.info("uiObjParam.borrower_finalOfferPage_processingFeeLabel : "+uiObjParam.borrower_finalOfferPage_processingFeeLabel);
        uiObjParam.borrower_finalOfferPage_nextButton = uiPropertiesReader.getBorrower_finalOfferPage_nextButton();
        logger.info("uiObjParam.borrower_finalOfferPage_nextButton : "+uiObjParam.borrower_finalOfferPage_nextButton);
        uiObjParam.borrower_finalOfferPage_appReceivedMsg = uiPropertiesReader.getBorrower_finalOfferPage_appReceivedMsg();
        logger.info("uiObjParam.borrower_finalOfferPage_appReceivedMsg : "+uiObjParam.borrower_finalOfferPage_appReceivedMsg);
        uiObjParam.borrower_loginPage_sendOtpLink = uiPropertiesReader.getBorrower_loginPage_sendOtpLink();
        logger.info("uiObjParam.borrower_loginPage_sendOtpLink : "+uiObjParam.borrower_loginPage_sendOtpLink);
        uiObjParam.borrower_loginPage_signInButton = uiPropertiesReader.getBorrower_loginPage_signInButton();
        logger.info("uiObjParam.borrower_loginPage_signInButton : "+uiObjParam.borrower_loginPage_signInButton);
        uiObjParam.borrower_socialPage_nextButton = uiPropertiesReader.getBorrower_socialPage_nextButton();
        logger.info("uiObjParam.borrower_socialPage_nextButton : "+uiObjParam.borrower_socialPage_nextButton);
        uiObjParam.borrower_socialPage_verificationDoneMsg = uiPropertiesReader.getBorrower_socialPage_verificationDoneMsg();
        logger.info("uiObjParam.borrower_socialPage_verificationDoneMsg : "+uiObjParam.borrower_socialPage_verificationDoneMsg);
        uiObjParam.borrower_loginPage_loginButton = uiPropertiesReader.getBorrower_loginPage_loginButton();
        logger.info("uiObjParam.borrower_loginPage_loginButton : "+uiObjParam.borrower_loginPage_loginButton);
        uiObjParam.borrower_finalOfferPage_readyForDisbursementMsg = uiPropertiesReader.getBorrower_finalOfferPage_readyForDisbursementMsg();
        logger.info("uiObjParam.borrower_finalOfferPage_readyForDisbursementMsg : "+uiObjParam.borrower_finalOfferPage_readyForDisbursementMsg);
        uiObjParam.backoffice_appDetailsPage_personalInfoTab_personalInfoLabel = uiPropertiesReader.getBackoffice_appDetailsPage_personalInfoTab_personalInfoLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_personalInfoTab_personalInfoLabel : "+uiObjParam.backoffice_appDetailsPage_personalInfoTab_personalInfoLabel);
        uiObjParam.backoffice_actionCenter = uiPropertiesReader.getBackoffice_actionCenter();
        logger.info("uiObjParam.backoffice_actionCenter : "+uiObjParam.backoffice_actionCenter);
        uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument = uiPropertiesReader.getBackoffice_appDetailsPage_actions_uploadPendingDocument();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument : "+uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument);
        uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_uploadPendingDocument_closeButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton : "+uiObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_screenshot();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_panDetails_closeButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton);


        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton);
        uiObjParam.backoffice_appDetailsPage_bottomTabs_documents = uiPropertiesReader.getBackoffice_appDetailsPage_bottomTabs_documents();
        logger.info("uiObjParam.backoffice_appDetailsPage_bottomTabs_documents : "+uiObjParam.backoffice_appDetailsPage_bottomTabs_documents);
        uiObjParam.backoffice_appDetailsPage_bottomTabs_documents_documentsLabel = uiPropertiesReader.getBackoffice_appDetailsPage_bottomTabs_documents_documentsLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_bottomTabs_documents_documentsLabel : "+uiObjParam.backoffice_appDetailsPage_bottomTabs_documents_documentsLabel);
        uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox : "+uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox);
        uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_updateBankDetails_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_submitButton : "+uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails_submitButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox);
        uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton = uiPropertiesReader.getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton : "+uiObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton);
        uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails = uiPropertiesReader.getBackoffice_appDetailsPage_actions_updateBankDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails : "+uiObjParam.backoffice_appDetailsPage_actions_updateBankDetails);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_submitButton : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_submitButton);
        uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup = uiPropertiesReader.getBackoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup : "+uiObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup);

        uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail = uiPropertiesReader.getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail : "+uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail);
        uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown = uiPropertiesReader.getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown : "+uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown);
        uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox : "+uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox);
        uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton : "+uiObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton);
        uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation = uiPropertiesReader.getBackoffice_appDetailsPage_action_addFOIRCalculation();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation : "+uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation);
        uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox : "+uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox);
        uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox : "+uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox);
        uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox = uiPropertiesReader.getBackoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox : "+uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox);
        uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_addFOIRCalculation_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_submitButton : "+uiObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_submitButton);
        uiObjParam.backoffice_appDetailsPage_action_addDeviation = uiPropertiesReader.getBackoffice_appDetailsPage_action_addDeviation();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addDeviation : "+uiObjParam.backoffice_appDetailsPage_action_addDeviation);
        uiObjParam.backoffice_appDetailsPage_action_addDeviation_deviationTextbox = uiPropertiesReader.getBackoffice_appDetailsPage_action_addDeviation_deviationTextbox();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addDeviation_deviationTextbox : "+uiObjParam.backoffice_appDetailsPage_action_addDeviation_deviationTextbox);
        uiObjParam.backoffice_appDetailsPage_action_addDeviation_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_addDeviation_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_addDeviation_submitButton : "+uiObjParam.backoffice_appDetailsPage_action_addDeviation_submitButton);
        uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource = uiPropertiesReader.getBackoffice_appDetailsPage_action_documentCollectionSource();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource : "+uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource);
        uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio = uiPropertiesReader.getBackoffice_appDetailsPage_action_documentCollectionSource_sourceRadio();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio : "+uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio);
        uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_documentCollectionSource_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_submitButton : "+uiObjParam.backoffice_appDetailsPage_action_documentCollectionSource_submitButton);
        uiObjParam.backoffice_appDetailsPage_action_completeFulfillment = uiPropertiesReader.getBackoffice_appDetailsPage_action_completeFulfillment();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_completeFulfillment : "+uiObjParam.backoffice_appDetailsPage_action_completeFulfillment);
        uiObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton : "+uiObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton);
        uiObjParam.backoffice_appDetailsPage_action_sendToBank = uiPropertiesReader.getBackoffice_appDetailsPage_action_sendToBank();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_sendToBank : "+uiObjParam.backoffice_appDetailsPage_action_sendToBank);
        uiObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton = uiPropertiesReader.getBackoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton : "+uiObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton);
        uiObjParam.bankPortal_loginPage_usernameTextBox = uiPropertiesReader.getBankPortal_loginPage_usernameTextBox();
        logger.info("uiObjParam.bankPortal_loginPage_usernameTextBox : "+uiObjParam.bankPortal_loginPage_usernameTextBox);
        uiObjParam.bankPortal_loginPage_passwordTextBox = uiPropertiesReader.getBankPortal_loginPage_passwordTextBox();
        logger.info("uiObjParam.bankPortal_loginPage_passwordTextBox : "+uiObjParam.bankPortal_loginPage_passwordTextBox);
        uiObjParam.bankPortal_loginPage_loginButton = uiPropertiesReader.getBankPortal_loginPage_loginButton();
        logger.info("uiObjParam.bankPortal_loginPage_loginButton : "+uiObjParam.bankPortal_loginPage_loginButton);
        uiObjParam.bankPortal_dashboard_pendingBankFraudReviewTab = uiPropertiesReader.getBankPortal_dashboard_pendingBankFraudReviewTab();
        logger.info("uiObjParam.bankPortal_dashboard_pendingBankFraudReviewTab : "+uiObjParam.bankPortal_dashboard_pendingBankFraudReviewTab);
        uiObjParam.bankPortal_dashboard_profileCircle = uiPropertiesReader.getBankPortal_dashboard_profileCircle();
        logger.info("uiObjParam.bankPortal_dashboard_profileCircle : "+uiObjParam.bankPortal_dashboard_profileCircle);
        uiObjParam.bankPortal_dashboard_profile_logoutButton = uiPropertiesReader.getBankPortal_dashboard_profile_logoutButton();
        logger.info("uiObjParam.bankPortal_dashboard_profile_logoutButton : "+uiObjParam.bankPortal_dashboard_profile_logoutButton);
        uiObjParam.bankPortal_dashboard_resultAppsMsg = uiPropertiesReader.getBankPortal_dashboard_resultAppsMsg();
        logger.info("uiObjParam.bankPortal_dashboard_resultAppsMsg : "+uiObjParam.bankPortal_dashboard_resultAppsMsg);
        uiObjParam.bankPortal_appDetailsPage_actionButton = uiPropertiesReader.getBankPortal_appDetailsPage_actionButton();
        logger.info("uiObjParam.bankPortal_appDetailsPage_actionButton : "+uiObjParam.bankPortal_appDetailsPage_actionButton);
        uiObjParam.bankPortal_appDetailsPage_headerStatus = uiPropertiesReader.getBankPortal_appDetailsPage_headerStatus();
        logger.info("uiObjParam.bankPortal_appDetailsPage_headerStatus : "+uiObjParam.bankPortal_appDetailsPage_headerStatus);
        uiObjParam.bankPortal_appDetailsPage_action_approveApplication = uiPropertiesReader.getBankPortal_appDetailsPage_action_approveApplication();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_approveApplication : "+uiObjParam.bankPortal_appDetailsPage_action_approveApplication);
        uiObjParam.bankPortal_appDetailsPage_action_approveApplication_approveAppButton = uiPropertiesReader.getBankPortal_appDetailsPage_action_approveApplication_approveAppButton();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_approveApplication_approveAppButton : "+uiObjParam.bankPortal_appDetailsPage_action_approveApplication_approveAppButton);
        uiObjParam.bankPortal_dashboard_pendingBankCreditReviewTab = uiPropertiesReader.getBankPortal_dashboard_pendingBankCreditReviewTab();
        logger.info("uiObjParam.bankPortal_dashboard_pendingBankCreditReviewTab : "+uiObjParam.bankPortal_dashboard_pendingBankCreditReviewTab);
        uiObjParam.bankPortal_dashboard_pendingFunding = uiPropertiesReader.getBankPortal_dashboard_pendingFunding();
        logger.info("uiObjParam.bankPortal_dashboard_pendingFunding : "+uiObjParam.bankPortal_dashboard_pendingFunding);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton);
        uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox = uiPropertiesReader.getBankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox : "+uiObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox);
        uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication = uiPropertiesReader.getBankPortal_appDetailsPage_action_approveCreditApplication();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication : "+uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication);
        uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton = uiPropertiesReader.getBankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton();
        logger.info("uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton : "+uiObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton);
        uiObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg = uiPropertiesReader.getBorrower_reviewPage_minimumCriteriaNoMatchMsg();
        logger.info("uiObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg : "+uiObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg);
        uiObjParam.borrower_eligibilityPage_sorryLabel = uiPropertiesReader.getBorrower_eligibilityPage_sorryLabel();
        logger.info("uiObjParam.borrower_eligibilityPage_sorryLabel : "+uiObjParam.borrower_eligibilityPage_sorryLabel);
        uiObjParam.borrower_eligibilityPage_unableToGenerateOfferMsg = uiPropertiesReader.getBorrower_eligibilityPage_unableToGenerateOfferMsg();
        logger.info("uiObjParam.borrower_eligibilityPage_unableToGenerateOfferMsg : "+uiObjParam.borrower_eligibilityPage_unableToGenerateOfferMsg);
        uiObjParam.backoffice_searchPage_applicantPhoneTextBox = uiPropertiesReader.getBackoffice_searchPage_applicantPhoneTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicantPhoneTextBox : "+uiObjParam.backoffice_searchPage_applicantPhoneTextBox);
        uiObjParam.borrower_logoutButton = uiPropertiesReader.getBorrower_logoutButton();
        logger.info("uiObjParam.borrower_logoutButton : "+uiObjParam.borrower_logoutButton);
        uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_notInterestedButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton : "+uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton);
        uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown = uiPropertiesReader.getBackoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown : "+uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown);
        uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton : "+uiObjParam.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyEMI = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_mothlyEMI();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyEMI : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyEMI);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyExpense = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_monthlyExpense();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyExpense : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyExpense);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_monthlyIncome();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyInvestment = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_mothlyInvestment();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyInvestment : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyInvestment);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_dailyAverageBalance();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_noOfECSbounce = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_noOfECSbounce();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_noOfECSbounce : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_noOfECSbounce);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_salarayDate = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_salarayDate();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_salarayDate : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_salarayDate);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_maximumCreditAmount();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount);
        uiObjParam.backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit = uiPropertiesReader.getBackoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit();
        logger.info("uiObjParam.backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit : "+uiObjParam.backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit);
        uiObjParam.backoffice_appDetails_getFinalOffer = uiPropertiesReader.getBackoffice_appDetails_getFinalOffer();
        logger.info("uiObjParam.backoffice_appDetails_getFinalOffer : "+uiObjParam.backoffice_appDetails_getFinalOffer);
        uiObjParam.backoffice_appDetails_submitGetFinalOffer = uiPropertiesReader.getBackoffice_appDetails_submitGetFinalOffer();
        logger.info("uiObjParam.backoffice_appDetails_submitGetFinalOffer : "+uiObjParam.backoffice_appDetails_submitGetFinalOffer);
        uiObjParam.backoffice_appDetails_selectFinalOffer = uiPropertiesReader.getBackoffice_appDetails_selectFinalOffer();
        logger.info("uiObjParam.backoffice_appDetails_selectFinalOffer : "+uiObjParam.backoffice_appDetails_selectFinalOffer);
        uiObjParam.backoffice_appDetails_submitSelectFinalOffer = uiPropertiesReader.getBackoffice_appDetails_submitSelectFinalOffer();
        logger.info("uiObjParam.backoffice_appDetails_submitSelectFinalOffer : "+uiObjParam.backoffice_appDetails_submitSelectFinalOffer);
        uiObjParam.backoffice_searchPage_createNewuser = uiPropertiesReader.getBackoffice_searchPage_createNewuser();
        logger.info("uiObjParam.backoffice_searchPage_createNewuser : "+uiObjParam.backoffice_searchPage_createNewuser);
        
        uiObjParam.backoffice_userAdministrationPage_createNewUser_fullName = uiPropertiesReader.getBackoffice_userAdministrationPage_createNewUser_fullName();
        logger.info("uiObjParam.backoffice_userAdministrationPage_createNewUser_fullName : "+uiObjParam.backoffice_userAdministrationPage_createNewUser_fullName);
      
        uiObjParam.backoffice_userAdministrationPage_createNewUser_userName = uiPropertiesReader.getBackoffice_userAdministrationPage_createNewUser_userName();
        logger.info("uiObjParam.backoffice_userAdministrationPage_createNewUser_userName : "+uiObjParam.backoffice_userAdministrationPage_createNewUser_userName);
      
        uiObjParam.backoffice_userAdministrationPage_createNewUser_password = uiPropertiesReader.getBackoffice_userAdministrationPage_createNewUser_password();
        logger.info("uiObjParam.backoffice_userAdministrationPage_createNewUser_password : "+uiObjParam.backoffice_userAdministrationPage_createNewUser_password);
      
        uiObjParam.backoffice_userAdministrationPage_createNewUser_email = uiPropertiesReader.getBackoffice_userAdministrationPage_createNewUser_email();
        logger.info("uiObjParam.backoffice_userAdministrationPage_createNewUser_email : "+uiObjParam.backoffice_userAdministrationPage_createNewUser_email);
      
        uiObjParam.backoffice_userAdministrationPage_createNewUser_submitNewUser = uiPropertiesReader.getBackoffice_userAdministrationPage_createNewUser_submitNewUser();
        logger.info("uiObjParam.backoffice_userAdministrationPage_createNewUser_submitNewUser : "+uiObjParam.backoffice_userAdministrationPage_createNewUser_submitNewUser);
      
        uiObjParam.backoffice_searchPage_allUser = uiPropertiesReader.getBackoffice_searchPage_allUser();
        logger.info("uiObjParam.backoffice_searchPage_allUser : "+uiObjParam.backoffice_searchPage_allUser);
        
        uiObjParam.backoffice_userAdministrationPage_allUser_pageNavigation = uiPropertiesReader.getBackoffice_userAdministrationPage_allUser_pageNavigation();
        logger.info("uiObjParam.backoffice_userAdministrationPage_allUser_pageNavigation : "+uiObjParam.backoffice_userAdministrationPage_allUser_pageNavigation);
      
        uiObjParam.backoffice_userAdministrationPage_editUser_editButton = uiPropertiesReader.getBackoffice_userAdministrationPage_editUser_editButton();
        logger.info("uiObjParam.backoffice_userAdministrationPage_editUser_editButton : "+uiObjParam.backoffice_userAdministrationPage_editUser_editButton);
        
        uiObjParam.backoffice_userAdministrationPage_editUser_updateDetailsButton = uiPropertiesReader.getBackoffice_userAdministrationPage_editUser_updateDetailsButton();
        logger.info("uiObjParam.backoffice_userAdministrationPage_editUser_updateDetailsButton : "+uiObjParam.backoffice_userAdministrationPage_editUser_updateDetailsButton);
             
        uiObjParam.backoffice_userAdministrationPage_editUser_emailInfo = uiPropertiesReader.getBackoffice_userAdministrationPage_editUser_emailInfo();
        logger.info("uiObjParam.backoffice_userAdministrationPage_editUser_emailInfo : "+uiObjParam.backoffice_userAdministrationPage_editUser_emailInfo);
       
        uiObjParam.backoffice_userAdministrationPage_deActivateButton = uiPropertiesReader.getBackoffice_userAdministrationPage_deActivateButton();
        logger.info("uiObjParam.backoffice_userAdministrationPage_deActivateButton : "+uiObjParam.backoffice_userAdministrationPage_deActivateButton);
     
        uiObjParam.backoffice_userAdministrationPage_userSettings = uiPropertiesReader.getBackoffice_userAdministrationPage_userSettings();
        logger.info("uiObjParam.backoffice_userAdministrationPage_userSettings : "+uiObjParam.backoffice_userAdministrationPage_userSettings);
        
        uiObjParam.backoffice_userAdministrationPage_userSettingsLogout = uiPropertiesReader.getBackoffice_userAdministrationPage_userSettingsLogout();
        logger.info("uiObjParam.backoffice_userAdministrationPage_userSettingsLogout : "+uiObjParam.backoffice_userAdministrationPage_userSettingsLogout);
      
        
        uiObjParam.backoffice_searchPage_applicationPANTextBox = uiPropertiesReader.getBackoffice_searchPage_applicationPANTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicationPANTextBox : "+uiObjParam.backoffice_searchPage_applicationPANTextBox);
        
        uiObjParam.backoffice_searchPage_applicationEmailTextBox = uiPropertiesReader.getBackoffice_searchPage_applicationEmailTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicationEmailTextBox : "+uiObjParam.backoffice_searchPage_applicationEmailTextBox);
     
        uiObjParam.backoffice_searchPage_applicationNameTextBox = uiPropertiesReader.getBackoffice_searchPage_applicationNameTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicationNameTextBox : "+uiObjParam.backoffice_searchPage_applicationNameTextBox);
     
        uiObjParam.backoffice_searchPage_applicationEmployerTextBox = uiPropertiesReader.getBackoffice_searchPage_applicationEmployerTextBox();
        logger.info("uiObjParam.backoffice_searchPage_applicationEmployerTextBox : "+uiObjParam.backoffice_searchPage_applicationEmployerTextBox);
     
        uiObjParam.backoffice_searchPage_resultDataFetch_appId = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_appId();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_appId : "+uiObjParam.backoffice_searchPage_resultDataFetch_appId);
       
        uiObjParam.backoffice_searchPage_resultDataFetch_mobile = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_mobile();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_mobile : "+uiObjParam.backoffice_searchPage_resultDataFetch_mobile);
       
        uiObjParam.backoffice_searchPage_resultDataFetch_pan = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_pan();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_pan : "+uiObjParam.backoffice_searchPage_resultDataFetch_pan);
        
        uiObjParam.backoffice_searchPage_resultDataFetch_email = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_email();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_email : "+uiObjParam.backoffice_searchPage_resultDataFetch_email);
       
        uiObjParam.backoffice_searchPage_resultDataFetch_name = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_name();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_name : "+uiObjParam.backoffice_searchPage_resultDataFetch_name);
      
        uiObjParam.backoffice_searchPage_resultDataFetch_employername = uiPropertiesReader.getBackoffice_searchPage_resultDataFetch_employername();
        logger.info("uiObjParam.backoffice_searchPage_resultDataFetch_employername : "+uiObjParam.backoffice_searchPage_resultDataFetch_employername);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_role = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_role();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_role : "+uiObjParam.backoffice_appDetailsPage_actions_assign_role);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_user = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_user();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_user : "+uiObjParam.backoffice_appDetailsPage_actions_assign_user);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_notes = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_notes();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_notes : "+uiObjParam.backoffice_appDetailsPage_actions_assign_notes);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_submitButton : "+uiObjParam.backoffice_appDetailsPage_actions_assign_submitButton);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_leftAssignMenu();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu : "+uiObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu);
     
        uiObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assign_rightAssignMenu();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu : "+uiObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu);
     
        uiObjParam.backoffice_searchPage_assignedResultData_appId = uiPropertiesReader.getBackoffice_searchPage_assignedResultData_appId();
        logger.info("uiObjParam.backoffice_searchPage_assignedResultData_appId : "+uiObjParam.backoffice_searchPage_assignedResultData_appId);
     
        uiObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu = uiPropertiesReader.getBackoffice_appDetailsPage_actions_claim_rightClaimMenu();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu : "+uiObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu);

        uiObjParam.backoffice_appDetailsPage_actions_claim_role = uiPropertiesReader.getBackoffice_appDetailsPage_actions_claim_role();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_claim_role : "+uiObjParam.backoffice_appDetailsPage_actions_claim_role);
     
        uiObjParam.backoffice_appDetailsPage_actions_claim_submitButton = uiPropertiesReader.getBackoffice_appDetailsPage_actions_claim_submitButton();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_claim_submitButton : "+uiObjParam.backoffice_appDetailsPage_actions_claim_submitButton);
         
        uiObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn = uiPropertiesReader.getBackoffice_searchPage_assignedResultData_appIdDetailsBtn();
        logger.info("uiObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn : "+uiObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn);
         
        uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_BottomMenu();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu);
         
        uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_BottomValueOne();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne);
         
        uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_BottomValueTwo();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo);
         
        uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_releaseBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn);
      
        uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_releaseSubBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn);
      
        uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn = uiPropertiesReader.getBackoffice_appDetailsPage_actions_assignments_releaseFinalBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn : "+uiObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn);
      

    	// Update failed submission application
        
        uiObjParam.backoffice_searchPage_failedSubmission = uiPropertiesReader.getBackoffice_searchPage_failedSubmission();
        logger.info("uiObjParam.backoffice_searchPage_failedSubmission : "+uiObjParam.backoffice_searchPage_failedSubmission);
      
        uiObjParam.backoffice_appDetailsPage_failedSubmission_selectDetailsApp = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_selectDetailsApp();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_selectDetailsApp : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_selectDetailsApp);
      
        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEduDetails = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEduDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEduDetails : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEduDetails);
      
        uiObjParam.backoffice_appDetailsPage_failedSubmission_eduQualification = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_eduQualification();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_eduQualification : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_eduQualification);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_eduInstitution = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_eduInstitution();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_eduInstitution : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_eduInstitution);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_eduSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_eduSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_eduSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_eduSubmitBtn);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDetails = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDetails : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDetails);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpCompanyName();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDesignation = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpDesignation();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDesignation : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDesignation);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpTotalExp();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpEmail = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpEmail();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpEmail : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpEmail);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpIncome = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpIncome();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpIncome : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpIncome);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress1 = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpAddress1();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress1 : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress1);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress2 = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpAddress2();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress2 : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress2);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpLocation = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpLocation();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpLocation : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpLocation);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpPincode = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpPincode();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpPincode : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpPincode);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updatePersonalDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updatePersonalDOB();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalPAN = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updatePersonalPAN();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalPAN : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalPAN);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressDetails = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressDetails : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressDetails);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd1 = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressAdd1();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd1 : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd1);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd2 = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressAdd2();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd2 : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd2);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressLoc = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressLoc();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressLoc : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressLoc);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressPincode = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressPincode();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressPincode : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressPincode);
       
        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressChkBox();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox);

        
        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeDetails();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeResidency = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeResidency();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeResidency : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeResidency);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance);

        uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn);
               
        uiObjParam.backoffice_searchPage_pendinginitialoffer = uiPropertiesReader.getBackoffice_searchPage_pendinginitialoffer();
        logger.info("uiObjParam.backoffice_searchPage_pendinginitialoffer : "+uiObjParam.backoffice_searchPage_pendinginitialoffer);
       
        uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp = uiPropertiesReader.getBackoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp();
        logger.info("uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp : "+uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp);
       
        uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer = uiPropertiesReader.getBackoffice_appDetailsPage_pendingInitialoffer_selectDropOffer();
        logger.info("uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer : "+uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer);
       
        uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer = uiPropertiesReader.getBackoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer();
        logger.info("uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer : "+uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer);
       
        uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn = uiPropertiesReader.getBackoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn();
        logger.info("uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn : "+uiObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn);
       
        uiObjParam.backoffice_searchPage_pendingbankstmt = uiPropertiesReader.getBackoffice_searchPage_pendingbankstmt();
        logger.info("uiObjParam.backoffice_searchPage_pendingbankstmt : "+uiObjParam.backoffice_searchPage_pendingbankstmt);
      
        uiObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp = uiPropertiesReader.getBackoffice_appDetailsPage_pendingBankstmt_selectDetailsApp();
        logger.info("uiObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp : "+uiObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp);
      
               
    }

    public void setupDBParams() {

        logger.info("--------------- READING PORTAL PROPERTIES FILE ----------------");

        portalParam.borrowerUrl = portalPropertiesReader.getBorrowerUrl();
        logger.info("portalParam.borrowerUrl :" + portalParam.borrowerUrl);
        portalParam.backOfficeUrl = portalPropertiesReader.getBackOfficeUrl();
        logger.info("portalParam.backOfficeUrl :" + portalParam.backOfficeUrl);
        portalParam.custom_report_location = portalPropertiesReader.getCustom_report_location();
        logger.info("portalParam.custom_report_location :" + portalParam.custom_report_location);
        portalParam.bankPortalUrl = portalPropertiesReader.getBankPortalUrl();
        logger.info("portalParam.bankPortalUrl :" + portalParam.bankPortalUrl);
        portalParam.password = portalPropertiesReader.getPassword();
        logger.info("portalParam.password :" + portalParam.password);
        portalParam.backOfficeUsername = portalPropertiesReader.getBackOfficeUsername();
        logger.info("portalParam.backOfficeUsername :" + portalParam.backOfficeUsername);
        portalParam.backOfficePassword = portalPropertiesReader.getBackOfficePassword();
        logger.info("portalParam.backOfficePassword :" + portalParam.backOfficePassword);
        portalParam.browser = portalPropertiesReader.getBrowser();
        logger.info("portalParam.browser :" + portalParam.browser);
        portalParam.upload_file_location = portalPropertiesReader.getUpload_file_location();
        logger.info("portalParam.upload_file_location :" + portalParam.upload_file_location);
        portalParam.mongoHost = portalPropertiesReader.getMongoHost();
        logger.info("portalParam.mongoHost :" + portalParam.mongoHost);
        portalParam.mongoPort = portalPropertiesReader.getMongoPort();
        logger.info("portalParam.mongoPort :" + portalParam.mongoPort);
        portalParam.mongoDb = portalPropertiesReader.getMongoDb();
        logger.info("portalParam.mongoDb :" + portalParam.mongoDb);
        portalParam.mongoCollection = portalPropertiesReader.getMongoCollection();
        logger.info("portalParam.mongoCollection :" + portalParam.mongoCollection);
        apiParam.token = apiPropertiesReader.getToken();
        logger.info("apiParam.token :" + apiParam.token);
        apiParam.verificationService = apiPropertiesReader.getVerificationService();
        logger.info("apiParam.verificationService :" + apiParam.verificationService);
        apiParam.cibilService = apiPropertiesReader.getCibilService();
        logger.info("apiParam.cibilService :" + apiParam.cibilService);
        portalParam.bankPortalUsername = portalPropertiesReader.getBankPortalUsername();
        logger.info("portalParam.bankPortalUsername :" + portalParam.bankPortalUsername);
        portalParam.bankPortalPassword = portalPropertiesReader.getBankPortalPassword();
        logger.info("portalParam.bankPortalPassword :" + portalParam.bankPortalPassword);
        portalParam.numberOfApps = portalPropertiesReader.getNumberOfApps();
        logger.info("portalParam.numberOfApps :" + portalParam.numberOfApps);
        portalParam.numberOfAppsWithStatus = portalPropertiesReader.getNumberOfAppsWithStatus();
        logger.info("portalParam.numberOfAppsWithStatus :" + portalParam.numberOfAppsWithStatus);

    }

    private void setupJetty(final String contextRoot, Server server) throws Exception {
        final AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(ApiConfig.class,PortalConfig.class);
//        applicationContext.register(SmppConfig.class);
        final ServletHolder servletHolder = new ServletHolder(new DispatcherServlet(applicationContext));
        final ServletContextHandler context = new ServletContextHandler();
        context.setErrorHandler(null);
        context.setContextPath("/" + contextRoot);
        context.addServlet(servletHolder, "/*");

        server.setHandler(context);
        server.start();
    }

    @Test(groups = { "Result", "", "" }, description = "Results file creation")
    public static void AAResultsBackUp() throws Exception {
        System.out.println("*****Begining of ResultsBackUp *********************");

        try {
            TestResults.CreateResultTxt();
            //TestResults.ExportResultToTxt("Portal Url:=",GlobalVariables.portalUrl);
            Assert.assertTrue(true, "ResultsBackUp test case has been Passed.");
            System.out.println("*****Ending of ResultsBackUp *********************");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Exception happened in executing test case ResultsBackUp");
            fail("ResultsBackUp test case has been failed.");
        }
    }

    protected void initializeData(String FuncMod, String Test_ID) throws Exception {

        try {

            logger.info("Reading test data from " + FuncMod + " worksheet.");
            logger.info("-----------------------------------------------");

            if (FuncMod.equalsIgnoreCase("Portal")) {
                portalParam.testId = portalFuncUtils.getTestData(FuncMod, Test_ID, "Test_ID");
                logger.info("portalParam.testId: " + portalParam.testId);
                portalParam.amountNeed = portalFuncUtils.getTestData(FuncMod, Test_ID, "amountNeed");
                logger.info("portalParam.amountNeed: " + portalParam.amountNeed);
                portalParam.loanPurpose = portalFuncUtils.getTestData(FuncMod, Test_ID, "loanPurpose");
                logger.info("portalParam.loanPurpose: " + portalParam.loanPurpose);
                portalParam.salutation = portalFuncUtils.getTestData(FuncMod, Test_ID, "salutation");
                logger.info("portalParam.salutation: " + portalParam.salutation);
                portalParam.maritalStatus = portalFuncUtils.getTestData(FuncMod, Test_ID, "maritalStatus");
                logger.info("portalParam.maritalStatus: " + portalParam.maritalStatus);
                portalParam.firstName = portalFuncUtils.getTestData(FuncMod, Test_ID, "firstName");
                logger.info("portalParam.firstName: " + portalParam.firstName);
                portalParam.middleName = portalFuncUtils.getTestData(FuncMod, Test_ID, "middleName");
                logger.info("portalParam.middleName: " + portalParam.middleName);
                portalParam.lastName = portalFuncUtils.getTestData(FuncMod, Test_ID, "lastName");
                logger.info("portalParam.lastName: " + portalParam.lastName);
                portalParam.emailID = portalFuncUtils.getTestData(FuncMod, Test_ID, "emailID");
                String inviteId = new SimpleDateFormat("ddMMHHmmss").format(Calendar.getInstance().getTime());
                portalParam.emailID = PortalParam.emailID + inviteId + "@gmail.com";
                logger.info("portalParam.emailID: " + portalParam.emailID);
                portalParam.mobile = (new Random().nextInt(10-6)+6) + RandomStringUtils.randomNumeric(9);
                logger.info("portalParam.mobile: " + portalParam.mobile);
                portalParam.otp = portalFuncUtils.getTestData(FuncMod, Test_ID, "otp");
                logger.info("portalParam.otp: " + portalParam.otp);
                portalParam.legalBusiness = portalFuncUtils.getTestData(FuncMod, Test_ID, "legalBusiness");
                logger.info("portalParam.legalBusiness: " + portalParam.legalBusiness);
                portalParam.salary = portalFuncUtils.getTestData(FuncMod, Test_ID, "salary");
                logger.info("portalParam.salary: " + portalParam.salary);
                portalParam.officialEmailId = portalFuncUtils.getTestData(FuncMod, Test_ID, "officialEmailId");
                portalParam.officialEmailId = PortalParam.officialEmailId + "@sigmainfo.net";
                logger.info("portalParam.officialEmailId: " + portalParam.officialEmailId);
                portalParam.residenceType = portalFuncUtils.getTestData(FuncMod, Test_ID, "residenceType");
                logger.info("portalParam.residenceType: " + portalParam.residenceType);
                portalParam.dob = portalFuncUtils.getTestData(FuncMod, Test_ID, "dob");
                logger.info("portalParam.dob: " + portalParam.dob);
                portalParam.mob = portalFuncUtils.getTestData(FuncMod, Test_ID, "mob");
                logger.info("portalParam.mob: " + portalParam.mob);
                portalParam.yob = portalFuncUtils.getTestData(FuncMod, Test_ID, "yob");
                logger.info("portalParam.yob: " + portalParam.yob);
                portalParam.aadharNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "aadharNumber");
                logger.info("portalParam.aadharNumber: " + portalParam.aadharNumber);
                portalParam.panNumber = PortalFuncUtils.getRandomAlphabets(3) + "P" + PortalFuncUtils.getRandomAlphabets(1) + String.format("%04d", Integer.valueOf(new Random().nextInt(10000))) + PortalFuncUtils.getRandomAlphabets(1);
                logger.info("portalParam.panNumber: " + portalParam.panNumber);
                portalParam.address1 = portalFuncUtils.getTestData(FuncMod, Test_ID, "address1");
                logger.info("portalParam.address1: " + portalParam.address1);
                portalParam.address2 = portalFuncUtils.getTestData(FuncMod, Test_ID, "address2");
                logger.info("portalParam.address2: " + portalParam.address2);
                portalParam.locality = portalFuncUtils.getTestData(FuncMod, Test_ID, "locality");
                logger.info("portalParam.locality: " + portalParam.locality);
                portalParam.pincode = portalFuncUtils.getTestData(FuncMod, Test_ID, "pincode");
                logger.info("portalParam.pincode: " + portalParam.pincode);
                portalParam.bothResidenceSame = Boolean.valueOf(portalFuncUtils.getTestData(FuncMod, Test_ID, "bothResidenceSame"));
                logger.info("portalParam.bothResidenceSame: " + portalParam.bothResidenceSame);
                portalParam.bankName = portalFuncUtils.getTestData(FuncMod, Test_ID, "bankName");
                logger.info("portalParam.bankName: " + portalParam.bankName);
                portalParam.education = portalFuncUtils.getTestData(FuncMod, Test_ID, "education");
                logger.info("portalParam.education: " + portalParam.education);
                portalParam.institutionName = portalFuncUtils.getTestData(FuncMod, Test_ID, "institutionName");
                logger.info("portalParam.institutionName: " + portalParam.institutionName);
                portalParam.designation = portalFuncUtils.getTestData(FuncMod, Test_ID, "designation");
                logger.info("portalParam.designation: " + portalParam.designation);
                portalParam.totalExperience = portalFuncUtils.getTestData(FuncMod, Test_ID, "totalExperience");
                logger.info("portalParam.totalExperience: " + portalParam.totalExperience);
                portalParam.employerAddress = portalFuncUtils.getTestData(FuncMod, Test_ID, "employerAddress");
                logger.info("portalParam.employerAddress: " + portalParam.employerAddress);
                portalParam.empLocality = portalFuncUtils.getTestData(FuncMod, Test_ID, "empLocality");
                logger.info("portalParam.empLocality: " + portalParam.empLocality);
                portalParam.empPincode = portalFuncUtils.getTestData(FuncMod, Test_ID, "empPincode");
                logger.info("portalParam.empPincode: " + portalParam.empPincode);
                portalParam.term = portalFuncUtils.getTestData(FuncMod, Test_ID, "term");
                logger.info("portalParam.term: " + portalParam.term);
                portalParam.loanAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "loanAmount");
                logger.info("portalParam.loanAmount: " + portalParam.loanAmount);
                portalParam.processingFee = portalFuncUtils.getTestData(FuncMod, Test_ID, "processingFee");
                logger.info("portalParam.processingFee: " + portalParam.processingFee);
                portalParam.emi = portalFuncUtils.getTestData(FuncMod, Test_ID, "emi");
                logger.info("portalParam.emi: " + portalParam.emi);
                portalParam.interestRate = portalFuncUtils.getTestData(FuncMod, Test_ID, "interestRate");
                logger.info("portalParam.interestRate: " + portalParam.interestRate);
                portalParam.processingFeePercentage = portalFuncUtils.getTestData(FuncMod, Test_ID, "processingFeePercentage");
                logger.info("portalParam.processingFeePercentage: " + portalParam.processingFeePercentage);
                portalParam.accountNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "accountNumber");
                logger.info("portalParam.accountNumber: " + portalParam.accountNumber);
                portalParam.ifscCode = portalFuncUtils.getTestData(FuncMod, Test_ID, "ifscCode");
                logger.info("portalParam.ifscCode: " + portalParam.ifscCode);
                portalParam.community = portalFuncUtils.getTestData(FuncMod, Test_ID, "community");
                logger.info("portalParam.community: " + portalParam.community);
                portalParam.caste = portalFuncUtils.getTestData(FuncMod, Test_ID, "caste");
                logger.info("portalParam.caste: " + portalParam.caste);
                portalParam.kycExpiryDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "kycExpiryDate");
                logger.info("portalParam.kycExpiryDate: " + portalParam.kycExpiryDate);
                portalParam.kycDocNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "kycDocNumber");
                logger.info("portalParam.kycDocNumber: " + portalParam.kycDocNumber);
                portalParam.motherName = portalFuncUtils.getTestData(FuncMod, Test_ID, "motherName");
                logger.info("portalParam.motherName: " + portalParam.motherName);
                portalParam.currentResidenceYears = portalFuncUtils.getTestData(FuncMod, Test_ID, "currentResidenceYears");
                logger.info("portalParam.currentResidenceYears: " + portalParam.currentResidenceYears);
                portalParam.currentCityYears = portalFuncUtils.getTestData(FuncMod, Test_ID, "currentCityYears");
                logger.info("portalParam.currentCityYears: " + portalParam.currentCityYears);
                portalParam.workExperience = portalFuncUtils.getTestData(FuncMod, Test_ID, "workExperience");
                logger.info("portalParam.workExperience: " + portalParam.workExperience);
                portalParam.kycDocType = portalFuncUtils.getTestData(FuncMod, Test_ID, "kycDocType");
                logger.info("portalParam.kycDocType: " + portalParam.kycDocType);
                portalParam.dependants = portalFuncUtils.getTestData(FuncMod, Test_ID, "dependants");
                logger.info("portalParam.dependants: " + portalParam.dependants);
                portalParam.gurdianName = portalFuncUtils.getTestData(FuncMod, Test_ID, "gurdianName");
                logger.info("portalParam.gurdianName: " + portalParam.gurdianName);
                portalParam.salary = portalFuncUtils.getTestData(FuncMod, Test_ID, "salary");
                logger.info("portalParam.salary: " + portalParam.salary);
                portalParam.salaryDate = portalFuncUtils.getTestData(FuncMod, Test_ID, "salaryDate");
                logger.info("portalParam.salaryDate: " + portalParam.salaryDate);
                portalParam.narration = portalFuncUtils.getTestData(FuncMod, Test_ID, "narration");
                logger.info("portalParam.narration: " + portalParam.narration);
                portalParam.typeOfLoan = portalFuncUtils.getTestData(FuncMod, Test_ID, "typeOfLoan");
                logger.info("portalParam.typeOfLoan: " + portalParam.typeOfLoan);
                portalParam.obligatedAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "obligatedAmount");
                logger.info("portalParam.obligatedAmount: " + portalParam.obligatedAmount);
                portalParam.comments = portalFuncUtils.getTestData(FuncMod, Test_ID, "comments");
                logger.info("portalParam.comments: " + portalParam.comments);
                portalParam.deviation = portalFuncUtils.getTestData(FuncMod, Test_ID, "deviation");
                logger.info("portalParam.deviation: " + portalParam.deviation);
                portalParam.docCollectionSource = portalFuncUtils.getTestData(FuncMod, Test_ID, "docCollectionSource");
                logger.info("portalParam.docCollectionSource: " + portalParam.docCollectionSource);
                portalParam.loanaccountNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "loanAccountNumber");
                logger.info("portalParam.loanaccountNumber: " + portalParam.loanaccountNumber);
                portalParam.paymentStatus = portalFuncUtils.getTestData(FuncMod, Test_ID, "paymentStatus");
                logger.info("portalParam.paymentStatus: " + portalParam.paymentStatus);
                portalParam.emiAmount = portalFuncUtils.getTestData(FuncMod, Test_ID, "emiAmount");
                logger.info("portalParam.emiAmount: " + portalParam.emiAmount);
                portalParam.utrNumber = portalFuncUtils.getTestData(FuncMod, Test_ID, "utrNumber");
                logger.info("portalParam.utrNumber: " + portalParam.utrNumber);
                portalParam.notInterestedReason = portalFuncUtils.getTestData(FuncMod, Test_ID, "notInterestedReason");
                logger.info("portalParam.notInterestedReason: " + portalParam.notInterestedReason);

            }
            logger.info("-----------------------------------------------");
        } catch (Exception e) {
            logger.info("    *****    Field not present in \"" + FuncMod + "\" worksheet.");
        }
    }

    public void writeToReport(String funcMod,String sTestID, String result) throws Exception {

        String base64String=null;
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", sTestID);
            obj.put("status", result);
            if((funcMod.contains("Portal") && (result.equalsIgnoreCase("Failed"))))
            {
                //File file = new File(System.getProperty("user.dir")+"\\src\\test\\resources\\images\\"+sTestID+".png");
                File file = new File(portalParam.custom_report_location+sTestID+".png");
                System.out.println(file.exists());
                byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
                base64String = new String(encoded, StandardCharsets.US_ASCII);
                obj.put("screenshot", "data:image/png;base64,"+base64String);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            File file = new File(PortalParam.custom_report_location + "test.json");
            ifFileExist = file.createNewFile();
            if (ifFileExist) {
                FileWriter f = new FileWriter(file);
                f.write("cases : [" + obj.toString());
                f.flush();
                f.close();
            } else {
                FileWriter f = new FileWriter(file, true);
                f.write("," + obj.toString());
                f.flush();
                f.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod(groups = {"PortalTests","createMultipleApps","verifyEndToEndFlow","createMultipleAppsWithStatus","unclassifiedCompany_manualreview","verifyNotInterestedApp"})
    @Parameters("browser")
    public void setUp() throws Exception {
        //String ibrowser = GlobalVariables.browser;

        browser_list browser = browser_list.valueOf(portalPropertiesReader.getBrowser());
        String nodeUrl;
        switch (browser) {

            case IE:

                // IEDriverServer available @
                // http://code.google.com/p/selenium/downloads/list
                System.setProperty("webdriver.ie.driver", "./webdriver/IEDriverServer.exe");
                DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
                capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                driver = new InternetExplorerDriver(capabilities);
                break;

            case Chrome:
                // chromedriver available @http://code.google.com/p/chromedriver/downloads/list
                System.setProperty("webdriver.chrome.driver", "./webdriver/chromedriver.exe");
            	//System.setProperty("webdriver.chrome.driver", "./webdriver/chromedriver");
                System.setProperty("java.awt.headless", "false");
                driver = new ChromeDriver();

                break;

            case FF:
                System.setProperty("webdriver.gecko.driver" ,"./webdriver/geckodriver.exe");
                driver = new FirefoxDriver();
        }

        Thread.sleep(profile_delay);
        driver.manage().timeouts().implicitlyWait(ShortSleep, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        logger.info("Loading url :" + portalPropertiesReader.getBorrowerUrl());
        driver.get(portalPropertiesReader.getBorrowerUrl());
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_loanDetailsMenu)),portalPropertiesReader.getBorrowerUrl() +" could not be loaded.");
    }



    @AfterMethod(groups = {"PortalTests","createMultipleApps","verifyEndToEndFlow","createMultipleAppsWithStatus","unclassifiedCompany_manualreview","verifyNotInterestedApp"})
    public void quit()
    {
        driver.quit();
    }
    /**
     * Waits for the expected condition to complete
     *
     * @param e the expected condition to wait until it becomes true
     * @param timeout  how long to wait for the expected condition to turn true
     * @return true if the expected condition returned true and false if not
     */
    public boolean waitForCondition(ExpectedCondition<Boolean> e, int timeout) {
        WebDriverWait w = new WebDriverWait(driver, timeout);
        boolean returnValue = true;
        try {
            w.until(e);
        } catch (TimeoutException te) {
            logger.error("Failed to find the expected condition", te);
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Wait for the element to disappear from the screen
     *
     * @param finder the element to wait to disappear
     * @param timeout  how long to wait for the item to disappear (in seconds)
     * @return true if the element disappeared and false if it did not
     */
    protected boolean waitForElementRemoval(final By finder, int timeout) {
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() == 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Wait for the element to appear on the screen
     * @param finder  the element to wait to appear
     * @param timeout  how long to wait for the item to appear (in seconds)
     * @return true if the element appeared and false if it did not
     */
    protected boolean waitForElement(final By finder, int timeout) {
        //logger.info("Waiting for element to load");
        ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElements(finder).size() > 0;
            }
        };
        boolean returnValue = waitForCondition(e, timeout);
        return returnValue;
    }

    /**
     * Sleeps for the desired amount of time
     *
     * @param time
     *            the amount of time to sleep in ms
     */
    protected void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ie) {
        }
    }

    public void waitForPageLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.manage().window().maximize();
        wait.until(pageLoadCondition);
    }

    @BeforeSuite(alwaysRun=true)
    public void  startCaseReport() throws Exception {
        deletePreviousScreenshots();
        logger.info("Initializing reporting framework");
        logger.info("---------------------------------");
        auto_start = System.currentTimeMillis();

        logger.info("portalParam.custom_report_location:"+portalParam.custom_report_location);

        FileUtils.copyFile(new File(portalParam.custom_report_location + "header.txt"),new File(portalParam.custom_report_location + "header_template.txt"));
        FileUtils.copyFile(new File(portalParam.custom_report_location + "trailor.txt"),new File(portalParam.custom_report_location + "trailor_template.txt"));

        logger.info("Calling Before Suite for generating reporting files.");

        casefile = new File(portalParam.custom_report_location+ "test.json");
        portal_scenfile = new File(portalParam.custom_report_location + "portal_scenario.json");
        portal_feafile = new File(portalParam.custom_report_location + "portal_feature.json");
        api_scenfile = new File(portalParam.custom_report_location + "api_scenario.json");
        api_feafile = new File(portalParam.custom_report_location + "api_feature.json");

        feafile = new File(portalParam.custom_report_location + "feature.json");

        casefile.createNewFile();
        FileWriter casef = new FileWriter(casefile);
        portal_scenfile.createNewFile();
        FileWriter portal_scenf = new FileWriter(portal_scenfile);
        portal_feafile.createNewFile();
        FileWriter portal_feaf = new FileWriter(portal_feafile);
        api_scenfile.createNewFile();
        FileWriter api_scenf = new FileWriter(api_scenfile);
        api_feafile.createNewFile();
        FileWriter api_feaf = new FileWriter(api_feafile);

        feafile.createNewFile();
        FileWriter featurefw = new FileWriter(feafile);
        portal_feaf.write("features: [");
        portal_scenf.write("scenarios: [");
        casef.write("cases : [");
        casef.flush();
        casef.close();
        portal_scenf.flush();
        portal_scenf.close();
        portal_feaf.flush();
        portal_feaf.close();
        featurefw.write("features: [");
        api_feaf.write("features: [");
        api_scenf.write("scenarios: [");
        api_scenf.flush();
        api_scenf.close();
        api_feaf.flush();
        api_feaf.close();
        featurefw.flush();
        featurefw.close();



    }

    private void deletePreviousScreenshots() {
        File folder = new File(PortalParam.custom_report_location);
        File fList[] = folder.listFiles();

        for (File f : fList) {
            if (f.getName().endsWith(".png")) {
                f.delete();
            }}
    }

    public void generateReport(String className, String description,String funcModule) throws IOException, JSONException {

        String classStatus = "";
        File scenFile = null;

        if(funcModule.equalsIgnoreCase("ProjectTest_Api"))
        {
            scenFile = api_scenfile;
        }
        else if(funcModule.equalsIgnoreCase("Portal"))
        {
            scenFile = portal_scenfile;
        }

        logger.info("scenFile:"+scenFile);
        logger.info("casefile:"+casefile);
        BufferedReader scenreader = new BufferedReader(new FileReader(scenFile));
        BufferedReader reader = new BufferedReader(new FileReader(casefile));
        String line = "", oldtext = "";
        while ((line = reader.readLine()) != null) {
            oldtext += line + "\r";
        }

        reader.close();
        String newtext = oldtext.replaceAll("\\[\\,", "[");
        newtext = newtext.replaceAll("\n", "");
        newtext = newtext + "]";
        FileWriter f = new FileWriter(casefile, true);
        f.write(newtext);
        f.flush();
        f.close();

        JSONObject jobj = new JSONObject();
        jobj.put("name", className);
        jobj.put("description", description);
        jobj.put("tags", "");
        if (newtext.contains("Fail")) {
            classStatus = "Fail";
        } else {
            classStatus = "Pass";
        }
        jobj.put("status", classStatus);
        jobj.put("automated", "true");

        line = "";
        oldtext = "";
        while ((line = scenreader.readLine()) != null) {
            oldtext += line + "\r";
        }
        FileWriter scenf = new FileWriter(scenFile, true);
        if (!oldtext.contains("scenarios: [")) {
            scenf.write("scenarios: [\n");
        }
        scenf.write(jobj.toString().replaceAll("\\}", "") + "," + newtext + "\n},");
        scenf.flush();
        scenf.close();
        //casefile.delete();
    }

    @AfterSuite (alwaysRun = true)
    public void prepareFinalReport() throws IOException, JSONException,java.lang.Exception {
        auto_finish = System.currentTimeMillis();

        String testEnvtString =  setTestEnvtDetails(auto_start,auto_finish,Execution_start_time);
        testEnvtString = testEnvtString.toString().replaceAll("\"values\"","values").replaceAll("\\[\\{","\\[").replaceAll("\\}\\]","\\]").replaceAll("\":\"", ":");

        UpdateEnvtParams(testEnvtString, portalParam.custom_report_location+ "header_template.txt");
        System.out.println(testEnvtString);
        logger.info("Calling After Suite for preparing html report.");

        if((portal_feafile.isFile()) && (portal_scenfile.isFile()))
        {
            BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
            BufferedReader portal_scenreader = new BufferedReader(new FileReader(portal_scenfile));

            String line = "", oldtext = "";
            while((line = portal_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","Portal");
            fobj.put("description","CE Portal Test Cases");

            FileWriter feaf = new FileWriter(portal_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = portal_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            portal_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(portal_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }

        /*if((api_feafile.isFile()) && (api_scenfile.isFile()))
        {
            BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));
            BufferedReader api_scenreader = new BufferedReader(new FileReader(api_scenfile));

            String line = "", oldtext = "";
            while((line = api_scenreader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_scenreader.close();
            //String newfeatext = oldtext.replaceAll("\\}\\,\\r",",\n");
            JSONObject fobj = new JSONObject();
            fobj.put("name","ProjectTest_Api");
            fobj.put("description","ProjectTest API Test Cases");

            FileWriter feaf = new FileWriter(api_feafile,true);
            feaf.write(fobj.toString() + ",\n"+ oldtext);
            feaf.flush();
            feaf.close();

            oldtext = "";
            while((line = api_feareader.readLine()) != null)
            {
                oldtext += line + "\r";
            }
            api_feareader.close();
            oldtext = oldtext.replaceAll("\\\"\\}\\,\\r", "\\\",\n");
            oldtext = oldtext.replaceAll("\\^\\,", "");
            String finalFeatureText = oldtext + "]},";
            FileWriter finalfeaf = new FileWriter(api_feafile);
            finalfeaf.write(finalFeatureText);
            finalfeaf.flush();
            finalfeaf.close();
        }
*/
        //casefile.delete();

        BufferedReader portal_feareader = new BufferedReader(new FileReader(portal_feafile));
        BufferedReader api_feareader = new BufferedReader(new FileReader(api_feafile));

        String oldtext="",line="";
        while((line = api_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }
        while((line = portal_feareader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        portal_feareader.close();
        api_feareader.close();
        String[] parts = oldtext.split("features: \\[", 2);
        oldtext = parts[0] + parts[1].replaceAll("features: \\[", "");

        FileWriter finalfeaf = new FileWriter(feafile,true);
        finalfeaf.write(oldtext);
        finalfeaf.flush();
        finalfeaf.close();

        File htmlReportFile = new File(portalParam.custom_report_location + "CE_Automation.html");
        File reportHeader = new File(portalParam.custom_report_location + "header_template.txt");
        File reportFeature = new File(portalParam.custom_report_location + "feature.json");
        File reportTrailer = new File(portalParam.custom_report_location + "trailor_template.txt");
        oldtext = "";line="";
        BufferedReader reportReader = new BufferedReader(new FileReader(reportHeader));
        while((line = reportReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader featureReader = new BufferedReader(new FileReader(reportFeature));
        while((line = featureReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        BufferedReader trailerReader = new BufferedReader(new FileReader(reportTrailer));
        while((line = trailerReader.readLine()) != null)
        {
            oldtext += line + "\r";
        }

        htmlReportFile.createNewFile();

        FileWriter htmlf = new FileWriter(htmlReportFile);
        htmlf.write(oldtext);
        htmlf.flush();
        htmlf.close();
        UpdateEnvtParams("ENVT_DETAILS", portalParam.custom_report_location + "header_template.txt");
    }

    public void UpdateEnvtParams(String testEnvtString,String fileName){
        try
        {
            String newtext=null;
            File file = new File(fileName);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "", oldtext = "";
            while((line = reader.readLine()) != null)
            {
                oldtext += line + "";
            }
            reader.close();
            // replace a word in a file
            //String newtext = oldtext.replaceAll("drink", "Love");

            //To replace a line in a file
            if(!testEnvtString.equalsIgnoreCase("ENVT_DETAILS")) {
                newtext = oldtext.replaceAll("ENVT_DETAILS", testEnvtString);
            }
            else {
                newtext = oldtext.replaceAll(testEnvtString, "ENVT_DETAILS");
            }
            FileWriter writer = new FileWriter(fileName);
            writer.write(newtext);writer.close();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

    }

    public String setTestEnvtDetails(long startTime,long endTime,String execution_start_time) throws java.lang.Exception{
        JSONObject jo = new JSONObject();
        jo.put("ProjectName","CE-Automation");
        jo.put("Operating System",System.getProperty("os.name").toLowerCase());
        jo.put("Testing environment",PortalFuncUtils.removeSpecialChar(System.getProperty("envParam")).replaceAll("\\\\",""));
        jo.put("Date",execution_start_time);
        jo.put("Total Execution Time",toHHMMDD(endTime - startTime));

        JSONArray ja = new JSONArray();
        ja.put(jo);

        JSONObject mainObj = new JSONObject();
        mainObj.put("values", ja);
        return mainObj.toString();
    }

    public String toHHMMDD(long time){

        String hms = String.format("%02d"+ " hrs" +":%02d"+ " mins" + ":%02d"+ " sec", TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
        return hms;
    }

    public boolean isPopedUp() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }   // catch
    }

    public String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z]");
        Matcher matcher = pattern.matcher(s);
        String str = matcher.replaceAll("");
        return str;
    }

    /*public DocusignPage readGmailAndDocusign(WebDriver driver, String mainTab) throws Exception {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.get("https://accounts.google.com/ServiceLogin?");
        loginToGmail(portalParam.gmailId,portalParam.gmailPassword);
        if(driver.getCurrentUrl().equalsIgnoreCase("https://myaccount.google.com/?pli=1"))
        {
            driver.get("https://mail.google.com/mail/u/0/#inbox");
        }
        *//*driver.findElement(By.xpath("*//*//**//*[@title='Google apps']")).click();
        driver.findElement(By.id("gb23")).click();*//*
        List<WebElement> unreademail = driver.findElements(By.xpath("/*//*[@class='zF']"));
        //String docittMailer = getMailSender(System.getProperty("envParam"));
        String docittMailer = "Sigma via DocuSign";
        for(int i=0;i<unreademail.size();i++) {
            if (unreademail.get(i).isDisplayed() == true) {
                // Verify whether email received from Docitt
                if (unreademail.get(i).getText().equals(docittMailer)) {
                    //driver.get(baseUrl + "/mail/#inbox");
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("./*//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")));
                    driver.findElement(By.xpath("./*//*[@id=':3a']/span[contains(text(),'"+docittMailer+"')]")).click();
                    String gmailTab = driver.getWindowHandle();
                    driver.findElement(By.xpath("//a[contains(@href,'https://demo.docusign.net')]")).click();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(gmailTab);
                    if(newTab.size() != 0) {
                        driver.switchTo().window(newTab.get(0));
                    }

                } else {
                    System.out.println("No mail form " + docittMailer);
                }
            }
        }
        return new DocusignPage(driver);
    }
*/
    private void loginToGmail(String id, String password) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        boolean newGmail = driver.findElements( By.xpath("//div[contains(text(),'More options')]") ).size() != 0;
        if(newGmail)
        {
            driver.findElement(By.xpath("//input[@id='identifierId']")).sendKeys(id);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Forgot password?')]"))).isDisplayed();
            driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
            driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
        }
        else {
            driver.findElement(By.id("Email")).sendKeys(id);
            driver.findElement(By.id("next")).click();
            driver.findElement(By.id("Passwd")).sendKeys(password);
            driver.findElement(By.id("signIn")).click();

        }
    }

    public static String getOtp(String mobileNumber)
    {
        MongoClient mongoClient = null;
        mongoClient = new MongoClient(new ServerAddress(PortalParam.mongoHost, Integer.parseInt(PortalParam.mongoPort)));
        DB db = mongoClient.getDB(PortalParam.mongoDb);
        DBCollection collection = db.getCollection(PortalParam.mongoCollection);
        BasicDBObject query = new BasicDBObject();
        query.append("Username", mobileNumber);
        DBObject dbObject = collection.findOne(query);
        return dbObject.get("OtpCode").toString();
    }

    public static String getEmailVerificationCode(String email,String applicationId) throws Exception {
        DBObject dbObject;
        try {
            MongoClient mongoClient = null;
            mongoClient = new MongoClient(new ServerAddress(PortalParam.mongoHost, Integer.parseInt(PortalParam.mongoPort)));
            DB db = mongoClient.getDB("email-verification");
            DBCollection collection = db.getCollection("email-verification");
            BasicDBObject query = new BasicDBObject();
            query.append("Email", email);
            query.append("EntityId", applicationId);
            dbObject = collection.findOne(query);
        }catch (Exception e)
        {
            throw new Exception("OTP can not be retrieved from database.");
        }
        return dbObject.get("Code").toString();

    }

    public static void removeDocumentFromMongo()
    {
        MongoClient mongoClient = null;
        mongoClient = new MongoClient(new ServerAddress("54.227.251.126", 27017));
        DB db = mongoClient.getDB("SimulationServer");
        DBCollection collection = db.getCollection("SimulationServerDocuments");
        BasicDBObject query = new BasicDBObject();
        query.append("ModuleName", "cibil");
        DBCursor cursorDetails = collection.find(query).sort(new BasicDBObject("_id",-1)).limit(1);
        System.out.println("Latest document :" +new JSONObject(cursorDetails.next().toString()));
        collection.remove(cursorDetails.one());
        DBCursor cursorDetails1 = collection.find(query).sort(new BasicDBObject("_id",-1)).limit(1);
        System.out.println("Latest document :" +new JSONObject(cursorDetails1.next().toString()));
    }

    public static String takeScreenShot(WebDriver driver,
                                        String screenShotName) {
        try {
            screenShotName = screenShotName + ".png";
            File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File targetFile = new File(PortalParam.custom_report_location, screenShotName);
            FileUtils.copyFile(screenshotFile, targetFile);
            return screenShotName;
        } catch (Exception e) {
            System.out.println("An exception occured while taking screenshot " + e.getCause());
            return null;
        }
    }
}
