package net.sigmainfo.lf.automation.api.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiPropertiesReader.java
 * Description          : Reads api.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class ApiPropertiesReader {

    @Value(value = "${baseresturl}")
    private String baseresturl;

    @Value(value = "${custom_report_location}")
    private String custom_report_location;

    @Value(value = "${token}")
    private String token;

    @Value(value = "${verificationService}")
    private String verificationService;

    @Value(value = "${cibilService}")
    private String cibilService;

    public String getCibilService() {
        return cibilService;
    }

    public void setCibilService(String cibilService) {
        this.cibilService = cibilService;
    }

    public String getVerificationService() {
        return verificationService;
    }

    public void setVerificationService(String verificationService) {
        this.verificationService = verificationService;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBaseresturl() {
        return baseresturl;
    }

    public void setBaseresturl(String baseresturl) {
        this.baseresturl = baseresturl;
    }

    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }
}
