package net.sigmainfo.lf.automation.api.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : ApiParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */

/**
 *  ALL THE VARIABLES TO BE DECLARED IN THIS CLASS
 */
@Component
public class ApiParam {

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String userType) {
        ApiParam.userType = userType;
    }

    public static String userType;
    public static String custom_report_location;
    public static String baseresturl;
    public static String testid;
    public static Boolean executeTestcase;
    public static String port;
    public static String requestType;
    public static String contentType;
    public static String serviceName;
    public static String token;
    public static String verificationService;
    public static String cibilService;

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ApiParam.token = token;
    }

    public static String getVerificationService() {
        return verificationService;
    }

    public static void setVerificationService(String verificationService) {
        ApiParam.verificationService = verificationService;
    }

    public static String getCustom_report_location() {
        return custom_report_location;
    }

    public static void setCustom_report_location(String custom_report_location) {
        ApiParam.custom_report_location = custom_report_location;
    }

    public static String getBaseresturl() {
        return baseresturl;
    }

    public static void setBaseresturl(String baseresturl) {
        ApiParam.baseresturl = baseresturl;
    }

    public static String getTestid() {
        return testid;
    }

    public static void setTestid(String testid) {
        ApiParam.testid = testid;
    }

    public static Boolean getExecuteTestcase() {
        return executeTestcase;
    }

    public static void setExecuteTestcase(Boolean executeTestcase) {
        ApiParam.executeTestcase = executeTestcase;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        ApiParam.port = port;
    }

    public static String getRequestType() {
        return requestType;
    }

    public static void setRequestType(String requestType) {
        ApiParam.requestType = requestType;
    }

    public static String getContentType() {
        return contentType;
    }

    public static void setContentType(String contentType) {
        ApiParam.contentType = contentType;
    }

    public static String getServiceName() {
        return serviceName;
    }

    public static void setServiceName(String serviceName) {
        ApiParam.serviceName = serviceName;
    }
}
