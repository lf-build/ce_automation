package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 09-02-2018.
 */
public class BorrowerDashboardPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BorrowerDashboardPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerDashboardPage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        logger.info("=========== BorrowerDashboardPage is loaded============");
    }
    public BorrowerDashboardPage(){}
}
