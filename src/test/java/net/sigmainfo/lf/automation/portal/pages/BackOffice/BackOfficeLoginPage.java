package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 22-02-2018.
 */
public class BackOfficeLoginPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BackOfficeLoginPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeLoginPage(WebDriver driver) throws Exception {
        try {
            this.driver = driver;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox));
            logger.info("=========== BackOfficeLoginPage is loaded============");
        }catch (Exception e)
        {
            throw new Exception("BackOfficeLoginPage could not be loaded within time");
        }
    }
    public BackOfficeLoginPage(){}

    public static BackOfficeSearchPage loginToBackOffice(WebDriver driver) throws Exception {
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), PortalParam.backOfficeUsername);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.backOfficePassword));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
        return new BackOfficeSearchPage(driver);
    }
}
