package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.CaptureScreenshot;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeAppDetailsPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeLoginPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeSearchPage;
import net.sigmainfo.lf.automation.portal.pages.BankPortal.BankPortalDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.BankPortal.BankPortalLoginPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerFinalOfferPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerLoginPage;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Listeners;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 08-02-2017.
 */
public class SanityTests extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(SanityTests.class);
    public static String funcMod="Portal";

    @Autowired
    TestResults testResults;

    @Autowired
    ApiFuncUtils apiFuncUtils;

    public SanityTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    /**
     * TestName     : verifyEndToEndFlow
     * Description  : This test case checks health check of actual functionality
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","verifyEndToEndFlow"})
    public void VerifyEndToEndFlow() throws Exception {
        String sTestID = "verifyEndToEndFlow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            portalParam.numberOfAppsWithStatus="";
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.moveApplication(applicationId);
            driver.get(portalParam.borrowerUrl);
            logger.info("Loading borrower url :"+portalParam.borrowerUrl);
            BorrowerFinalOfferPage borrowerFinalOfferPage = BorrowerLoginPage.loginToBorrowerPortal();
            BorrowerDashboardPage borrowerDashboardPage1  = borrowerFinalOfferPage.selectOffer();
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage1 = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage1.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage2 = backOfficeAppDetailsPage2.verifyPostAcceptanceStages(applicationId);
            driver.get(portalParam.bankPortalUrl);
            logger.info("Loading bankportal url :"+portalParam.bankPortalUrl);
            BankPortalDashboardPage bankPortalDashboardPage = BankPortalLoginPage.loginBankPortal();
            BankPortalLoginPage bankPortalLoginPage = bankPortalDashboardPage.approveFunding(applicationId);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : InitialOfferRrejectedInitialEligibility
     * Description  : This test case verifies rejection of the application becasue of initial eligibility
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","InitialOfferRrejectedInitialEligibility"})
    public void VerifyInitialOfferRrejectedInitialEligibility() throws Exception {
        String sTestID = "InitialOfferRrejectedInitialEligibility";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String errorMessage =  borrowerHomePage.submitApplication(driver,sTestID);
            assertTrue(errorMessage.equalsIgnoreCase("Your application does not meet our minimum qualifying criteria."),"Error message is not seen or changed.");
            logger.info("Error message captured:"+errorMessage);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : InitialOfferFailedCibilFailure
     * Description  : This test case verifies failure of the offer because of Cibil Failure
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","InitialOfferFailedCibilFailure"})
    public void VerifyInitialOfferFailedCibilFailure() throws Exception {
        String sTestID = "InitialOfferFailedCibilFailure";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String errorMessage =  borrowerHomePage.submitApplication(driver,sTestID);
            assertTrue(errorMessage.equalsIgnoreCase("Oops! Something went wrong, we are unable to generate your offer as of now. Please try again later."),"Error message is not seen or changed.");
            logger.info("Error message captured:"+errorMessage);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : RejectAppCibilReport
     * Description  : This test case verifies rejection of the application because of Cibil report
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","RejectAppCibilReport"})
    public void VerifyRejectAppCibilReport() throws Exception {
        String sTestID = "RejectAppCibilReport";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String errorMessage =  borrowerHomePage.submitApplication(driver,sTestID);
            assertTrue(errorMessage.equalsIgnoreCase("Your application does not meet our minimum qualifying criteria."),"Error message is not seen or changed.");
            logger.info("Error message captured:"+errorMessage);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchFromMobile(PortalParam.mobile);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.verifyErrorMessage(sTestID);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : RejectAppBureauScore
     * Description  : This test case verifies rejection of the application because of bureau score
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","RejectAppBureauScore"})
    public void VerifyRejectAppBureauScore() throws Exception {
        String sTestID = "RejectAppBureauScore";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.verifyErrorMessage(sTestID);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : verifyNotInterestedApp
     * Description  : This test case verifies not interested application
     * Owner        : Shaishav S
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","verifyNotInterestedApp"})
    public void VerifyNotInterestedApp() throws Exception {
        String sTestID = "verifyNotInterestedApp";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            portalParam.numberOfAppsWithStatus="";
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.rejectApp(applicationId);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : FinalOfferManualCashFlow
     * Description  : This test case verifies final offer manual cashflow
     * Owner        : Thangaraj C
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","FinalOfferManualCashFlow"})
    public void FinalOfferManualCashFlow() throws Exception {
        String sTestID = "FinalOfferManualCashFlow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.manualCashFlowGenerateOffer(applicationId);
        }catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : VerifyPerfiosfailedFlow
     * Description  : This test case verifies Perfios flow failure
     * Owner        : Thangaraj C
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","VerifyPerfiosfailedFlow"}, enabled=true)
    public void VerifyPerfiosfailedFlow() throws Exception {
        String sTestID = "VerifyPerfiosfailedFlow";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
            logger.info("ApplicationId generated is:"+applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.verifyErrorMessage(sTestID);
            result = "Passed";
        } catch (Exception e) {
            assertFalse(result.contains("Passed"),"Testcase fails.");
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    /**
     * TestName     : unclassifiedCompany_manualreview
     * Description  : This test case verifies manual review for unclassified company
     * Owner        : Thangaraj C
     *
     */

    @Test(description = "", groups = {"PortalTests","Sanity","unclassifiedCompany_manualreview"}, enabled=true)
    public void unclassifiedCompany_manualreview() throws Exception {
        String sTestID = "unclassifiedCompany_manualreview";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
            logger.info("ApplicationId generated is:"+applicationId);
            BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
            logger.info("ApplicationId generated is:"+applicationId);
            driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.verifyErrorMessage(sTestID);
            result = "Passed";
        } catch (Exception e) {
            assertFalse(result.contains("Passed"),"Testcase fails.");
            logger.info("******************" + sTestID + "  failed. *****\n" + e);
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

}
