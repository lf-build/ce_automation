package net.sigmainfo.lf.automation.portal.pages.BankPortal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 07-03-2018.
 */
public class BankPortalLoginPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BankPortalLoginPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BankPortalLoginPage(WebDriver driver) throws Exception {
        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_loginPage_passwordTextBox));
        logger.info("=========== BankPortalLoginPage is loaded============");
    }
    public BankPortalLoginPage(){}

    public static BankPortalDashboardPage loginBankPortal() throws Exception {
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_loginPage_usernameTextBox), PortalParam.bankPortalUsername);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_loginPage_passwordTextBox), StringEncrypter.createNewEncrypter().decrypt(PortalParam.bankPortalPassword));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.bankPortal_loginPage_loginButton), "SignIn");
        return new BankPortalDashboardPage(driver);
    }
}
