package net.sigmainfo.lf.automation.portal.constant;

import org.springframework.stereotype.Component;

/**
 * Created by shaishav.s on 09-05-2017.
 */
@Component
public class UIObjParam {

    public static String backoffice_loginPage_usernameTextBox;
    public static String backoffice_loginPage_passwordTextBox;
    public static String backoffice_loginPage_loginButton;
    public static String borrower_loanAmountpage_howMuchDoYouNeedLabel;
    public static String borrower_loanAmountpage_loanDetailsMenu;
    public static String borrower_loanAmountpage_nextButton;
    public static String borrower_loanAmountpage_amountTextBox;
    public static String borrower_loanPurposepage_needPurposeLabel;
    public static String borrower_loanPurposepage_travelButton;
    public static String borrower_loanPurposepage_vehiclePurposeButton;
    public static String borrower_loanPurposepage_medicalButton;
    public static String borrower_loanPurposepage_loanRefinanceButton;
    public static String borrower_loanPurposepage_weddingButton;
    public static String borrower_loanPurposepage_homeImprovementButton;
    public static String borrower_loanPurposepage_nextButton;
    public static String borrower_profilePage_personalInfoLabel;
    public static String borrower_profilePage_mrTitleButton;
    public static String borrower_profilePage_msTitleButton;
    public static String borrower_profilePage_mrsTitleButton;
    public static String borrower_profilePage_maritalStatusSingleButton;
    public static String borrower_profilePage_maritalStatusMarriedButton;
    public static String borrower_profilePage_firstNameTextBox;
    public static String borrower_profilePage_middleNameTextBox;
    public static String borrower_profilePage_lastNameTextBox;
    public static String borrower_profilePage_emailIdTextBox;
    public static String borrower_profilePage_nextButton;
    public static String borrower_accountCreationpage_mobileVerificationLabel;
    public static String borrower_accountCreationpage_mobileNumberTextBox;
    public static String borrower_accountCreationpage_nextButton;
    public static String borrower_accountCreationpage_otpSentLabel;
    public static String borrower_accountCreationpage_otpTextBox;
    public static String borrower_accountCreationpage_otpNextButton;
    public static String borrower_employmentDetailspage_legalBusinessTextBox;
    public static String borrower_employmentDetailspage_takeHomeSalaryTextBox;
    public static String borrower_employmentDetailspage_officialEmailIdTextBox;
    public static String borrower_employmentDetailspage_nextButton;
    public static String borrower_expensesPage_residenseTypeDropdown;
    public static String borrower_expensesPage_nextButton;
    public static String borrower_identificationDetails_dateDropdown;
    public static String borrower_identificationDetails_monthDropdown;
    public static String borrower_identificationDetails_yearDropdown;
    public static String borrower_identificationDetails_idDetailsLabel;
    public static String borrower_identificationDetails_aadharNumberTextBox;
    public static String borrower_identificationDetails_panNumberTextBox;
    public static String borrower_identificationDetails_address1TextBox;
    public static String borrower_identificationDetails_address2TextBox;
    public static String borrower_identificationDetails_localityTextBox;
    public static String borrower_identificationDetails_pincodeTextBox;
    public static String borrower_identificationDetails_cityTextBox;
    public static String borrower_identificationDetails_stateTextBox;
    public static String borrower_identificationDetails_bothAddressSameButton;
    public static String borrower_identificationDetails_bothAddressDifferentButton;
    public static String borrower_identificationDetails_nextButton;
    public static String borrower_reviewPage_reviewApplicationLabel;
    public static String borrower_reviewPage_nextButton;
    public static String borrower_reviewPage_agreementCheckbox;
    public static String borrower_employmentDetailspage_whatAboutWorkLabel;
    public static String borrower_expensesPage_expensesLabel;
    public static String borrower_eligibilityPage_greetLabel;
    public static String borrower_eligibilityPage_applicationNumberLabel;
    public static String borrower_eligibilityPage_nextButton;
    public static String borrower_bankVerificationPage_connectAccountRadioButton;
    public static String borrower_bankVerificationPage_uploadFileRadioButton;
    public static String borrower_bankVerificationPage_continueButton;
    public static String borrower_bankVerificationPage_uploadBankStatement_bankTextBox;
    public static String borrower_bankVerificationPage_uploadBankStatement_uploadFileArea;
    public static String borrower_bankVerificationPage_uploadBankStatement_nextButton;
    public static String borrower_educationPage_educationLevelDropdown;
    public static String borrower_educationPage_educationalInstituteTextBox;
    public static String borrower_educationPage_nextButton;
    public static String borrower_employmentPage_employerName;
    public static String borrower_employmentPage_designationTextBox;
    public static String borrower_employmentPage_workExperienceDropdown;
    public static String borrower_employmentPage_address1TextBox;
    public static String borrower_employmentPage_localityTextBox;
    public static String borrower_employmentPage_pincodeTextBox;
    public static String borrower_employmentPage_nextButton;
    public static String borrower_socialPage_skipButton;
    public static String borrower_appComplete_message;
    public static String borrower_appComplete_getUniqueReferralButton;
    public static String borrower_logoutLink;
    public static String borrower_socialPage_verifyMeImage;
    public static String backoffice_searchPage_profileCircle;
    public static String backoffice_searchPage_applicationNumberTextBox;
    public static String backoffice_searchPage_searchButton;
    public static String backoffice_searchPage_resultData_appId;
    public static String backoffice_searchPage_resultData_appDate;
    public static String backoffice_searchPage_resultData_applicantName;
    public static String backoffice_searchPage_resultData_phoneNumber;
    public static String backoffice_searchPage_resultData_loanAmount;
    public static String backoffice_searchPage_resultData_employerName;
    public static String backoffice_searchPage_resultData_expiryDate;
    public static String backoffice_searchPage_resultData_source;
    public static String backoffice_searchPage_resultData_detailsButton;
    public static String backoffice_appDetailsPage_headerStatus;
    public static String backoffice_appDetailsPage_verificationDashboardTab;
    public static String backoffice_appDetailsPage_actionButton;
    public static String backoffice_appDetailsPage_actions_addManualOfferButton;
    public static String backoffice_appDetailsPage_actions_presentFinalOfferButton;
    public static String backoffice_appDetailsPage_actions_addManualOffer_termDropdown;
    public static String backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox;
    public static String backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox;
    public static String backoffice_appDetailsPage_actions_addManualOffer_emiTextBox;
    public static String backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox;
    public static String backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox;
    public static String backoffice_appDetailsPage_actions_addManualOffer_submitButton;
    public static String backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton;
    public static String backoffice_searchPage_profile_logoutButton;
    public static String borrower_loginPage_mobileNumberTextBox;
    public static String borrower_loginPage_otpTextBox;
    public static String borrower_finalOfferPage_loanRadioButton;
    public static String borrower_finalOfferPage_loanAmountLabel;
    public static String borrower_finalOfferPage_durationLabel;
    public static String borrower_finalOfferPage_emiLabel;
    public static String borrower_finalOfferPage_processingFeeLabel;
    public static String borrower_finalOfferPage_nextButton;
    public static String borrower_finalOfferPage_appReceivedMsg;
    public static String borrower_loginPage_sendOtpLink;
    public static String borrower_loginPage_signInButton;
    public static String borrower_socialPage_nextButton;
    public static String borrower_socialPage_verificationDoneMsg;
    public static String borrower_loginPage_loginButton;
    public static String borrower_finalOfferPage_readyForDisbursementMsg;
    public static String backoffice_appDetailsPage_personalInfoTab_personalInfoLabel;
    public static String backoffice_actionCenter;
    public static String backoffice_appDetailsPage_actions_uploadPendingDocument;
    public static String backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton;
    public static String backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton;
    public static String backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton;
    public static String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton;
    public static String backoffice_appDetailsPage_bottomTabs_documents;
    public static String backoffice_appDetailsPage_bottomTabs_documents_documentsLabel;
    public static String backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox;
    public static String backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox;
    public static String backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox;
    public static String backoffice_appDetailsPage_actions_updateBankDetails_submitButton;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox;
    public static String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton;
    public static String backoffice_appDetailsPage_actions_updateBankDetails;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_submitButton;
    public static String backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup;
    public static String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail;
    public static String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox;
    public static String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown;
    public static String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox;
    public static String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton;
    public static String backoffice_appDetailsPage_action_addFOIRCalculation;
    public static String backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox;
    public static String backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox;
    public static String backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox;
    public static String backoffice_appDetailsPage_action_addFOIRCalculation_submitButton;
    public static String backoffice_appDetailsPage_action_addDeviation;
    public static String backoffice_appDetailsPage_action_addDeviation_deviationTextbox;
    public static String backoffice_appDetailsPage_action_addDeviation_submitButton;
    public static String backoffice_appDetailsPage_action_documentCollectionSource;
    public static String backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio;
    public static String backoffice_appDetailsPage_action_documentCollectionSource_submitButton;
    public static String backoffice_appDetailsPage_action_completeFulfillment;
    public static String backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton;
    public static String backoffice_appDetailsPage_action_sendToBank;
    public static String backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton;
    public static String bankPortal_loginPage_usernameTextBox;
    public static String bankPortal_loginPage_passwordTextBox;
    public static String bankPortal_loginPage_loginButton;
    public static String bankPortal_dashboard_pendingBankFraudReviewTab;
    public static String bankPortal_dashboard_profileCircle;
    public static String bankPortal_dashboard_profile_logoutButton;
    public static String bankPortal_dashboard_resultAppsMsg;
    public static String bankPortal_appDetailsPage_actionButton;
    public static String bankPortal_appDetailsPage_headerStatus;
    public static String bankPortal_appDetailsPage_action_approveApplication;
    public static String bankPortal_appDetailsPage_action_approveApplication_approveAppButton;
    public static String bankPortal_appDetailsPage_action_approveCreditApplication;
    public static String bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton;
    public static String bankPortal_dashboard_pendingBankCreditReviewTab;
    public static String bankPortal_dashboard_pendingFunding;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton;
    public static String bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox;
    public static String borrower_reviewPage_minimumCriteriaNoMatchMsg;
    public static String borrower_eligibilityPage_sorryLabel;
    public static String borrower_eligibilityPage_unableToGenerateOfferMsg;
    public static String backoffice_searchPage_applicantPhoneTextBox;
    public static String borrower_logoutButton;
    public static String backoffice_appDetailsPage_actions_notInterestedButton;
    public static String backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown;
    public static String backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton;
    public static String backoffice_appDetailsPage_updateCashFlow;
    public static String backoffice_appDetailsPage_updateCashFlow_mothlyEMI;
    public static String backoffice_appDetailsPage_updateCashFlow_monthlyExpense;
    public static String backoffice_appDetailsPage_updateCashFlow_monthlyIncome;
    public static String backoffice_appDetailsPage_updateCashFlow_mothlyInvestment;
    public static String backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance;
    public static String backoffice_appDetailsPage_updateCashFlow_noOfECSbounce;
    public static String backoffice_appDetailsPage_updateCashFlow_salarayDate;
    public static String backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount;
    public static String backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit;
    public static String backoffice_appDetails_getFinalOffer;
    public static String backoffice_appDetails_submitGetFinalOffer;
    public static String backoffice_appDetails_selectFinalOffer;
    public static String backoffice_appDetails_submitSelectFinalOffer;
	public static String backoffice_searchPage_createNewuser;
	public static String backoffice_userAdministrationPage_createNewUser_fullName;
	public static String backoffice_userAdministrationPage_createNewUser_userName;
	public static String backoffice_userAdministrationPage_createNewUser_password;
	public static String backoffice_userAdministrationPage_createNewUser_email;
	public static String backoffice_userAdministrationPage_createNewUser_submitNewUser;
	public static String backoffice_searchPage_allUser;
	public static String backoffice_userAdministrationPage_allUser_pageNavigation;
	public static String backoffice_userAdministrationPage_editUser_editButton;
	public static String backoffice_userAdministrationPage_editUser_updateDetailsButton;
	public static String backoffice_userAdministrationPage_editUser_emailInfo;
	public static String backoffice_userAdministrationPage_deActivateButton;
	public static String backoffice_userAdministrationPage_userSettings;
	public static String backoffice_userAdministrationPage_userSettingsLogout;
	
	public static String backoffice_searchPage_applicationPANTextBox;
	public static String backoffice_searchPage_applicationEmailTextBox;
	public static String backoffice_searchPage_applicationNameTextBox;
	public static String backoffice_searchPage_applicationEmployerTextBox;
	
	public static String backoffice_searchPage_resultDataFetch_appId;
	public static String backoffice_searchPage_resultDataFetch_mobile;
	public static String backoffice_searchPage_resultDataFetch_pan;
	public static String backoffice_searchPage_resultDataFetch_email;
	public static String backoffice_searchPage_resultDataFetch_name;
	public static String backoffice_searchPage_resultDataFetch_employername;
	public static String backoffice_appDetailsPage_actions_assign_role;
	public static String backoffice_appDetailsPage_actions_assign_user;
	public static String backoffice_appDetailsPage_actions_assign_notes;
	public static String backoffice_appDetailsPage_actions_assign_submitButton;
	public static String backoffice_appDetailsPage_actions_assign_leftAssignMenu;
	public static String backoffice_searchPage_assignedResultData_appId;
	public static String backoffice_appDetailsPage_actions_assign_rightAssignMenu;
	public static String backoffice_appDetailsPage_actions_claim_rightClaimMenu;
	public static String backoffice_appDetailsPage_actions_claim_role;
	public static String backoffice_appDetailsPage_actions_claim_submitButton;
	public static String backoffice_searchPage_assignedResultData_appIdDetailsBtn;
	public static String backoffice_appDetailsPage_actions_assignments_BottomMenu;
	public static String backoffice_appDetailsPage_actions_assignments_BottomValueOne;
	public static String backoffice_appDetailsPage_actions_assignments_BottomValueTwo;
	public static String backoffice_appDetailsPage_actions_assignments_releaseBtn;
	public static String backoffice_appDetailsPage_actions_assignments_releaseSubBtn;
	public static String backoffice_appDetailsPage_actions_assignments_releaseFinalBtn;
	
	public static String backoffice_searchPage_failedSubmission;
	public static String backoffice_appDetailsPage_failedSubmission_selectDetailsApp;
	public static String backoffice_appDetailsPage_failedSubmission_updateEduDetails;
	public static String backoffice_appDetailsPage_failedSubmission_eduQualification;
	public static String backoffice_appDetailsPage_failedSubmission_eduInstitution;
	public static String backoffice_appDetailsPage_failedSubmission_eduSubmitBtn;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpDetails;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpDesignation;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpEmail;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpIncome;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpAddress1;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpAddress2;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpLocation;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpPincode;
	public static String backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn;
	public static String backoffice_appDetailsPage_failedSubmission_updatePersonalDetails;
	public static String backoffice_appDetailsPage_failedSubmission_updatePersonalDOB;
	public static String backoffice_appDetailsPage_failedSubmission_updatePersonalPAN;
	public static String backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber;
	public static String backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressDetails;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressAdd1;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressAdd2;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressLoc;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressPincode;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressChkBox;
	public static String backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeDetails;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeResidency;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance;
	public static String backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn;
	
	public static String backoffice_searchPage_pendinginitialoffer;
	public static String backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp;
	public static String backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer;	
	public static String backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer;
	public static String backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn;
	
	public static String backoffice_searchPage_pendingbankstmt;
	public static String backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp;
	
	public static String backoffice_searchPage_pendingGetFinalOffer;
	public static String backoffice_searchPage_manualReviewOffer;
	
	
}
