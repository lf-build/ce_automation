package net.sigmainfo.lf.automation.portal.tests;

import java.io.IOException;

import org.json.JSONException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeAppDetailsPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeLoginPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeSearchPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeUserAdministration;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;

public class UserAdministrationTests extends AbstractTests {
	
	 private Logger logger = LoggerFactory.getLogger(UserAdministrationTests.class);
	    public static String funcMod="Portal";

	    @Autowired
	    TestResults testResults;

	    @Autowired
	    ApiFuncUtils apiFuncUtils;

	    public UserAdministrationTests() {}

	    @AfterClass(alwaysRun=true)
	    public void endCasereport() throws IOException, JSONException {

	        String funcModule = "Portal";
	        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
	        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
	        generateReport(className, description,funcModule);
	    }
	    
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","newUserCreation"}, enabled=true)
	    public void newUserCreation() throws Exception {
	        String sTestID = "newUserCreation";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	       // initializeData(funcMod,sTestID);
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeUserAdministration = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeUserAdministration backOfficeUserAdministration2 = backOfficeUserAdministration.moveTocreateNewUser();
	            BackOfficeUserAdministration backOfficeUserAdministration3 = backOfficeUserAdministration2.createNewUser();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","VerifyListAllUser"}, enabled=true)
	    public void VerifyListAllUser() throws Exception {
	        String sTestID = "VerifyListAllUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeUserAdministration = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeUserAdministration backOfficeUserAdministration2 = backOfficeUserAdministration.moveToAllUser();
	            BackOfficeUserAdministration backOfficeUserAdministration3 = backOfficeUserAdministration2.verifyUserList();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	 
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","editUserCreation"}, enabled=true)
	    public void editUserCreation() throws Exception {
	        String sTestID = "editUserCreation";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	       // initializeData(funcMod,sTestID);
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeUserAdministration = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeUserAdministration backOfficeUserAdministration2 = backOfficeUserAdministration.moveToAllUser();
	            BackOfficeUserAdministration backOfficeUserAdministration3 = backOfficeUserAdministration2.editUser();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    
	    
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","VerifyUserDeactivation"}, enabled=true)
	    public void VerifyUserDeactivation() throws Exception {
	        String sTestID = "VerifyUserDeactivation";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	       // initializeData(funcMod,sTestID);
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeUserAdministration = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeUserAdministration backOfficeUserAdministration2 = backOfficeUserAdministration.moveToAllUser();
	            BackOfficeUserAdministration backOfficeUserAdministration3 = backOfficeUserAdministration2.userDeactivation();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	   
	    
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","VerifyUserActivation"}, dependsOnMethods={"VerifyUserDeactivation"}, enabled=true)
	    public void VerifyUserActivation() throws Exception {
	        String sTestID = "VerifyUserActivation";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	       // initializeData(funcMod,sTestID);
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeUserAdministration = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeUserAdministration backOfficeUserAdministration2 = backOfficeUserAdministration.moveToAllUser();
	            BackOfficeUserAdministration backOfficeUserAdministration3 = backOfficeUserAdministration2.userActivation();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
		
	    @Test(description = "", groups = {"PortalTests","UserAdministrationTests","VerifySearchApplication"},enabled=true)
	    public void VerifySearchApplication() throws Exception {
	        String sTestID = "VerifySearchApplication";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	       // initializeData(funcMod,sTestID);
	        try {
	             driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.applicationSearch();
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }	    
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","AssignApplication2SingleUser"},enabled=true)
	    public void AssignApplication2SingleUser() throws Exception {
	        String sTestID = "AssignApplication2SingleUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.AssignApplicationtoSingleUser(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","AssignApplication2MultiUser"},enabled=true)
	    public void AssignApplication2MultiUser() throws Exception {
	        String sTestID = "AssignApplication2MultiUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId = borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.AssignApplicationtoMultiUser(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","UnAssignApplicationMultiUser"},enabled=true)
	    public void UnAssignApplicationMultiUser() throws Exception {
	        String sTestID = "UnAssignApplicationMultiUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId = borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.UnAssignApplicationMultiUser(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","ClaimApplication2SingleUser"},enabled=true)
	    public void ClaimApplication2SingleUser() throws Exception {
	        String sTestID = "ClaimApplication2SingleUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId = borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.ClaimApplicationToSingleUser(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","ClaimApplication2MultiUser"},enabled=true)
	    public void ClaimApplication2MultiUser() throws Exception {
	        String sTestID = "ClaimApplication2MultiUser";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId = borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.ClaimApplicationToMultiUser(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }
	    
	    @Test(description = "", groups = {"PortalTests","Sanity","ReleaseClaimApplication"},enabled=true)
	    public void ReleaseClaimApplication() throws Exception {
	        String sTestID = "ReleaseClaimApplication";
	        String result = "Failed";
	        Boolean isDeclined=false;
	        Boolean isManualUpload=false;
	        String appId;
	        WebDriverWait wait = new WebDriverWait(driver,60);
	        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
	        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
	        initializeData(funcMod,sTestID);
	        try {
	            BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
	            String applicationId = borrowerHomePage.submitApplication(driver,sTestID);
	            logger.info("ApplicationId generated is:"+applicationId);	       
	        	driver.get(portalParam.backOfficeUrl);
	            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
	            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
	            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
	            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.verifyReleaseClaimApplication(applicationId);
	            result = "Passed";
	        } catch (Exception e) {
	            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
	        } finally {
	            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
	            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
	            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
	            writeToReport(funcMod,sTestID, result);
	        }
	    }

		  
}
