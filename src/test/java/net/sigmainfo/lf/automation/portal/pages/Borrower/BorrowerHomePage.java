package net.sigmainfo.lf.automation.portal.pages.Borrower;

import com.google.common.base.Predicate;
import junit.framework.Assert;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sound.sampled.Port;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 06-10-2017.
 */
public class BorrowerHomePage extends AbstractTests {

    ApiFuncUtils apiFuncUtils = new ApiFuncUtils();

    private Logger logger = LoggerFactory.getLogger(BorrowerHomePage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerHomePage(WebDriver driver) throws Exception {
        try {
            this.driver = driver;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_howMuchDoYouNeedLabel));
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_loanDetailsMenu));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_nextButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_amountTextBox));
            logger.info("=========== BorrowerHomePage is loaded============");
        }catch (Exception e)
        {
            throw new Exception("BorrowerHomePage could not be loaded within time");
        }
    }
    public BorrowerHomePage(){}

    public String submitApplication(WebDriver driver,String sTestID) throws Exception {
        String msg=null;

        PortalFuncUtils.waitForPageToLoad(driver);
            try {
                enterLoanDetails();

            }catch (Exception e){
                throw new Exception();
            }
            if(sTestID.contains("InitialOfferRrejectedInitialEligibility")) {
                msg = enterPersonalDetails(sTestID);
            }
            else if((sTestID.contains("verifyEndToEndFlow") || (sTestID.contains("createMultipleApps") || (sTestID.contains("createMultipleAppsWithStatus")) || (sTestID.contains("verifyNotInterestedApp")) || (sTestID.contains("FinalOfferManualCashFlow")) || (sTestID.contains("VerifyPerfiosfailedFlow")) || (sTestID.contains("unclassifiedCompany_manualreview")) || (sTestID.contains("AssignApplication2SingleUser")))))
            {
                msg = enterPersonalDetails(sTestID);
            }
            else if(sTestID.contains("InitialOfferFailedCibilFailure"))
            {
                msg = enterPersonalDetails(sTestID);
            }
            else if(sTestID.contains("RejectAppCibilReport"))
            {
                msg = enterPersonalDetails(sTestID);
            }
            else if(sTestID.contains("RejectAppBureauScore"))
            {
                msg = enterPersonalDetails(sTestID);
            }
            else
            {
            	 msg = enterPersonalDetails(sTestID);
            }
            return msg;
    }

    public BorrowerDashboardPage verifyApplication(String applicationId) throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadFileRadioButton),"Upload File");
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox),PortalParam.bankName);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadBankStatement_bankTextBox),"Bank Name");
        Thread.sleep(3000);
        if(driver.findElements(By.xpath("//*[contains(text(),'"+PortalParam.bankName+", India')]")).size() > 0)
        {
            driver.findElement(By.xpath("//*[contains(text(),'"+PortalParam.bankName+", India')]")).click();
            logger.info("Selected :"+PortalParam.bankName);
        }
        else
        {
            throw new Exception(PortalParam.bankName + " is not seen for selection");
        }
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadBankStatement_uploadFileArea),"Upload File Area");
        fileupload("BankStatement");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_bankVerificationPage_uploadBankStatement_nextButton),"Next");
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_educationPage_educationLevelDropdown),PortalParam.education);
        Thread.sleep(500);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_educationPage_educationalInstituteTextBox),PortalParam.institutionName);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_educationPage_nextButton),"Next");
        if(!(PortalParam.testId).contains("unclassifiedCompany_manualreview")) {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_employerName)));
        }
        Thread.sleep(2000);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_designationTextBox),PortalParam.designation);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_workExperienceDropdown));
        PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_workExperienceDropdown),PortalParam.totalExperience);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_address1TextBox),PortalParam.employerAddress);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_localityTextBox),PortalParam.empLocality);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_pincodeTextBox),PortalParam.empPincode);
        driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_pincodeTextBox)).sendKeys(Keys.TAB);
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//input[starts-with(@id,'employerAddresscity-PleaseenterPinCodeabove-City') and @value='bangalore']")));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentPage_nextButton),"Next");

        /* THIS TWO LINES ARE COMMENTED BECAUSE DEV TEAM HAS REMOVED VERIFY ME TEMPORARILY */

        /*assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_socialPage_verifyMeImage)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_socialPage_skipButton),"Skip");*/

        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_socialPage_verificationDoneMsg)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_socialPage_nextButton),"Next");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_appComplete_message)));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_logoutLink),"Logout");
        PortalFuncUtils.waitForPageToLoad(driver);
        return new BorrowerDashboardPage(driver);
    }

    private void fileupload(String doc) throws InterruptedException, AWTException {
        String userDirectory = System.getProperty("user.dir");
        userDirectory = userDirectory.replaceAll("/", "\\\\/");

        String filepath = null;
        Thread.sleep(3000);
        if (doc.contains("Payslip")) {
            try {
                String command = PortalParam.upload_file_location + "upload.exe";
                File file = new File(PortalParam.upload_file_location + "PayStub.pdf");
                Runtime.getRuntime().exec(command + " " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            Thread.sleep(3000);
            try {
                String command = PortalParam.upload_file_location + "upload.exe";
                File file = new File(PortalParam.upload_file_location + "BankStatement.pdf");
                Runtime.getRuntime().exec(command + " " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Thread.sleep(3000);
    }

    private String enterPersonalDetails(String sTestID) throws Exception {
            String msg = null;
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_personalInfoLabel));
            if((PortalParam.salutation).contains("Ms")) {
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_msTitleButton), "Ms");
            }
            if((PortalParam.salutation).contains("Mrs")) {
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_mrsTitleButton), "Mrs");
            }
            if((PortalParam.salutation).contains("Mr")) {
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_mrTitleButton), "Mr");
            }
            if((PortalParam.maritalStatus).contains("Married")) {
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_maritalStatusMarriedButton), "Married");
            }
            if((PortalParam.maritalStatus).contains("Single")) {
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_maritalStatusSingleButton), "Single");
            }
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_firstNameTextBox),PortalParam.firstName);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_middleNameTextBox),PortalParam.middleName);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_lastNameTextBox),PortalParam.lastName);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_emailIdTextBox),PortalParam.emailID);
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_nextButton));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_profilePage_nextButton),"Next");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_mobileVerificationLabel)),"Mobile Verification Label is not seen.");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_mobileVerificationLabel));
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_mobileNumberTextBox),PortalParam.mobile);
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_nextButton));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_nextButton),"Send OTP");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_otpSentLabel)),"OTP label is not seen");
            //assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_otpTextBox)));
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_otpTextBox),PortalParam.otp);
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_accountCreationpage_otpNextButton),"Next");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//span[contains(.,'Hello "+PortalParam.firstName+"')]")),"What about work page is not visible");
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_legalBusinessTextBox),PortalParam.legalBusiness);
            if(!(PortalParam.testId).contains("unclassifiedCompany_manualreview")) {
                Thread.sleep(5000);
                if (driver.findElements(By.xpath("//*[contains(text(),'SIGMA INFOSOLUTIONS LIMITED')]")).size() > 0) {
                    driver.findElement(By.xpath("//*[contains(text(),'SIGMA INFOSOLUTIONS LIMITED')]")).click();
                    logger.info("Selected :" + PortalParam.legalBusiness);
                } else {
                    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_legalBusinessTextBox), "Legal Business");
                    Thread.sleep(1000);
                    driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_legalBusinessTextBox)).sendKeys(Keys.BACK_SPACE);
                    Thread.sleep(5000);
                    if (driver.findElements(By.xpath("//*[contains(text(),'SIGMA INFOSOLUTIONS LIMITED')]")).size() > 0) {
                        driver.findElement(By.xpath("//*[contains(text(),'SIGMA INFOSOLUTIONS LIMITED')]")).click();
                        logger.info("Selected :" + PortalParam.legalBusiness);
                    } else {
                        throw new Exception("Can't select options.");
                    }
                }
            }
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_takeHomeSalaryTextBox),PortalParam.salary);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_officialEmailIdTextBox),PortalParam.officialEmailId);
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_nextButton));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_employmentDetailspage_nextButton),"Next");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_expensesPage_expensesLabel)),"Expenses page is not seen");
            PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_expensesPage_residenseTypeDropdown),PortalParam.residenceType);
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_expensesPage_nextButton));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_expensesPage_nextButton),"Next");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_idDetailsLabel)),"Id details page is not seen");
            PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_dateDropdown),PortalParam.dob);
            PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_monthDropdown),PortalParam.mob);
            PortalFuncUtils.selectDropdownByvalue(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_yearDropdown),PortalParam.yob);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_aadharNumberTextBox),PortalParam.aadharNumber);
            //PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_panNumberTextBox));
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_panNumberTextBox),PortalParam.panNumber);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_address1TextBox),PortalParam.address1);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_address2TextBox),PortalParam.address2);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_localityTextBox),PortalParam.locality);
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_pincodeTextBox),PortalParam.pincode);
            driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_pincodeTextBox)).sendKeys(Keys.TAB);
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_nextButton));
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//input[starts-with(@id,'currentResidentialAddresscity-PleaseenterPinCodeabove-City') and @value='bangalore']")));
            if(PortalParam.bothResidenceSame)
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_bothAddressSameButton),"Yes");
            if(!PortalParam.bothResidenceSame)
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_bothAddressSameButton),"No");
            PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_identificationDetails_nextButton));
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_reviewApplicationLabel)),"Review Application page is not seen.");
            Thread.sleep(10000);
            PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_agreementCheckbox));

            if(sTestID.contains("InitialOfferRrejectedInitialEligibility"))
            {
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_sorryLabel));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg)).getText();
            }
            else if((sTestID.contains("verifyEndToEndFlow") || (sTestID.contains("createMultipleApps") || (sTestID.contains("createMultipleAppsWithStatus")) || (sTestID.contains("verifyNotInterestedApp")) || (sTestID.contains("FinalOfferManualCashFlow")) || (sTestID.contains("VerifyPerfiosfailedFlow")) || (sTestID.contains("unclassifiedCompany_manualreview")) || (sTestID.contains("AssignApplication2SingleUser")))))
            {
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_greetLabel));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_applicationNumberLabel)).getText();
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_nextButton),"Next");
            }
            else if(sTestID.contains("InitialOfferFailedCibilFailure")) {
                apiFuncUtils.simulateResponse(PortalParam.mobile);
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_unableToGenerateOfferMsg));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_unableToGenerateOfferMsg)).getText();
            }
            else if(sTestID.contains("RejectAppCibilReport")) {
                apiFuncUtils.rejectAppSimulate(PortalParam.mobile);
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_minimumCriteriaNoMatchMsg)).getText();
            }
            else if(sTestID.contains("RejectAppBureauScore")) {
                apiFuncUtils.rejectAppBureauSimulate(PortalParam.mobile);
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_greetLabel));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_applicationNumberLabel)).getText();
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_nextButton),"Next");
            }
            else
            {
                PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_reviewPage_nextButton));
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_greetLabel));
                msg = driver.findElement(PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_applicationNumberLabel)).getText();
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_eligibilityPage_nextButton),"Next");
         
            }
            return msg;
    }

    private void enterLoanDetails() throws Exception {
        try {
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_amountTextBox),PortalParam.amountNeed);
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_nextButton),"Next");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_needPurposeLabel));
            if(PortalParam.loanPurpose.contains("Travel"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_travelButton),"Travel");
            if(PortalParam.loanPurpose.contains("Vehicle"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_vehiclePurposeButton),"Vehicle Purchase");
            if(PortalParam.loanPurpose.contains("Medical"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_medicalButton),"Medical");
            if(PortalParam.loanPurpose.contains("Refinance"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_loanRefinanceButton),"Loan Refinancing");
            if(PortalParam.loanPurpose.contains("Wedding"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_weddingButton),"Wedding");
            if(PortalParam.loanPurpose.contains("Improvement"))
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_homeImprovementButton),"Home Improvement");
            PortalFuncUtils.scrollToElement(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_nextButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_nextButton));
            PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loanPurposepage_nextButton),"Next");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


}
