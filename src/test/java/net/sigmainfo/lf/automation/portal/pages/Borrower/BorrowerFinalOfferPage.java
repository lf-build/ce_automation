package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 27-02-2018.
 */
public class BorrowerFinalOfferPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BorrowerFinalOfferPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerFinalOfferPage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        PortalFuncUtils.scrollOnTopOfThePage(driver,"Borrower");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_finalOfferPage_readyForDisbursementMsg));
        logger.info("=========== BorrowerFinalOfferPage is loaded============");
    }
    public BorrowerFinalOfferPage(){}

    public BorrowerDashboardPage selectOffer() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_finalOfferPage_loanRadioButton),"Loan");
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_finalOfferPage_nextButton),"Next");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_finalOfferPage_appReceivedMsg));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_logoutLink),"Logout");
        PortalFuncUtils.waitForPageToLoad(driver);
        assertTrue(driver.getCurrentUrl().contains("https://www.qbera.com"),"Not logged out or lending on incorrect page after logging out.");
        return new BorrowerDashboardPage(driver);
    }
}
