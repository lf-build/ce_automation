package net.sigmainfo.lf.automation.portal.constant;

import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalParam.java
 * Description          : Contains all placeholders declared within the test package
 * Includes             : 1. Declares the place holders being used
 *                        2. Contains getter and setter methods
 */
@Component
public class PortalParam {
    public static String backOfficeUrl;
    public static String borrowerUrl;
    public static String custom_report_location;
    public static String bankPortalUrl;
    public static String password;
    public static String backOfficeUsername;
    public static String backOfficePassword;
    public static String browser;
    public static String testId;
    public static String amountNeed;
    public static String loanPurpose;
    public static String salutation;
    public static String maritalStatus;
    public static String firstName;
    public static String middleName;
    public static String lastName;
    public static String emailID;
    public static String mobile;
    public static String otp;
    public static String legalBusiness;
    public static String salary;
    public static String officialEmailId;
    public static String residenceType;
    public static String dob;
    public static String mob;
    public static String yob;
    public static String aadharNumber;
    public static String panNumber;
    public static String address1;
    public static String address2;
    public static String locality;
    public static String pincode;
    public static boolean bothResidenceSame;
    public static String bankName;
    public static String upload_file_location;
    public static String education;
    public static String institutionName;
    public static String designation;
    public static String totalExperience;
    public static String employerAddress;
    public static String empLocality;
    public static String empPincode;
    public static String mongoHost;
    public static String mongoPort;
    public static String mongoDb;
    public static String mongoCollection;
    public static String term;
    public static String loanAmount;
    public static String processingFee;
    public static String emi;
    public static String interestRate;
    public static String processingFeePercentage;
    public static String accountNumber;
    public static String ifscCode;
    public static String community;
    public static String caste;
    public static String kycExpiryDate;
    public static String kycDocNumber;
    public static String motherName;
    public static String currentResidenceYears;
    public static String currentCityYears;
    public static String workExperience;
    public static String kycDocType;
    public static String dependants;
    public static String gurdianName;
    public static String salaryDate;
    public static String narration;
    public static String typeOfLoan;
    public static String obligatedAmount;
    public static String comments;
    public static String deviation;
    public static String docCollectionSource;
    public static String bankPortalPassword;
    public static String bankPortalUsername;
    public static String loanaccountNumber;
    public static String paymentStatus;
    public static String emiAmount;
    public static String utrNumber;
    public static String numberOfApps;
    public static String numberOfAppsWithStatus;
    public static String notInterestedReason;
}
