package net.sigmainfo.lf.automation.portal.tests;

import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.common.TestResults;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.PortalPropertiesReader;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeAppDetailsPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeLoginPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeSearchPage;
import net.sigmainfo.lf.automation.portal.pages.BackOffice.BackOfficeUserAdministration;
import net.sigmainfo.lf.automation.portal.pages.BankPortal.BankPortalDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.BankPortal.BankPortalLoginPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerDashboardPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerFinalOfferPage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerHomePage;
import net.sigmainfo.lf.automation.portal.pages.Borrower.BorrowerLoginPage;
import org.json.JSONException;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 21-03-2018.
 */
public class CEMiscTests extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(CEMiscTests.class);
    public static String funcMod="Portal";

    @Autowired
    TestResults testResults;

    @Autowired
    ApiFuncUtils apiFuncUtils;

    @Autowired
    PortalParam portalParam;

    @Autowired
    PortalPropertiesReader portalPropertiesReader;

    public CEMiscTests() {}

    @AfterClass(alwaysRun=true)
    public void endCasereport() throws IOException, JSONException {

        String funcModule = "Portal";
        String className = org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        String description = "Verify  "+ org.apache.commons.lang.StringUtils.substringAfterLast(this.getClass().getName(), ".");
        generateReport(className, description,funcModule);
    }

    @Test(description = "", groups = {"CEMiscTests","createMultipleApps"},enabled=false)
    public void CreateMultipleApplications() throws Exception {
        String sTestID = "createMultipleApps";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        List<String> applications = new ArrayList<String>();
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        try {
            for(int i = 1; i <=Integer.parseInt(PortalParam.numberOfApps); i++) {
                initializeData(funcMod,sTestID);
                driver.get(portalPropertiesReader.getBorrowerUrl());
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_loanDetailsMenu)), portalPropertiesReader.getBorrowerUrl() +" could not be loaded.");
                BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
                String applicationId = borrowerHomePage.submitApplication(driver, sTestID);
                /*logger.info("ApplicationId generated is:" + applicationId);*/
                assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_logoutButton)));
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_logoutButton),"Logout");
                PortalFuncUtils.waitForPageToLoad(driver);
                assertTrue(driver.getCurrentUrl().contains("https://www.qbera.com"),"Not logged out or lending on incorrect page after logging out.");
                applications.add(applicationId);
            }
            logger.info("Generated Applications are :" +applications);
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

    @Test(description = "", groups = {"CEMiscTests","createMultipleAppsWithStatus"},enabled=false)
    public void CreateMultipleAppsWithStatus() throws Exception {
        String sTestID = "createMultipleAppsWithStatus";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        List<String> applications = new ArrayList<String>();
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        try {
            for(int i = 1; i <=Integer.parseInt(PortalParam.numberOfApps); i++) {
                initializeData(funcMod,sTestID);
                driver.get(portalPropertiesReader.getBorrowerUrl());
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loanAmountpage_loanDetailsMenu)), portalPropertiesReader.getBorrowerUrl() +" could not be loaded.");
                BorrowerHomePage borrowerHomePage = new BorrowerHomePage(driver);
                String applicationId =  borrowerHomePage.submitApplication(driver,sTestID);
                logger.info("ApplicationId generated is:"+applicationId);
                BorrowerDashboardPage borrowerDashboardPage = borrowerHomePage.verifyApplication(applicationId);
                if(!portalParam.numberOfAppsWithStatus.contains("Credit Review")) {
                    driver.get(portalParam.backOfficeUrl);
                    logger.info("Loading backoffice url :" + portalParam.backOfficeUrl);
                    BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
                    BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.searchApp(applicationId);
                    BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.moveApplication(applicationId);
                    if(!portalParam.numberOfAppsWithStatus.contains("Final offer made")) {
                        driver.get(portalParam.borrowerUrl);
                        logger.info("Loading borrower url :" + portalParam.borrowerUrl);
                        BorrowerFinalOfferPage borrowerFinalOfferPage = BorrowerLoginPage.loginToBorrowerPortal();
                        BorrowerDashboardPage borrowerDashboardPage1 = borrowerFinalOfferPage.selectOffer();
                        if(!portalParam.numberOfAppsWithStatus.contains("Final Offer Accepted")) {
                            driver.get(portalParam.backOfficeUrl);
                            logger.info("Loading backoffice url :" + portalParam.backOfficeUrl);
                            BackOfficeSearchPage backOfficeSearchPage1 = BackOfficeLoginPage.loginToBackOffice(driver);
                            BackOfficeAppDetailsPage backOfficeAppDetailsPage2 = backOfficeSearchPage1.searchApp(applicationId);
                            if((!portalParam.numberOfAppsWithStatus.contains("Bank Fraud Review")) || (!portalParam.numberOfAppsWithStatus.contains("CE Approved"))) {
                                BackOfficeLoginPage backOfficeLoginPage2 = backOfficeAppDetailsPage2.verifyPostAcceptanceStages(applicationId);
                                driver.get(portalParam.bankPortalUrl);
                                logger.info("Loading bankportal url :" + portalParam.bankPortalUrl);
                                BankPortalDashboardPage bankPortalDashboardPage = BankPortalLoginPage.loginBankPortal();
                                if((!portalParam.numberOfAppsWithStatus.contains("Bank Credit Review")) || (!portalParam.numberOfAppsWithStatus.contains("Approved")) || (!portalParam.numberOfAppsWithStatus.contains("Funded"))) {
                                    BankPortalLoginPage bankPortalLoginPage = bankPortalDashboardPage.approveFunding(applicationId);
                                }
                            }
                        }
                    }
                }
                applications.add(applicationId);
            }
            result = "Passed";
        } catch (Exception e) {
            logger.info("Generated Applications with status " + portalPropertiesReader.getNumberOfAppsWithStatus() +" are :"+applications);
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("Generated Applications with status " + portalPropertiesReader.getNumberOfAppsWithStatus() +" are :"+applications);
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","FailedSubmissionAppSubmit"},enabled=true)
    public void FailedSubmissionAppSubmit() throws Exception {
        String sTestID = "FailedSubmissionAppSubmit";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.moveToFailedsubmission();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.updateFailedSubmissionApp();
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","SelectInitalOffer"},enabled=true)
    public void SelectInitalOffer() throws Exception {
        String sTestID = "SelectInitalOffer";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.moveToPendingInitialOffersubmission();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.updatePendinginitialOffer();
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","PendingBankStatement"},enabled=true)
    public void PendingBankStatement() throws Exception {
        String sTestID = "PendingBankStatement";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.moveToPendingBankstmtsubmission();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.updatePendingBankstmt();
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","GetFinalOffer"},enabled=true)
    public void GetFinalOffer() throws Exception {
        String sTestID = "GetFinalOffer";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.moveToPendingFinalOffer();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.updateGetFinalOffer();
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }
    
    @Test(description = "", groups = {"PortalTests","ManualReviewOffer"},enabled=true)
    public void ManualReviewOffer() throws Exception {
        String sTestID = "ManualReviewOffer";
        String result = "Failed";
        Boolean isDeclined=false;
        Boolean isManualUpload=false;
        String appId;
        WebDriverWait wait = new WebDriverWait(driver,60);
        testResults.ExportResultToTxt(sTestID + "\t" + result, sResBackUp);
        logger.info("******************Begining of TEST CASE: " + sTestID + "*******************");
        initializeData(funcMod,sTestID);
        try {
        	driver.get(portalParam.backOfficeUrl);
            logger.info("Loading backoffice url :"+portalParam.backOfficeUrl);
            BackOfficeSearchPage backOfficeSearchPage = BackOfficeLoginPage.loginToBackOffice(driver);
            BackOfficeAppDetailsPage backOfficeAppDetailsPage = backOfficeSearchPage.moveToManualReviewOffer();
            BackOfficeLoginPage backOfficeLoginPage = backOfficeAppDetailsPage.updateManualReviewOffer();
            result = "Passed";
        } catch (Exception e) {
            throw new Exception(sTestID + " fails xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        } finally {
            logger.info("\n*****Execution of testcase: " + sTestID + "  ends.******************\n");
            String imagePath = portalParam.custom_report_location + takeScreenShot(driver, sTestID);
            testResults.ExportResultToTxt(sTestID + "\t" + result, testResults.sResBackUp + testResults.sResFile);
            writeToReport(funcMod,sTestID, result);
        }
    }

}
