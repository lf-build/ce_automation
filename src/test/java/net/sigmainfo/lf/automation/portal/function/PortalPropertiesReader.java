package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by           : Shaishav.s on 08-02-2017.
 * Test class           : PortalPropertiesReader.java
 * Description          : Reads portal.properties key values declared
 * Includes             : 1. Declaration of keys declared in property file
 *                        2. Getter and setter methods
 */
@Component
public class PortalPropertiesReader {

    @Value(value = "${borrowerUrl}")
    private String borrowerUrl;
    @Value(value = "${backOfficeUrl}")
    private String backOfficeUrl;
    @Value(value = "${custom_report_location}")
    private String custom_report_location;
    @Value(value = "${upload_file_location}")
    private String upload_file_location;
    @Value(value = "${bankPortalUrl}")
    private String bankPortalUrl;
    @Value(value = "${browser}")
    private String browser;
    @Value(value = "${password}")
    private String password;
    @Value(value = "${backOfficeUsername}")
    private String backOfficeUsername;
    @Value(value = "${backOfficePassword}")
    private String backOfficePassword;
    @Value(value = "${mongoHost}")
    private String mongoHost;
    @Value(value = "${mongoPort}")
    private String mongoPort;
    @Value(value = "${mongoDb}")
    private String mongoDb;
    @Value(value = "${mongoCollection}")
    private String mongoCollection;
    @Value(value = "${bankPortalUsername}")
    private String bankPortalUsername;
    @Value(value = "${bankPortalPassword}")
    private String bankPortalPassword;
    @Value(value = "${numberOfApps}")
    private String numberOfApps;
    @Value(value = "${numberOfAppsWithStatus}")
    private String numberOfAppsWithStatus;

    public String getNumberOfAppsWithStatus() {
        return numberOfAppsWithStatus;
    }

    public void setNumberOfAppsWithStatus(String numberOfAppsWithStatus) {
        this.numberOfAppsWithStatus = numberOfAppsWithStatus;
    }

    public String getNumberOfApps() {
        return numberOfApps;
    }

    public void setNumberOfApps(String numberOfApps) {
        this.numberOfApps = numberOfApps;
    }

    public String getBankPortalUsername() {
        return bankPortalUsername;
    }

    public void setBankPortalUsername(String bankPortalUsername) {
        this.bankPortalUsername = bankPortalUsername;
    }

    public String getBankPortalPassword() {
        return bankPortalPassword;
    }

    public void setBankPortalPassword(String bankPortalPassword) {
        this.bankPortalPassword = bankPortalPassword;
    }

    public String getMongoHost() {
        return mongoHost;
    }

    public void setMongoHost(String mongoHost) {
        this.mongoHost = mongoHost;
    }

    public String getMongoPort() {
        return mongoPort;
    }

    public void setMongoPort(String mongoPort) {
        this.mongoPort = mongoPort;
    }

    public String getMongoDb() {
        return mongoDb;
    }

    public void setMongoDb(String mongoDb) {
        this.mongoDb = mongoDb;
    }

    public String getMongoCollection() {
        return mongoCollection;
    }

    public void setMongoCollection(String mongoCollection) {
        this.mongoCollection = mongoCollection;
    }

    public String getUpload_file_location() {
        return upload_file_location;
    }

    public void setUpload_file_location(String upload_file_location) {
        this.upload_file_location = upload_file_location;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBankPortalUrl() {
        return bankPortalUrl;
    }

    public void setBankPortalUrl(String bankPortalUrl) {
        this.bankPortalUrl = bankPortalUrl;
    }

    public String getBackOfficeUrl() {
        return backOfficeUrl;
    }

    public void setBackOfficeUrl(String backOfficeUrl) {
        this.backOfficeUrl = backOfficeUrl;
    }

    public String getBackOfficeUsername() {
        return backOfficeUsername;
    }

    public void setBackOfficeUsername(String backOfficeUsername) {
        this.backOfficeUsername = backOfficeUsername;
    }

    public String getBackOfficePassword() {
        return backOfficePassword;
    }

    public void setBackOfficePassword(String backOfficePassword) {
        this.backOfficePassword = backOfficePassword;
    }

    public String getBorrowerUrl() {
        return borrowerUrl;
    }

    public void setBorrowerUrl(String borrowerUrl) {
        this.borrowerUrl = borrowerUrl;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getCustom_report_location() {
        return custom_report_location;
    }

    public void setCustom_report_location(String custom_report_location) {
        this.custom_report_location = custom_report_location;
    }
}
