package net.sigmainfo.lf.automation.portal.pages.Borrower;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 27-02-2018.
 */
public class BorrowerLoginPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BorrowerLoginPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BorrowerLoginPage(WebDriver driver) throws Exception {

        this.driver = driver;
        PortalFuncUtils.waitForPageToLoad(driver);
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_loginButton));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_loginButton));
        logger.info("=========== BorrowerLoginPage is loaded============");
    }



    public BorrowerLoginPage(){}

    public static BorrowerFinalOfferPage loginToBorrowerPortal() throws Exception {
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_loginButton),"Login");
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_mobileNumberTextBox)));
        assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_mobileNumberTextBox)));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_mobileNumberTextBox), PortalParam.mobile);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_sendOtpLink),"Send OTP");
        Thread.sleep(2500);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_otpTextBox), getOtp(PortalParam.mobile));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.borrower_loginPage_signInButton),"Sign In");
        return new BorrowerFinalOfferPage(driver);
    }
}
