package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by Thangaraj on 16-04-2018.
 */
public class BackOfficeSearchPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BackOfficeSearchPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BackOfficeSearchPage(WebDriver driver) throws Exception {
        try {
            this.driver = driver;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNumberTextBox));
            logger.info("=========== BackOfficeSearchPage is loaded============");
        }catch (Exception e)
        {
            throw new Exception("BackOfficeSearchPage could not be loaded within time");
        }
    }
    public BackOfficeSearchPage(){}

    public BackOfficeAppDetailsPage searchApp(String applicationId) throws Exception {
        try {
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNumberTextBox), applicationId);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_searchButton), "Search");
            /*wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("main:([style*=\"margin-bottom\"])")));*/
            Thread.sleep(5000);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
            /*assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appId)).getText(), PortalParam.mobile, "Application Id does not match");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appDate)).getText(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Application date does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_applicantName)).getText().contains(PortalParam.firstName + " " + PortalParam.middleName + " " + PortalParam.lastName), "Applicant name does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_phoneNumber)).getText().contains(PortalParam.mobile), "Applicant contact does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_loanAmount)).getText().contains(PortalFuncUtils.removeSpecialChar(PortalParam.amountNeed)), "Loam amount does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_employerName)).getText().contains(PortalParam.legalBusiness), "Business name does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_source)).getText().contains("Organic"), "Source does not match");*/
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton), "Details");
            return new BackOfficeAppDetailsPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public BackOfficeAppDetailsPage searchFromMobile(String mobileNumber) throws Exception {
        try {
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicantPhoneTextBox), mobileNumber);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_searchButton), "Search");
            Thread.sleep(5000);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
            /*assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appId)).getText(), PortalParam.mobile, "Application Id does not match");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appDate)).getText(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Application date does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_applicantName)).getText().contains(PortalParam.firstName + " " + PortalParam.middleName + " " + PortalParam.lastName), "Applicant name does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_phoneNumber)).getText().contains(PortalParam.mobile), "Applicant contact does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_loanAmount)).getText().contains(PortalFuncUtils.removeSpecialChar(PortalParam.amountNeed)), "Loam amount does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_employerName)).getText().contains(PortalParam.legalBusiness), "Business name does not match");
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_source)).getText().contains("Organic"), "Source does not match");*/

            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton), "Details");
            return new BackOfficeAppDetailsPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    // Created by Thangaraj C
    public BackOfficeUserAdministration moveTocreateNewUser() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_createNewuser), "Create new user");
        return new BackOfficeUserAdministration(driver);
	}
	
	public BackOfficeUserAdministration moveToAllUser() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_allUser), "All User");
        return new BackOfficeUserAdministration(driver);
	}
	
	  public BackOfficeAppDetailsPage applicationSearch() throws Exception {
	        try {
	        	
	        	  String appType[] = new String[6]; //declaring array of six items
	        	 
	        	  appType[0] ="appNum_0002512";
	        	  
	        	  appType[1] ="mobile_6425956013";
	 
	        	  appType[2] ="pan_VKUPV0895S";
	 
	        	  appType[3] ="email_shaishav.sigma0404143334@gmail.com";     
	        	  
	        	  appType[4] ="name_Thangaraj";
	        	  
	        	  appType[5] ="employername_MY COMPUTER";
	        	  
	 
	              // Looping multiple application search
	 
	              for (int i=0;i<appType.length;i++)
	              {
	            	  String[] parts = appType[i].split("_");
	            	  String ApplicationType = parts[0]; 
	            	  String ApplicationValue = parts[1]; 
	            	  
	            	  if(ApplicationType.equalsIgnoreCase("appNum"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNumberTextBox), ApplicationValue);
	            	  }
	            	  else if(ApplicationType.equalsIgnoreCase("mobile"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicantPhoneTextBox), ApplicationValue);
	            	  }
	            	  else if(ApplicationType.equalsIgnoreCase("pan"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationPANTextBox), ApplicationValue);
	            	  }
	            	  else if(ApplicationType.equalsIgnoreCase("email"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationEmailTextBox), ApplicationValue);
	            	  }
	            	  else if(ApplicationType.equalsIgnoreCase("name"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNameTextBox), ApplicationValue);
	            	  }
	            	  else if(ApplicationType.equalsIgnoreCase("employername"))
	            	  {
	            	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationEmployerTextBox), ApplicationValue);
	            	  }
	            	  else
	            	  {
	            		  System.out.println("----No value is matching----");
	            	  }            	  
	             
	        	
	            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_searchButton), "Search");
	            Thread.sleep(5000);
	            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
	            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
	            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
	            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton), "Details");
		        Thread.sleep(5000);      
		        
		        if(ApplicationType.equalsIgnoreCase("appNum")){		         
		         assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_appId)).getText().contains(ApplicationValue), "Application Id does not match");
		        } else if(ApplicationType.equalsIgnoreCase("mobile")){
          	     assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_mobile)).getText(), ApplicationValue, "Mobile does not match");
          	    } else if(ApplicationType.equalsIgnoreCase("pan")){
           	     assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_pan)).getText(), ApplicationValue, "Pan does not match");
          	    } else if(ApplicationType.equalsIgnoreCase("email")){
           	     assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_email)).getText(), ApplicationValue, "Email does not match");
          	    } else if(ApplicationType.equalsIgnoreCase("name")){
          	    	assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_name)).getText().contains(ApplicationValue), "Name does not match");           	            		    
          	    } else if(ApplicationType.equalsIgnoreCase("employername")){
           	     assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultDataFetch_employername)).getText(), ApplicationValue, "Employer name does not match");
          	    } else {
          		  System.out.println("----No value is matching----");
          	    }  
		        
		        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_actionCenter), "Action Center Click");
			    Thread.sleep(8000);   
		        
		        
		         
	            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appId)).getText(), PortalParam.mobile, "Application Id does not match");
	            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_appDate)).getText(), new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "Application date does not match");
	            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_applicantName)).getText().contains(PortalParam.firstName + " " + PortalParam.middleName + " " + PortalParam.lastName), "Applicant name does not match");
	            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_phoneNumber)).getText().contains(PortalParam.mobile), "Applicant contact does not match");
	            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_loanAmount)).getText().contains(PortalFuncUtils.removeSpecialChar(PortalParam.amountNeed)), "Loam amount does not match");
	            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_employerName)).getText().contains(PortalParam.legalBusiness), "Business name does not match");
	            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_source)).getText().contains("Organic"), "Source does not match");
	            
	            
	            
	            
	              }
	              
	         return new BackOfficeAppDetailsPage(driver);
	        }catch (Exception e)
	        {
	            throw new Exception(e.getMessage());
	        }
	    }
	
	public BackOfficeAppDetailsPage moveToFailedsubmission() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_failedSubmission), "Clicked Failed Submission button");
        return new BackOfficeAppDetailsPage(driver);
	}
	
	public BackOfficeAppDetailsPage moveToPendingInitialOffersubmission() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_pendinginitialoffer), "Clicked Pending initial offer menu");
        return new BackOfficeAppDetailsPage(driver);
	}
	

	public BackOfficeAppDetailsPage moveToPendingBankstmtsubmission() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_pendingbankstmt), "Clicked Pending bank statement menu");
        return new BackOfficeAppDetailsPage(driver);
	}
	
	public BackOfficeAppDetailsPage moveToPendingFinalOffer() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_pendingGetFinalOffer), "Clicked get final offer menu");
        return new BackOfficeAppDetailsPage(driver);
	}
	public BackOfficeAppDetailsPage moveToManualReviewOffer() throws Exception {
		PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_manualReviewOffer), "Clicked manual review");
        return new BackOfficeAppDetailsPage(driver);

	}


}
