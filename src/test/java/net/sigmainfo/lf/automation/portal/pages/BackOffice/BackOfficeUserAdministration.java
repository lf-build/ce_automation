package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import net.sigmainfo.lf.automation.portal.function.StringEncrypter;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class BackOfficeUserAdministration extends AbstractTests {
	
	 private Logger logger = LoggerFactory.getLogger(BackOfficeUserAdministration.class);
	     static WebDriverWait wait = new WebDriverWait(driver,60);

	    @Autowired
	    BackOfficeSearchPage backOfficeSearchPage;

	    public BackOfficeUserAdministration(WebDriver driver) throws Exception {
	        try {
	            this.driver = driver;
	            PortalFuncUtils.waitForPageToLoad(driver);
	            //PortalFuncUtils.scrollOnTopOfThePage(driver,"Backoffice");
	            //PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
	            logger.info("=========== BackOfficeUserAdministrationPage is loaded============");
	        }catch (Exception e)
	        {
	            throw new Exception("BackOfficeUserAdministrationPage could not be loaded within time");
	        }
	    }
	    public BackOfficeUserAdministration(){}
	    
	    public static BackOfficeUserAdministration createNewUser() throws Exception
	    {	    	
	    	String emailID = RandomEmailGen()+"@gmail.com";
	    	PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_fullName), "TestCEautomation");            
	        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_userName), "Test" + RandomStringUtils.randomAlphabetic(10));
	        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_password), "Test" + RandomStringUtils.randomAlphabetic(10));
	        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_email), emailID);
	        SelectNoOfAutority();
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_submitNewUser));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_submitNewUser), "Submit to create a new user");
            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'User is successfully created.')]")));
            //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'User is successfully created.')]")));
            Assert.assertEquals("User is successfully created.", driver.findElement(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'User is successfully created.')]")).getText());
          
	    	return new BackOfficeUserAdministration(driver);	    	
	    }
	    
	    public static BackOfficeUserAdministration verifyUserList() throws Exception
	    {   		    		
	    	
	    	for(int j=1;j<=3;j++){	    	
	    		List<WebElement> rows = driver.findElements(By.xpath("//*[@id='userList']/tbody/tr"));
	    		int count = rows.size();
	    		System.out.println("ROW COUNT : "+count);
	    		for(int i=1; i<= count; i++ ) {
	    			System.out.println("Page-----------> : "+j);
	    			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='userList']/tbody/tr["+i+"]/td[1]")));	    
	    		}	    	
	    	
	    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_allUser_pageNavigation));
	    	PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_allUser_pageNavigation), "Page navigation");
	    	} 
	    	return new BackOfficeUserAdministration(driver);
	    }
	    	    
	    public static BackOfficeUserAdministration editUser() throws Exception
	    {	    	
	    	String emailID = RandomEmailGen()+"@gmail.com";
	    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_editUser_editButton));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_editUser_editButton), "Edit button clicked");
	        Thread.sleep(20000);
	        PortalFuncUtils.clear(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_fullName), "AutomatonTest");            
 	    	PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_fullName), "AutomatonTest");            
	        PortalFuncUtils.clear(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_email),emailID) ;
	        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_createNewUser_email), emailID);
	        EditSelectNoOfAutority();
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_editUser_updateDetailsButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_editUser_updateDetailsButton), "Update user details");
            Thread.sleep(5000);
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_editUser_emailInfo), emailID), "Email ID does not match");
            
	    	return new BackOfficeUserAdministration(driver);	    	
	    }
	    
	    public static BackOfficeUserAdministration userDeactivation() throws Exception
	    { 
	    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_deActivateButton));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_deActivateButton), "Deactivate button clicked");
	        Thread.sleep(3000);
	        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettings));
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettings));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettings), "User setting dropdown clicked");
	        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettingsLogout));
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettingsLogout));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettingsLogout), "User setting logout button clicked");
	        Thread.sleep(3000);
	        PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox));
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), "thangaraj");
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox),"thangaraj");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
            Thread.sleep(2000);
            Assert.assertEquals("Your username or password was incorrect", driver.findElement(By.xpath("//*[@id='error-message']")).getText());
	    	return new BackOfficeUserAdministration(driver);
	    }
	    
	    
	    public static BackOfficeUserAdministration userActivation() throws Exception
	    { 
	    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_deActivateButton));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_deActivateButton), "Deactivate button clicked");
	        Thread.sleep(3000);
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettings));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettings), "User setting dropdown clicked");
	        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettingsLogout));
	        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_userAdministrationPage_userSettingsLogout), "User setting logout button clicked");
	        Thread.sleep(3000);
	        PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox));
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), "thangaraj");
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox),"thangaraj");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle)); Thread.sleep(5000);
            assertTrue(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle)).getText().contains("T"), "Source does not match");
           	return new BackOfficeUserAdministration(driver);
	    }
	    
	    public static void SelectNoOfAutority() throws InterruptedException
	    {
	    	List<WebElement> els = driver.findElements(By.xpath("//*[@id='checkboxContainer']"));
	    	for ( WebElement el : els ) {
	    	    if ( !el.isSelected() ) {
	    	        el.click();
	    	    }
	    	}
	    }
	    
	    public static String RandomEmailGen() {
	        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	        StringBuilder salt = new StringBuilder();
	        Random rnd = new Random();
	        while (salt.length() < 10) { // length of the random string.
	            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
	            salt.append(SALTCHARS.charAt(index));
	        }
	        String saltStr = salt.toString();
	        return saltStr;

	    }
	    public static void EditSelectNoOfAutority() throws InterruptedException
	    {
	    	List<WebElement> els = driver.findElements(By.xpath("//*[@id='checkboxContainer']"));
	    	for ( WebElement el : els ) {
	    	    if ( !el.isSelected() ) {
	    	        el.click();
	    	    }
	    	}
	    	driver.findElement(By.xpath("//*[@id='ITExecutive']")).click();
	    }
	    
	    //This method generates random string
	    public String generateRandomString(int length){	    	
	        return RandomStringUtils.randomAlphabetic(length);
	    } 

}
