package net.sigmainfo.lf.automation.portal.function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Thangaraj C on 19-04-2018.
 */
@Component
public class UIObjPropertiesReader {

    @Value(value = "${backoffice.loginPage.usernameTextBox}")
    private String backoffice_loginPage_usernameTextBox;

    @Value(value = "${backoffice.loginPage.passwordTextBox}")
    private String backoffice_loginPage_passwordTextBox;

    @Value(value = "${backoffice.loginPage.loginButton}")
    private String backoffice_loginPage_loginButton;

    @Value(value = "${borrower.loanAmountpage.howMuchDoYouNeedLabel}")
    private String borrower_loanAmountpage_howMuchDoYouNeedLabel;

    @Value(value = "${borrower.loanAmountpage.loanDetailsMenu}")
    private String borrower_loanAmountpage_loanDetailsMenu;

    @Value(value = "${borrower.loanAmountpage.nextButton}")
    private String borrower_loanAmountpage_nextButton;

    @Value(value = "${borrower.loanAmountpage.amountTextBox}")
    private String borrower_loanAmountpage_amountTextBox;

    @Value(value = "${borrower.loanPurposepage.needPurposeLabel}")
    private String borrower_loanPurposepage_needPurposeLabel;

    @Value(value = "${borrower.loanPurposepage.travelButton}")
    private String borrower_loanPurposepage_travelButton;

    @Value(value = "${borrower.loanPurposepage.vehiclePurposeButton}")
    private String borrower_loanPurposepage_vehiclePurposeButton;

    @Value(value = "${borrower.loanPurposepage.medicalButton}")
    private String borrower_loanPurposepage_medicalButton;

    @Value(value = "${borrower.loanPurposepage.loanRefinanceButton}")
    private String borrower_loanPurposepage_loanRefinanceButton;

    @Value(value = "${borrower.loanPurposepage.weddingButton}")
    private String borrower_loanPurposepage_weddingButton;

    @Value(value = "${borrower.loanPurposepage.homeImprovementButton}")
    private String borrower_loanPurposepage_homeImprovementButton;

    @Value(value = "${borrower.loanPurposepage.nextButton}")
    private String borrower_loanPurposepage_nextButton;

    @Value(value = "${borrower.profilePage.personalInfoLabel}")
    private String borrower_profilePage_personalInfoLabel;

    @Value(value = "${borrower.profilePage.mrTitleButton}")
    private String borrower_profilePage_mrTitleButton;

    @Value(value = "${borrower.profilePage.msTitleButton}")
    private String borrower_profilePage_msTitleButton;

    @Value(value = "${borrower.profilePage.mrsTitleButton}")
    private String borrower_profilePage_mrsTitleButton;

    @Value(value = "${borrower.profilePage.maritalStatusSingleButton}")
    private String borrower_profilePage_maritalStatusSingleButton;

    @Value(value = "${borrower.profilePage.maritalStatusMarriedButton}")
    private String borrower_profilePage_maritalStatusMarriedButton;

    @Value(value = "${borrower.profilePage.firstNameTextBox}")
    private String borrower_profilePage_firstNameTextBox;

    @Value(value = "${borrower.profilePage.middleNameTextBox}")
    private String borrower_profilePage_middleNameTextBox;

    @Value(value = "${borrower.profilePage.lastNameTextBox}")
    private String borrower_profilePage_lastNameTextBox;

    @Value(value = "${borrower.profilePage.emailIdTextBox}")
    private String borrower_profilePage_emailIdTextBox;

    @Value(value = "${borrower.profilePage.nextButton}")
    private String borrower_profilePage_nextButton;

    @Value(value = "${borrower.accountCreationpage.mobileVerificationLabel}")
    private String borrower_accountCreationpage_mobileVerificationLabel;

    @Value(value = "${borrower.accountCreationpage.mobileNumberTextBox}")
    private String borrower_accountCreationpage_mobileNumberTextBox;

    @Value(value = "${borrower.accountCreationpage.nextButton}")
    private String borrower_accountCreationpage_nextButton;

    @Value(value = "${borrower.accountCreationpage.otpSentLabel}")
    private String borrower_accountCreationpage_otpSentLabel;

    @Value(value = "${borrower.accountCreationpage.otpTextBox}")
    private String borrower_accountCreationpage_otpTextBox;

    @Value(value = "${borrower.accountCreationpage.otpNextButton}")
    private String borrower_accountCreationpage_otpNextButton;

    @Value(value = "${borrower.employmentDetailspage.legalBusinessTextBox}")
    private String borrower_employmentDetailspage_legalBusinessTextBox;

    @Value(value = "${borrower.employmentDetailspage.takeHomeSalaryTextBox}")
    private String borrower_employmentDetailspage_takeHomeSalaryTextBox;

    @Value(value = "${borrower.employmentDetailspage.officialEmailIdTextBox}")
    private String borrower_employmentDetailspage_officialEmailIdTextBox;

    @Value(value = "${borrower.employmentDetailspage.nextButton}")
    private String borrower_employmentDetailspage_nextButton;

    @Value(value = "${borrower.expensesPage.residenseTypeDropdown}")
    private String borrower_expensesPage_residenseTypeDropdown;

    @Value(value = "${borrower.expensesPage.nextButton}")
    private String borrower_expensesPage_nextButton;

    @Value(value = "${borrower.identificationDetails.dateDropdown}")
    private String borrower_identificationDetails_dateDropdown;

    @Value(value = "${borrower.identificationDetails.monthDropdown}")
    private String borrower_identificationDetails_monthDropdown;

    @Value(value = "${borrower.identificationDetails.yearDropdown}")
    private String borrower_identificationDetails_yearDropdown;

    @Value(value = "${borrower.identificationDetails.idDetailsLabel}")
    private String borrower_identificationDetails_idDetailsLabel;

    @Value(value = "${borrower.identificationDetails.aadharNumberTextBox}")
    private String borrower_identificationDetails_aadharNumberTextBox;

    @Value(value = "${borrower.identificationDetails.panNumberTextBox}")
    private String borrower_identificationDetails_panNumberTextBox;

    @Value(value = "${borrower.identificationDetails.address1TextBox}")
    private String borrower_identificationDetails_address1TextBox;

    @Value(value = "${borrower.identificationDetails.address2TextBox}")
    private String borrower_identificationDetails_address2TextBox;

    @Value(value = "${borrower.identificationDetails.localityTextBox}")
    private String borrower_identificationDetails_localityTextBox;

    @Value(value = "${borrower.identificationDetails.pincodeTextBox}")
    private String borrower_identificationDetails_pincodeTextBox;

    @Value(value = "${borrower.identificationDetails.cityTextBox}")
    private String borrower_identificationDetails_cityTextBox;

    @Value(value = "${borrower.identificationDetails.stateTextBox}")
    private String borrower_identificationDetails_stateTextBox;

    @Value(value = "${borrower.identificationDetails.bothAddressSameButton}")
    private String borrower_identificationDetails_bothAddressSameButton;

    @Value(value = "${borrower.identificationDetails.bothAddressDifferentButton}")
    private String borrower_identificationDetails_bothAddressDifferentButton;

    @Value(value = "${borrower.identificationDetails.nextButton}")
    private String borrower_identificationDetails_nextButton;

    @Value(value = "${borrower.reviewPage.reviewApplicationLabel}")
    private String borrower_reviewPage_reviewApplicationLabel;

    @Value(value = "${borrower.reviewPage.nextButton}")
    private String borrower_reviewPage_nextButton;

    @Value(value = "${borrower.reviewPage.agreementCheckbox}")
    private String borrower_reviewPage_agreementCheckbox;

    @Value(value = "${borrower.employmentDetailspage.whatAboutWorkLabel}")
    private String borrower_employmentDetailspage_whatAboutWorkLabel;

    @Value(value = "${borrower.expensesPage.expensesLabel}")
    private String borrower_expensesPage_expensesLabel;

    @Value(value = "${borrower.identificationDetails.detailsLabel}")
    private String borrower_identificationDetails_detailsLabel;

    @Value(value = "${borrower.eligibilityPage.greetLabel}")
    private String borrower_eligibilityPage_greetLabel;

    @Value(value = "${borrower.eligibilityPage.applicationNumberLabel}")
    private String borrower_eligibilityPage_applicationNumberLabel;

    @Value(value = "${borrower.eligibilityPage.nextButton}")
    private String borrower_eligibilityPage_nextButton;

    @Value(value = "${borrower.bankVerificationPage.connectAccountRadioButton}")
    private String borrower_bankVerificationPage_connectAccountRadioButton;

    @Value(value = "${borrower.bankVerificationPage.uploadFileRadioButton}")
    private String borrower_bankVerificationPage_uploadFileRadioButton;

    @Value(value = "${borrower.bankVerificationPage.continueButton}")
    private String borrower_bankVerificationPage_continueButton;

    @Value(value = "${borrower.bankVerificationPage.uploadBankStatement.bankTextBox}")
    private String borrower_bankVerificationPage_uploadBankStatement_bankTextBox;

    @Value(value = "${borrower.bankVerificationPage.uploadBankStatement.uploadFileArea}")
    private String borrower_bankVerificationPage_uploadBankStatement_uploadFileArea;

    @Value(value = "${borrower.bankVerificationPage.uploadBankStatement.nextButton}")
    private String borrower_bankVerificationPage_uploadBankStatement_nextButton;

    @Value(value = "${borrower.educationPage.educationLevelDropdown}")
    private String borrower_educationPage_educationLevelDropdown;

    @Value(value = "${borrower.educationPage.educationalInstituteTextBox}")
    private String borrower_educationPage_educationalInstituteTextBox;

    @Value(value = "${borrower.educationPage.nextButton}")
    private String borrower_educationPage_nextButton;

    @Value(value = "${borrower.employmentPage.employerName}")
    private String borrower_employmentPage_employerName;

    @Value(value = "${borrower.employmentPage.designationTextBox}")
    private String borrower_employmentPage_designationTextBox;

    @Value(value = "${borrower.employmentPage.workExperienceDropdown}")
    private String borrower_employmentPage_workExperienceDropdown;

    @Value(value = "${borrower.employmentPage.address1TextBox}")
    private String borrower_employmentPage_address1TextBox;

    @Value(value = "${borrower.employmentPage.localityTextBox}")
    private String borrower_employmentPage_localityTextBox;

    @Value(value = "${borrower.employmentPage.pincodeTextBox}")
    private String borrower_employmentPage_pincodeTextBox;

    @Value(value = "${borrower.employmentPage.nextButton}")
    private String borrower_employmentPage_nextButton;

    @Value(value = "${borrower.socialPage.skipButton}")
    private String borrower_socialPage_skipButton;

    @Value(value = "${borrower.appComplete.message}")
    private String borrower_appComplete_message;

    @Value(value = "${borrower.appComplete.getUniqueReferralButton}")
    private String borrower_appComplete_getUniqueReferralButton;

    @Value(value = "${borrower.logoutLink}")
    private String borrower_logoutLink;

    @Value(value = "${borrower.socialPage.verifyMeImage}")
    private String borrower_socialPage_verifyMeImage;

    @Value(value = "${backoffice.searchPage.profileCircle}")
    private String backoffice_searchPage_profileCircle;

    @Value(value = "${backoffice.searchPage.applicationNumberTextBox}")
    private String backoffice_searchPage_applicationNumberTextBox;

    @Value(value = "${backoffice.searchPage.searchButton}")
    private String backoffice_searchPage_searchButton;

    @Value(value = "${backoffice.searchPage.resultData.appId}")
    private String backoffice_searchPage_resultData_appId;

    @Value(value = "${backoffice.searchPage.resultData.appDate}")
    private String backoffice_searchPage_resultData_appDate;

    @Value(value = "${backoffice.searchPage.resultData.applicantName}")
    private String backoffice_searchPage_resultData_applicantName;

    @Value(value = "${backoffice.searchPage.resultData.phoneNumber}")
    private String backoffice_searchPage_resultData_phoneNumber;

    @Value(value = "${backoffice.searchPage.resultData.loanAmount}")
    private String backoffice_searchPage_resultData_loanAmount;

    @Value(value = "${backoffice.searchPage.resultData.employerName}")
    private String backoffice_searchPage_resultData_employerName;

    @Value(value = "${backoffice.searchPage.resultData.expiryDate}")
    private String backoffice_searchPage_resultData_expiryDate;

    @Value(value = "${backoffice.searchPage.resultData.source}")
    private String backoffice_searchPage_resultData_source;

    @Value(value = "${backoffice.searchPage.resultData.detailsButton}")
    private String backoffice_searchPage_resultData_detailsButton;

    @Value(value = "${backoffice.appDetailsPage.headerStatus}")
    private String backoffice_appDetailsPage_headerStatus;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboardTab}")
    private String backoffice_appDetailsPage_verificationDashboardTab;

    @Value(value = "${backoffice.appDetailsPage.actionButton}")
    private String backoffice_appDetailsPage_actionButton;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOfferButton}")
    private String backoffice_appDetailsPage_actions_addManualOfferButton;

    @Value(value = "${backoffice.appDetailsPage.actions.presentFinalOfferButton}")
    private String backoffice_appDetailsPage_actions_presentFinalOfferButton;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.termDropdown}")
    private String backoffice_appDetailsPage_actions_addManualOffer_termDropdown;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.loanAmountTextBox}")
    private String backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.processingFeeTextBox}")
    private String backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.emiTextBox}")
    private String backoffice_appDetailsPage_actions_addManualOffer_emiTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.interestRateTextBox}")
    private String backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.processingFeePersentTextBox}")
    private String backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.addManualOffer.submitButton}")
    private String backoffice_appDetailsPage_actions_addManualOffer_submitButton;

    @Value(value = "${backoffice.appDetailsPage.actions.presentFinalOffer.presentFinalOfferButton}")
    private String backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton;

    @Value(value = "${backoffice.searchPage.profile.logoutButton}")
    private String backoffice_searchPage_profile_logoutButton;

    @Value(value = "${borrower.loginPage.mobileNumberTextBox}")
    private String borrower_loginPage_mobileNumberTextBox;

    @Value(value = "${borrower.loginPage.otpTextBox}")
    private String borrower_loginPage_otpTextBox;

    @Value(value = "${borrower.finalOfferPage.loanRadioButton}")
    private String borrower_finalOfferPage_loanRadioButton;

    @Value(value = "${borrower.finalOfferPage.loanAmountLabel}")
    private String borrower_finalOfferPage_loanAmountLabel;

    @Value(value = "${borrower.finalOfferPage.durationLabel}")
    private String borrower_finalOfferPage_durationLabel;

    @Value(value = "${borrower.finalOfferPage.emiLabel}")
    private String borrower_finalOfferPage_emiLabel;

    @Value(value = "${borrower.finalOfferPage.processingFeeLabel}")
    private String borrower_finalOfferPage_processingFeeLabel;

    @Value(value = "${borrower.finalOfferPage.nextButton}")
    private String borrower_finalOfferPage_nextButton;

    @Value(value = "${borrower.finalOfferPage.appReceivedMsg}")
    private String borrower_finalOfferPage_appReceivedMsg;

    @Value(value = "${borrower.loginPage.sendOtpLink}")
    private String borrower_loginPage_sendOtpLink;

    @Value(value = "${borrower.socialPage.nextButton}")
    private String borrower_socialPage_nextButton;

    @Value(value = "${borrower.loginPage.signInButton}")
    private String borrower_loginPage_signInButton;

    @Value(value = "${borrower.socialPage.verificationDoneMsg}")
    private String borrower_socialPage_verificationDoneMsg;

    @Value(value = "${borrower.loginPage.loginButton}")
    private String borrower_loginPage_loginButton;

    @Value(value = "${borrower.finalOfferPage.readyForDisbursementMsg}")
    private String borrower_finalOfferPage_readyForDisbursementMsg;

    @Value(value = "${backoffice.appDetailsPage.personalInfoTab.personalInfoLabel}")
    private String backoffice_appDetailsPage_personalInfoTab_personalInfoLabel;

    @Value(value = "${backoffice.actionCenter}")
    private String backoffice_actionCenter;

    @Value(value = "${backoffice.appDetailsPage.actions.uploadPendingDocument}")
    private String backoffice_appDetailsPage_actions_uploadPendingDocument;

    @Value(value = "${backoffice.appDetailsPage.actions.uploadPendingDocument.closeButton}")
    private String backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.kycStartedMsg}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.kycLabel}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.screenshot}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.applicantNameCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.applicantPhotoCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.applicantDoBCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.applicantPanCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.verifyButton}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.panDetails.closeButton}")
    private String backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.kycStartedMsg}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.kycLabel}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.screenshot}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.applicantNameCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.applicantPhotoCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.verifyButton}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.addressDetails.closeButton}")
    private String backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.kycStartedMsg}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.kycLabel}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.screenshot}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.appSignedCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.kycVerifiedCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.loanAgreementSignedCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.ecsSignedCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.signatureMatchCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.verifyButton}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.signedDocDetails.closeButton}")
    private String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton;

    @Value(value = "${backoffice.appDetailsPage.bottomTabs.documents}")
    private String backoffice_appDetailsPage_bottomTabs_documents;

    @Value(value = "${backoffice.appDetailsPage.bottomTabs.documents.documentsLabel}")
    private String backoffice_appDetailsPage_bottomTabs_documents_documentsLabel;

    @Value(value = "${backoffice.appDetailsPage.actions.updateBankDetails.accountNumberTextBox}")
    private String backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.updateBankDetails.confirmAccountNumberTextBox}")
    private String backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.updateBankDetails.ifscCodeTextBox}")
    private String backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox;

    @Value(value = "${backoffice.appDetailsPage.actions.updateBankDetails.submitButton}")
    private String backoffice_appDetailsPage_actions_updateBankDetails_submitButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.bankVerificationLabel}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.bankVerificationNotInitiatedMsg}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.initiateNowButton}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.bankVerificationStartedMsg}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.accountNumberTextBox}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.nameMatchCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.bankNameMatchCheckBox}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox;

    @Value(value = "${backoffice.appDetailsPage.verificationDashboard.bankVerificationDetails.verifyButton}")
    private String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton;

    @Value(value = "${backoffice.appDetailsPage.actions.updateBankDetails}")
    private String backoffice_appDetailsPage_actions_updateBankDetails;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.selectCommunityDropdown}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.generalCastRadioButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.scCastRadioButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.stCastRadioButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.obcCastRadioButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.otherCastRadioButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.kycDocumentExpiryCalendar}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.kycDocumentNumberTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.motherNameTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.residenceYearsTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.cityYearsTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.workExperienceTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.kycDocumentTypeTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.dependantsTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.guardianNameTextBox}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.submitButton}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_submitButton;

    @Value(value = "${backoffice.appDetailsPage.action.updateAncilliaryData.casteRadioGroup}")
    private String backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup;

    @Value(value = "${backoffice.appDetailsPage.action.updatePerfiosSalaryDetail}")
    private String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail;

    @Value(value = "${backoffice.appDetailsPage.action.updatePerfiosSalaryDetail.amountTextBox}")
    private String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updatePerfiosSalaryDetail.salaryDateDropdown}")
    private String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown;

    @Value(value = "${backoffice.appDetailsPage.action.updatePerfiosSalaryDetail.narrationTextBox}")
    private String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.updatePerfiosSalaryDetail.submitButton}")
    private String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton;

    @Value(value = "${backoffice.appDetailsPage.action.addFOIRCalculation}")
    private String backoffice_appDetailsPage_action_addFOIRCalculation;

    @Value(value = "${backoffice.appDetailsPage.action.addFOIRCalculation.typeOfLoanTextBox}")
    private String backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.addFOIRCalculation.obligatedAmountTextBox}")
    private String backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.addFOIRCalculation.commentTextBox}")
    private String backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox;

    @Value(value = "${backoffice.appDetailsPage.action.addFOIRCalculation.submitButton}")
    private String backoffice_appDetailsPage_action_addFOIRCalculation_submitButton;

    @Value(value = "${backoffice.appDetailsPage.action.addDeviation}")
    private String backoffice_appDetailsPage_action_addDeviation;

    @Value(value = "${backoffice.appDetailsPage.action.addDeviation.deviationTextbox}")
    private String backoffice_appDetailsPage_action_addDeviation_deviationTextbox;

    @Value(value = "${backoffice.appDetailsPage.action.addDeviation.submitButton}")
    private String backoffice_appDetailsPage_action_addDeviation_submitButton;

    @Value(value = "${backoffice.appDetailsPage.action.documentCollectionSource}")
    private String backoffice_appDetailsPage_action_documentCollectionSource;

    @Value(value = "${backoffice.appDetailsPage.action.documentCollectionSource.sourceRadio}")
    private String backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio;

    @Value(value = "${backoffice.appDetailsPage.action.documentCollectionSource.submitButton}")
    private String backoffice_appDetailsPage_action_documentCollectionSource_submitButton;

    @Value(value = "${backoffice.appDetailsPage.action.completeFulfillment}")
    private String backoffice_appDetailsPage_action_completeFulfillment;

    @Value(value = "${backoffice.appDetailsPage.action.completeFulfillment.approveApplicationButton}")
    private String backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton;

    @Value(value = "${backoffice.appDetailsPage.action.sendToBank}")
    private String backoffice_appDetailsPage_action_sendToBank;

    @Value(value = "${backoffice.appDetailsPage.action.sendToBank.bankFraudReviewButton}")
    private String backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton;

    @Value(value = "${bankPortal.loginPage.usernameTextBox}")
    private String bankPortal_loginPage_usernameTextBox;

    @Value(value = "${bankPortal.loginPage.passwordTextBox}")
    private String bankPortal_loginPage_passwordTextBox;

    @Value(value = "${bankPortal.loginPage.loginButton}")
    private String bankPortal_loginPage_loginButton;

    @Value(value = "${bankPortal.dashboard.pendingBankFraudReviewTab}")
    private String bankPortal_dashboard_pendingBankFraudReviewTab;

    @Value(value = "${bankPortal.dashboard.profileCircle}")
    private String bankPortal_dashboard_profileCircle;

    @Value(value = "${bankPortal.dashboard.profile.logoutButton}")
    private String bankPortal_dashboard_profile_logoutButton;

    @Value(value = "${bankPortal.dashboard.resultAppsMsg}")
    private String bankPortal_dashboard_resultAppsMsg;

    @Value(value = "${bankPortal.appDetailsPage.actionButton}")
    private String bankPortal_appDetailsPage_actionButton;

    @Value(value = "${bankPortal.appDetailsPage.headerStatus}")
    private String bankPortal_appDetailsPage_headerStatus;

    @Value(value = "${bankPortal.appDetailsPage.action.approveApplication}")
    private String bankPortal_appDetailsPage_action_approveApplication;

    @Value(value = "${bankPortal.appDetailsPage.action.approveApplication.approveAppButton}")
    private String bankPortal_appDetailsPage_action_approveApplication_approveAppButton;

    @Value(value = "${bankPortal.appDetailsPage.action.approveCreditApplication}")
    private String bankPortal_appDetailsPage_action_approveCreditApplication;

    @Value(value = "${bankPortal.appDetailsPage.action.approveCreditApplication..approveAppButton}")
    private String bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton;

    @Value(value = "${bankPortal.dashboard.pendingBankCreditReviewTab}")
    private String bankPortal_dashboard_pendingBankCreditReviewTab;

    @Value(value = "${bankPortal.dashboard.pendingFunding}")
    private String bankPortal_dashboard_pendingFunding;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.loanAccountNumberTextbox}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.paymentStatusDropdown}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.fundedAmountTextbox}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.fundedDateCalendar}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.emiAmountTextbox}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.firstEmiDateCalendar}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.submitButton}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton;

    @Value(value = "${bankPortal.appDetailsPage.action.updateDisbursementDetails.utrNumberTextbox}")
    private String bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox;

    @Value(value = "${borrower.reviewPage.minimumCriteriaNoMatchMsg}")
    private String borrower_reviewPage_minimumCriteriaNoMatchMsg;

    @Value(value = "${borrower.eligibilityPage.sorryLabel}")
    private String borrower_eligibilityPage_sorryLabel;

    @Value(value = "${borrower.eligibilityPage.unableToGenerateOfferMsg}")
    private String borrower_eligibilityPage_unableToGenerateOfferMsg;

    @Value(value = "${backoffice.searchPage.applicantPhoneTextBox}")
    private String backoffice_searchPage_applicantPhoneTextBox;

    @Value(value = "${borrower.logoutButton}")
    private String borrower_logoutButton;

    @Value(value = "${backoffice.appDetailsPage.actions.notInterestedButton}")
    private String backoffice_appDetailsPage_actions_notInterestedButton;

    @Value(value = "${backoffice.appDetailsPage.actions.notInterestedButton.reasonDropdown}")
    private String backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown;

    @Value(value = "${backoffice.appDetailsPage.actions.notInterestedButton.notInterestedButton}")
    private String backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow}")
    private String backoffice_appDetailsPage_updateCashFlow;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.mothlyEMI}")
    private String backoffice_appDetailsPage_updateCashFlow_mothlyEMI;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.monthlyExpense}")
    private String backoffice_appDetailsPage_updateCashFlow_monthlyExpense;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.monthlyIncome}")
    private String backoffice_appDetailsPage_updateCashFlow_monthlyIncome;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.mothlyInvestment}")
    private String backoffice_appDetailsPage_updateCashFlow_mothlyInvestment;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.dailyAverageBalance}")
    private String backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.noOfECSbounce}")
    private String backoffice_appDetailsPage_updateCashFlow_noOfECSbounce;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.salarayDate}")
    private String backoffice_appDetailsPage_updateCashFlow_salarayDate;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.maximumCreditAmount}")
    private String backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount;

    @Value(value = "${backoffice.appDetailsPage.updateCashFlow.cashFlowSubmit}")
    private String backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit;

    @Value(value = "${backoffice.appDetails.getFinalOffer}")
    private String backoffice_appDetails_getFinalOffer;

    @Value(value = "${backoffice.appDetails.submitGetFinalOffer}")
    private String backoffice_appDetails_submitGetFinalOffer;

    @Value(value = "${backoffice.appDetails.selectFinalOffer}")
    private String backoffice_appDetails_selectFinalOffer;

    @Value(value = "${backoffice.appDetails.submitSelectFinalOffer}")
    private String backoffice_appDetails_submitSelectFinalOffer;
    
    @Value(value = "${backoffice.searchPage.createNewuser}")
    private String backoffice_searchPage_createNewuser;
  
    @Value(value = "${backoffice.userAdministrationPage.createNewUser.fullName}")
    private String backoffice_userAdministrationPage_createNewUser_fullName;
    
    @Value(value = "${backoffice.userAdministrationPage.createNewUser.userName}")
    private String backoffice_userAdministrationPage_createNewUser_userName;
    
    @Value(value = "${backoffice.userAdministrationPage.createNewUser.password}")
    private String backoffice_userAdministrationPage_createNewUser_password;
    
    @Value(value = "${backoffice.userAdministrationPage.createNewUser.email}")
    private String backoffice_userAdministrationPage_createNewUser_email;
    
    @Value(value = "${backoffice.userAdministrationPage.createNewUser.submitNewUser}")
    private String backoffice_userAdministrationPage_createNewUser_submitNewUser;
    
    @Value(value = "${backoffice.searchPage.allUser}")
    private String backoffice_searchPage_allUser;

    @Value(value = "${backoffice.userAdministrationPage.allUser.pageNavigation}")
    private String backoffice_userAdministrationPage_allUser_pageNavigation;

    @Value(value = "${backoffice.userAdministrationPage.editUser.editButton}")
    private String backoffice_userAdministrationPage_editUser_editButton;

    @Value(value = "${backoffice.userAdministrationPage.editUser.updateDetailsButton}")
    private String backoffice_userAdministrationPage_editUser_updateDetailsButton;

    @Value(value = "${backoffice.userAdministrationPage.editUser.emailInfo}")
    private String backoffice_userAdministrationPage_editUser_emailInfo;

    @Value(value = "${backoffice.userAdministrationPage.deActivateButton}")
    private String backoffice_userAdministrationPage_deActivateButton;

    @Value(value = "${backoffice.userAdministrationPage.userSettings}")
    private String backoffice_userAdministrationPage_userSettings;

    @Value(value = "${backoffice.userAdministrationPage.userSettingsLogout}")
    private String backoffice_userAdministrationPage_userSettingsLogout;

    @Value(value = "${backoffice.searchPage.applicationPANTextBox}")
    private String backoffice_searchPage_applicationPANTextBox;
   
    @Value(value = "${backoffice.searchPage.applicationEmailTextBox}")
    private String backoffice_searchPage_applicationEmailTextBox;
    
    @Value(value = "${backoffice.searchPage.applicationNameTextBox}")
    private String backoffice_searchPage_applicationNameTextBox;
    
    @Value(value = "${backoffice.searchPage.applicationEmployerTextBox}")
    private String backoffice_searchPage_applicationEmployerTextBox;    
        
    @Value(value = "${backoffice.searchPage.resultDataFetch.appId}")
    private String backoffice_searchPage_resultDataFetch_appId;
  
    @Value(value = "${backoffice.searchPage.resultDataFetch.mobile}")
    private String backoffice_searchPage_resultDataFetch_mobile;
    
    @Value(value = "${backoffice.searchPage.resultDataFetch.pan}")
    private String backoffice_searchPage_resultDataFetch_pan;
  
    @Value(value = "${backoffice.searchPage.resultDataFetch.email}")
    private String backoffice_searchPage_resultDataFetch_email;
    
    @Value(value = "${backoffice.searchPage.resultDataFetch.name}")
    private String backoffice_searchPage_resultDataFetch_name;
  
    @Value(value = "${backoffice.searchPage.resultDataFetch.employername}")
    private String backoffice_searchPage_resultDataFetch_employername;
    
    @Value(value = "${backoffice.appDetailsPage.actions.assign.role}")
    private String backoffice_appDetailsPage_actions_assign_role;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assign.user}")
    private String backoffice_appDetailsPage_actions_assign_user;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assign.notes}")
    private String backoffice_appDetailsPage_actions_assign_notes;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assign.submitButton}")
    private String backoffice_appDetailsPage_actions_assign_submitButton;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assign.leftAssignMenu}")
    private String backoffice_appDetailsPage_actions_assign_leftAssignMenu;
    
    @Value(value = "${backoffice.appDetailsPage.actions.assign.rightAssignMenu}")
    private String backoffice_appDetailsPage_actions_assign_rightAssignMenu;
   
    @Value(value = "${backoffice.searchPage.assignedResultData.appId}")
    private String backoffice_searchPage_assignedResultData_appId;   
   
    @Value(value = "${backoffice.appDetailsPage.actions.claim.rightClaimMenu}")
    private String backoffice_appDetailsPage_actions_claim_rightClaimMenu;
  
    @Value(value = "${backoffice.appDetailsPage.actions.claim.role}")
    private String backoffice_appDetailsPage_actions_claim_role;
  
    @Value(value = "${backoffice.appDetailsPage.actions.claim.submitButton}")
    private String backoffice_appDetailsPage_actions_claim_submitButton;
  
    @Value(value = "${backoffice.searchPage.assignedResultData.appIdDetailsBtn}")
    private String backoffice_searchPage_assignedResultData_appIdDetailsBtn;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.BottomMenu}")
    private String backoffice_appDetailsPage_actions_assignments_BottomMenu;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.BottomValueOne}")
    private String backoffice_appDetailsPage_actions_assignments_BottomValueOne;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.BottomValueTwo}")
    private String backoffice_appDetailsPage_actions_assignments_BottomValueTwo;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.releaseBtn}")
    private String backoffice_appDetailsPage_actions_assignments_releaseBtn;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.releaseSubBtn}")
    private String backoffice_appDetailsPage_actions_assignments_releaseSubBtn;
  
    @Value(value = "${backoffice.appDetailsPage.actions.assignments.releaseFinalBtn}")
    private String backoffice_appDetailsPage_actions_assignments_releaseFinalBtn;

    
    // Update failed submission application  //
    
    @Value(value = "${backoffice.searchPage.failedSubmission}")
    private String backoffice_searchPage_failedSubmission;
    
    @Value(value = "${backoffice.appDetailsPage.failedSubmission.selectDetailsApp}")
    private String backoffice_appDetailsPage_failedSubmission_selectDetailsApp;
   
    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEduDetails}")
    private String backoffice_appDetailsPage_failedSubmission_updateEduDetails;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.eduQualification}")
    private String backoffice_appDetailsPage_failedSubmission_eduQualification;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.eduInstitution}")
    private String backoffice_appDetailsPage_failedSubmission_eduInstitution;
    
    @Value(value = "${backoffice.appDetailsPage.failedSubmission.eduSubmitBtn}")
    private String backoffice_appDetailsPage_failedSubmission_eduSubmitBtn;
  

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpDetails}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpDetails;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpCompanyName}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpDesignation}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpDesignation;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpTotalExp}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpEmail}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpEmail;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpIncome}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpIncome;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpAddress1}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpAddress1;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpAddress2}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpAddress2;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpLocation}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpLocation;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpPincode}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpPincode;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpCity}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpCity;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpState}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpState;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateEmpSubmitBtn}")
    private String backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updatePersonalDetails}")
    private String backoffice_appDetailsPage_failedSubmission_updatePersonalDetails;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updatePersonalDOB}")
    private String backoffice_appDetailsPage_failedSubmission_updatePersonalDOB;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updatePersonalPAN}")
    private String backoffice_appDetailsPage_failedSubmission_updatePersonalPAN;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updatePersonalAadhaarNumber}")
    private String backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updatePersonalSubmitBtn}")
    private String backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressDetails}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressDetails;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressAdd1}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressAdd1;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressAdd2}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressAdd2;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressLoc}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressLoc;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressPincode}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressPincode;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressChkBox}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressChkBox;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressSubmitBtn}")
    private String backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateIncomeDetails}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeDetails;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateIncomeResidency}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeResidency;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateIncomeMonthlyRent}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateIncomeOtherEMI}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateIncomeCCBalance}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance;

    @Value(value = "${backoffice.appDetailsPage.failedSubmission.updateAddressSubmitBtn}")
    private String backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn;

  
    // Pending Initial Offer 
    
    @Value(value = "${backoffice.searchPage.pendinginitialoffer}")
    private String backoffice_searchPage_pendinginitialoffer;
    
    @Value(value = "${backoffice.appDetailsPage.pendingInitialoffer.selectDetailsApp}")
    private String backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp;
    
    @Value(value = "${backoffice.appDetailsPage.pendingInitialoffer.selectDropOffer}")
    private String backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer;
    
    @Value(value = "${backoffice.appDetailsPage.pendingInitialoffer.selectFirstOffer}")
    private String backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer;
    
    @Value(value = "${backoffice.appDetailsPage.pendingInitialoffer.selectOfferSubmitBtn}")
    private String backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn;
        
    @Value(value = "${backoffice.searchPage.pendingbankstmt}")
    private String backoffice_searchPage_pendingbankstmt;
   
    @Value(value = "${backoffice.appDetailsPage.pendingBankstmt.selectDetailsApp}")
    private String backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp;
   
    
    
    
    
    
    
    
    
    

    public String getBackoffice_userAdministrationPage_deActivateButton() {
		return backoffice_userAdministrationPage_deActivateButton;
	}

	public void setBackoffice_userAdministrationPage_deActivateButton(
			String backoffice_userAdministrationPage_deActivateButton) {
		this.backoffice_userAdministrationPage_deActivateButton = backoffice_userAdministrationPage_deActivateButton;
	}

	public String getBackoffice_userAdministrationPage_userSettings() {
		return backoffice_userAdministrationPage_userSettings;
	}

	public void setBackoffice_userAdministrationPage_userSettings(String backoffice_userAdministrationPage_userSettings) {
		this.backoffice_userAdministrationPage_userSettings = backoffice_userAdministrationPage_userSettings;
	}

	public String getBackoffice_userAdministrationPage_userSettingsLogout() {
		return backoffice_userAdministrationPage_userSettingsLogout;
	}

	public void setBackoffice_userAdministrationPage_userSettingsLogout(
			String backoffice_userAdministrationPage_userSettingsLogout) {
		this.backoffice_userAdministrationPage_userSettingsLogout = backoffice_userAdministrationPage_userSettingsLogout;
	}

	public String getBackoffice_userAdministrationPage_editUser_emailInfo() {
		return backoffice_userAdministrationPage_editUser_emailInfo;
	}

	public void setBackoffice_userAdministrationPage_editUser_emailInfo(
			String backoffice_userAdministrationPage_editUser_emailInfo) {
		this.backoffice_userAdministrationPage_editUser_emailInfo = backoffice_userAdministrationPage_editUser_emailInfo;
	}

	public String getBackoffice_userAdministrationPage_editUser_updateDetailsButton() {
		return backoffice_userAdministrationPage_editUser_updateDetailsButton;
	}

	public void setBackoffice_userAdministrationPage_editUser_updateDetailsButton(
			String backoffice_userAdministrationPage_editUser_updateDetailsButton) {
		this.backoffice_userAdministrationPage_editUser_updateDetailsButton = backoffice_userAdministrationPage_editUser_updateDetailsButton;
	}

	public String getBackoffice_userAdministrationPage_editUser_editButton() {
		return backoffice_userAdministrationPage_editUser_editButton;
	}

	public void setBackoffice_userAdministrationPage_editUser_editButton(
			String backoffice_userAdministrationPage_editUser_editButton) {
		this.backoffice_userAdministrationPage_editUser_editButton = backoffice_userAdministrationPage_editUser_editButton;
	}

	public String getBackoffice_userAdministrationPage_allUser_pageNavigation() {
		return backoffice_userAdministrationPage_allUser_pageNavigation;
	}

	public void setBackoffice_userAdministrationPage_allUser_pageNavigation(
			String backoffice_userAdministrationPage_allUser_pageNavigation) {
		this.backoffice_userAdministrationPage_allUser_pageNavigation = backoffice_userAdministrationPage_allUser_pageNavigation;
	}

	public String getBackoffice_searchPage_allUser() {
		return backoffice_searchPage_allUser;
	}

	public void setBackoffice_searchPage_allUser(String backoffice_searchPage_allUser) {
		this.backoffice_searchPage_allUser = backoffice_searchPage_allUser;
	}

	public String getBackoffice_userAdministrationPage_createNewUser_submitNewUser() {
		return backoffice_userAdministrationPage_createNewUser_submitNewUser;
	}

	public void setBackoffice_userAdministrationPage_createNewUser_submitNewUser(
			String backoffice_userAdministrationPage_createNewUser_submitNewUser) {
		this.backoffice_userAdministrationPage_createNewUser_submitNewUser = backoffice_userAdministrationPage_createNewUser_submitNewUser;
	}

	public String getBackoffice_userAdministrationPage_createNewUser_fullName() {
		return backoffice_userAdministrationPage_createNewUser_fullName;
	}

	public void setBackoffice_userAdministrationPage_createNewUser_fullName(
			String backoffice_userAdministrationPage_createNewUser_fullName) {
		this.backoffice_userAdministrationPage_createNewUser_fullName = backoffice_userAdministrationPage_createNewUser_fullName;
	}

	public String getBackoffice_userAdministrationPage_createNewUser_userName() {
		return backoffice_userAdministrationPage_createNewUser_userName;
	}

	public void setBackoffice_userAdministrationPage_createNewUser_userName(
			String backoffice_userAdministrationPage_createNewUser_userName) {
		this.backoffice_userAdministrationPage_createNewUser_userName = backoffice_userAdministrationPage_createNewUser_userName;
	}

	public String getBackoffice_userAdministrationPage_createNewUser_password() {
		return backoffice_userAdministrationPage_createNewUser_password;
	}

	public void setBackoffice_userAdministrationPage_createNewUser_password(
			String backoffice_userAdministrationPage_createNewUser_password) {
		this.backoffice_userAdministrationPage_createNewUser_password = backoffice_userAdministrationPage_createNewUser_password;
	}

	public String getBackoffice_userAdministrationPage_createNewUser_email() {
		return backoffice_userAdministrationPage_createNewUser_email;
	}

	public void setBackoffice_userAdministrationPage_createNewUser_email(
			String backoffice_userAdministrationPage_createNewUser_email) {
		this.backoffice_userAdministrationPage_createNewUser_email = backoffice_userAdministrationPage_createNewUser_email;
	}

	public String getBackoffice_searchPage_createNewuser() {
		return backoffice_searchPage_createNewuser;
	}

	public void setBackoffice_searchPage_createNewuser(String backoffice_searchPage_createNewuser) {
		this.backoffice_searchPage_createNewuser = backoffice_searchPage_createNewuser;
	}

    
    
    

    public String getBackoffice_appDetailsPage_updateCashFlow() {
        return backoffice_appDetailsPage_updateCashFlow;
    }

    public String getBackoffice_appDetails_getFinalOffer() {
        return backoffice_appDetails_getFinalOffer;
    }

    public void setBackoffice_appDetails_getFinalOffer(String backoffice_appDetails_getFinalOffer) {
        this.backoffice_appDetails_getFinalOffer = backoffice_appDetails_getFinalOffer;
    }

    public String getBackoffice_appDetails_submitGetFinalOffer() {
        return backoffice_appDetails_submitGetFinalOffer;
    }

    public void setBackoffice_appDetails_submitGetFinalOffer(String backoffice_appDetails_submitGetFinalOffer) {
        this.backoffice_appDetails_submitGetFinalOffer = backoffice_appDetails_submitGetFinalOffer;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow(String backoffice_appDetailsPage_updateCashFlow) {
        this.backoffice_appDetailsPage_updateCashFlow = backoffice_appDetailsPage_updateCashFlow;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_mothlyEMI() {
        return backoffice_appDetailsPage_updateCashFlow_mothlyEMI;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_mothlyEMI(String backoffice_appDetailsPage_updateCashFlow_mothlyEMI) {
        this.backoffice_appDetailsPage_updateCashFlow_mothlyEMI = backoffice_appDetailsPage_updateCashFlow_mothlyEMI;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_monthlyExpense() {
        return backoffice_appDetailsPage_updateCashFlow_monthlyExpense;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_monthlyExpense(String backoffice_appDetailsPage_updateCashFlow_monthlyExpense) {
        this.backoffice_appDetailsPage_updateCashFlow_monthlyExpense = backoffice_appDetailsPage_updateCashFlow_monthlyExpense;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_monthlyIncome() {
        return backoffice_appDetailsPage_updateCashFlow_monthlyIncome;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_monthlyIncome(String backoffice_appDetailsPage_updateCashFlow_monthlyIncome) {
        this.backoffice_appDetailsPage_updateCashFlow_monthlyIncome = backoffice_appDetailsPage_updateCashFlow_monthlyIncome;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_mothlyInvestment() {
        return backoffice_appDetailsPage_updateCashFlow_mothlyInvestment;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_mothlyInvestment(String backoffice_appDetailsPage_updateCashFlow_mothlyInvestment) {
        this.backoffice_appDetailsPage_updateCashFlow_mothlyInvestment = backoffice_appDetailsPage_updateCashFlow_mothlyInvestment;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_dailyAverageBalance() {
        return backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_dailyAverageBalance(String backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance) {
        this.backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance = backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_noOfECSbounce() {
        return backoffice_appDetailsPage_updateCashFlow_noOfECSbounce;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_noOfECSbounce(String backoffice_appDetailsPage_updateCashFlow_noOfECSbounce) {
        this.backoffice_appDetailsPage_updateCashFlow_noOfECSbounce = backoffice_appDetailsPage_updateCashFlow_noOfECSbounce;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_salarayDate() {
        return backoffice_appDetailsPage_updateCashFlow_salarayDate;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_salarayDate(String backoffice_appDetailsPage_updateCashFlow_salarayDate) {
        this.backoffice_appDetailsPage_updateCashFlow_salarayDate = backoffice_appDetailsPage_updateCashFlow_salarayDate;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_maximumCreditAmount() {
        return backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_maximumCreditAmount(String backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount) {
        this.backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount = backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount;
    }

    public String getBackoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit() {
        return backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit;
    }

    public void setBackoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit(String backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit) {
        this.backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit = backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit;
    }



    public String getBackoffice_appDetails_selectFinalOffer() {
        return backoffice_appDetails_selectFinalOffer;
    }

    public void setBackoffice_appDetails_selectFinalOffer(String backoffice_appDetails_selectFinalOffer) {
        this.backoffice_appDetails_selectFinalOffer = backoffice_appDetails_selectFinalOffer;
    }

    public String getBackoffice_appDetails_submitSelectFinalOffer() {
        return backoffice_appDetails_submitSelectFinalOffer;
    }

    public void setBackoffice_appDetails_submitSelectFinalOffer(String backoffice_appDetails_submitSelectFinalOffer) {
        this.backoffice_appDetails_submitSelectFinalOffer = backoffice_appDetails_submitSelectFinalOffer;
    }

    public String getBackoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton() {
        return backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton;
    }

    public void setBackoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton(String backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton) {
        this.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton = backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton;
    }

    public String getBackoffice_appDetailsPage_actions_notInterestedButton() {
        return backoffice_appDetailsPage_actions_notInterestedButton;
    }

    public void setBackoffice_appDetailsPage_actions_notInterestedButton(String backoffice_appDetailsPage_actions_notInterestedButton) {
        this.backoffice_appDetailsPage_actions_notInterestedButton = backoffice_appDetailsPage_actions_notInterestedButton;
    }

    public String getBackoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown() {
        return backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown;
    }

    public void setBackoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown(String backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown) {
        this.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown = backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown;
    }

    public String getBorrower_logoutButton() {
        return borrower_logoutButton;
    }

    public void setBorrower_logoutButton(String borrower_logoutButton) {
        this.borrower_logoutButton = borrower_logoutButton;
    }

    public String getBackoffice_searchPage_applicantPhoneTextBox() {
        return backoffice_searchPage_applicantPhoneTextBox;
    }

    public void setBackoffice_searchPage_applicantPhoneTextBox(String backoffice_searchPage_applicantPhoneTextBox) {
        this.backoffice_searchPage_applicantPhoneTextBox = backoffice_searchPage_applicantPhoneTextBox;
    }

    public String getBorrower_eligibilityPage_unableToGenerateOfferMsg() {
        return borrower_eligibilityPage_unableToGenerateOfferMsg;
    }

    public void setBorrower_eligibilityPage_unableToGenerateOfferMsg(String borrower_eligibilityPage_unableToGenerateOfferMsg) {
        this.borrower_eligibilityPage_unableToGenerateOfferMsg = borrower_eligibilityPage_unableToGenerateOfferMsg;
    }

    public String getBorrower_eligibilityPage_sorryLabel() {
        return borrower_eligibilityPage_sorryLabel;
    }

    public void setBorrower_eligibilityPage_sorryLabel(String borrower_eligibilityPage_sorryLabel) {
        this.borrower_eligibilityPage_sorryLabel = borrower_eligibilityPage_sorryLabel;
    }

    public String getBorrower_reviewPage_minimumCriteriaNoMatchMsg() {
        return borrower_reviewPage_minimumCriteriaNoMatchMsg;
    }

    public void setBorrower_reviewPage_minimumCriteriaNoMatchMsg(String borrower_reviewPage_minimumCriteriaNoMatchMsg) {
        this.borrower_reviewPage_minimumCriteriaNoMatchMsg = borrower_reviewPage_minimumCriteriaNoMatchMsg;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox(String bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox = bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails(String bankPortal_appDetailsPage_action_updateDisbursementDetails) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails = bankPortal_appDetailsPage_action_updateDisbursementDetails;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox(String bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox = bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown(String bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown = bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox(String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox = bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar(String bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar = bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox(String bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox = bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar(String bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar = bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar;
    }

    public String getBankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton() {
        return bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton;
    }

    public void setBankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton(String bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton) {
        this.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton = bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton;
    }

    public String getBankPortal_dashboard_pendingFunding() {
        return bankPortal_dashboard_pendingFunding;
    }

    public void setBankPortal_dashboard_pendingFunding(String bankPortal_dashboard_pendingFunding) {
        this.bankPortal_dashboard_pendingFunding = bankPortal_dashboard_pendingFunding;
    }

    public String getBankPortal_dashboard_pendingBankCreditReviewTab() {
        return bankPortal_dashboard_pendingBankCreditReviewTab;
    }

    public void setBankPortal_dashboard_pendingBankCreditReviewTab(String bankPortal_dashboard_pendingBankCreditReviewTab) {
        this.bankPortal_dashboard_pendingBankCreditReviewTab = bankPortal_dashboard_pendingBankCreditReviewTab;
    }

    public String getBankPortal_appDetailsPage_action_approveCreditApplication() {
        return bankPortal_appDetailsPage_action_approveCreditApplication;
    }

    public void setBankPortal_appDetailsPage_action_approveCreditApplication(String bankPortal_appDetailsPage_action_approveCreditApplication) {
        this.bankPortal_appDetailsPage_action_approveCreditApplication = bankPortal_appDetailsPage_action_approveCreditApplication;
    }

    public String getBankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton() {
        return bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton;
    }

    public void setBankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton(String bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton) {
        this.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton = bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton;
    }

    public String getBankPortal_appDetailsPage_action_approveApplication() {
        return bankPortal_appDetailsPage_action_approveApplication;
    }

    public void setBankPortal_appDetailsPage_action_approveApplication(String bankPortal_appDetailsPage_action_approveApplication) {
        this.bankPortal_appDetailsPage_action_approveApplication = bankPortal_appDetailsPage_action_approveApplication;
    }

    public String getBankPortal_appDetailsPage_action_approveApplication_approveAppButton() {
        return bankPortal_appDetailsPage_action_approveApplication_approveAppButton;
    }

    public void setBankPortal_appDetailsPage_action_approveApplication_approveAppButton(String bankPortal_appDetailsPage_action_approveApplication_approveAppButton) {
        this.bankPortal_appDetailsPage_action_approveApplication_approveAppButton = bankPortal_appDetailsPage_action_approveApplication_approveAppButton;
    }

    public String getBankPortal_appDetailsPage_headerStatus() {
        return bankPortal_appDetailsPage_headerStatus;
    }

    public void setBankPortal_appDetailsPage_headerStatus(String bankPortal_appDetailsPage_headerStatus) {
        this.bankPortal_appDetailsPage_headerStatus = bankPortal_appDetailsPage_headerStatus;
    }

    public String getBankPortal_appDetailsPage_actionButton() {
        return bankPortal_appDetailsPage_actionButton;
    }

    public void setBankPortal_appDetailsPage_actionButton(String bankPortal_appDetailsPage_actionButton) {
        this.bankPortal_appDetailsPage_actionButton = bankPortal_appDetailsPage_actionButton;
    }

    public String getBankPortal_dashboard_resultAppsMsg() {
        return bankPortal_dashboard_resultAppsMsg;
    }

    public void setBankPortal_dashboard_resultAppsMsg(String bankPortal_dashboard_resultAppsMsg) {
        this.bankPortal_dashboard_resultAppsMsg = bankPortal_dashboard_resultAppsMsg;
    }

    public String getBankPortal_loginPage_usernameTextBox() {
        return bankPortal_loginPage_usernameTextBox;
    }

    public void setBankPortal_loginPage_usernameTextBox(String bankPortal_loginPage_usernameTextBox) {
        this.bankPortal_loginPage_usernameTextBox = bankPortal_loginPage_usernameTextBox;
    }

    public String getBankPortal_loginPage_passwordTextBox() {
        return bankPortal_loginPage_passwordTextBox;
    }

    public void setBankPortal_loginPage_passwordTextBox(String bankPortal_loginPage_passwordTextBox) {
        this.bankPortal_loginPage_passwordTextBox = bankPortal_loginPage_passwordTextBox;
    }

    public String getBankPortal_loginPage_loginButton() {
        return bankPortal_loginPage_loginButton;
    }

    public void setBankPortal_loginPage_loginButton(String bankPortal_loginPage_loginButton) {
        this.bankPortal_loginPage_loginButton = bankPortal_loginPage_loginButton;
    }

    public String getBankPortal_dashboard_pendingBankFraudReviewTab() {
        return bankPortal_dashboard_pendingBankFraudReviewTab;
    }

    public void setBankPortal_dashboard_pendingBankFraudReviewTab(String bankPortal_dashboard_pendingBankFraudReviewTab) {
        this.bankPortal_dashboard_pendingBankFraudReviewTab = bankPortal_dashboard_pendingBankFraudReviewTab;
    }

    public String getBankPortal_dashboard_profileCircle() {
        return bankPortal_dashboard_profileCircle;
    }

    public void setBankPortal_dashboard_profileCircle(String bankPortal_dashboard_profileCircle) {
        this.bankPortal_dashboard_profileCircle = bankPortal_dashboard_profileCircle;
    }

    public String getBankPortal_dashboard_profile_logoutButton() {
        return bankPortal_dashboard_profile_logoutButton;
    }

    public void setBankPortal_dashboard_profile_logoutButton(String bankPortal_dashboard_profile_logoutButton) {
        this.bankPortal_dashboard_profile_logoutButton = bankPortal_dashboard_profile_logoutButton;
    }

    public String getBackoffice_appDetailsPage_action_sendToBank() {
        return backoffice_appDetailsPage_action_sendToBank;
    }

    public void setBackoffice_appDetailsPage_action_sendToBank(String backoffice_appDetailsPage_action_sendToBank) {
        this.backoffice_appDetailsPage_action_sendToBank = backoffice_appDetailsPage_action_sendToBank;
    }

    public String getBackoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton() {
        return backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton;
    }

    public void setBackoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton(String backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton) {
        this.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton = backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton;
    }

    public String getBackoffice_appDetailsPage_action_completeFulfillment() {
        return backoffice_appDetailsPage_action_completeFulfillment;
    }

    public void setBackoffice_appDetailsPage_action_completeFulfillment(String backoffice_appDetailsPage_action_completeFulfillment) {
        this.backoffice_appDetailsPage_action_completeFulfillment = backoffice_appDetailsPage_action_completeFulfillment;
    }

    public String getBackoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton() {
        return backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton;
    }

    public void setBackoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton(String backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton) {
        this.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton = backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton;
    }

    public String getBackoffice_appDetailsPage_action_documentCollectionSource() {
        return backoffice_appDetailsPage_action_documentCollectionSource;
    }

    public void setBackoffice_appDetailsPage_action_documentCollectionSource(String backoffice_appDetailsPage_action_documentCollectionSource) {
        this.backoffice_appDetailsPage_action_documentCollectionSource = backoffice_appDetailsPage_action_documentCollectionSource;
    }

    public String getBackoffice_appDetailsPage_action_documentCollectionSource_sourceRadio() {
        return backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio;
    }

    public void setBackoffice_appDetailsPage_action_documentCollectionSource_sourceRadio(String backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio) {
        this.backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio = backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio;
    }

    public String getBackoffice_appDetailsPage_action_documentCollectionSource_submitButton() {
        return backoffice_appDetailsPage_action_documentCollectionSource_submitButton;
    }

    public void setBackoffice_appDetailsPage_action_documentCollectionSource_submitButton(String backoffice_appDetailsPage_action_documentCollectionSource_submitButton) {
        this.backoffice_appDetailsPage_action_documentCollectionSource_submitButton = backoffice_appDetailsPage_action_documentCollectionSource_submitButton;
    }

    public String getBackoffice_appDetailsPage_action_addDeviation() {
        return backoffice_appDetailsPage_action_addDeviation;
    }

    public void setBackoffice_appDetailsPage_action_addDeviation(String backoffice_appDetailsPage_action_addDeviation) {
        this.backoffice_appDetailsPage_action_addDeviation = backoffice_appDetailsPage_action_addDeviation;
    }

    public String getBackoffice_appDetailsPage_action_addDeviation_deviationTextbox() {
        return backoffice_appDetailsPage_action_addDeviation_deviationTextbox;
    }

    public void setBackoffice_appDetailsPage_action_addDeviation_deviationTextbox(String backoffice_appDetailsPage_action_addDeviation_deviationTextbox) {
        this.backoffice_appDetailsPage_action_addDeviation_deviationTextbox = backoffice_appDetailsPage_action_addDeviation_deviationTextbox;
    }

    public String getBackoffice_appDetailsPage_action_addDeviation_submitButton() {
        return backoffice_appDetailsPage_action_addDeviation_submitButton;
    }

    public void setBackoffice_appDetailsPage_action_addDeviation_submitButton(String backoffice_appDetailsPage_action_addDeviation_submitButton) {
        this.backoffice_appDetailsPage_action_addDeviation_submitButton = backoffice_appDetailsPage_action_addDeviation_submitButton;
    }

    public String getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail() {
        return backoffice_appDetailsPage_action_updatePerfiosSalaryDetail;
    }

    public void setBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail(String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail) {
        this.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail = backoffice_appDetailsPage_action_updatePerfiosSalaryDetail;
    }

    public String getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox() {
        return backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox(String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox) {
        this.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox = backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown() {
        return backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown;
    }

    public void setBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown(String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown) {
        this.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown = backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown;
    }

    public String getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox() {
        return backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox(String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox) {
        this.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox = backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton() {
        return backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton;
    }

    public void setBackoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton(String backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton) {
        this.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton = backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton;
    }

    public String getBackoffice_appDetailsPage_action_addFOIRCalculation() {
        return backoffice_appDetailsPage_action_addFOIRCalculation;
    }

    public void setBackoffice_appDetailsPage_action_addFOIRCalculation(String backoffice_appDetailsPage_action_addFOIRCalculation) {
        this.backoffice_appDetailsPage_action_addFOIRCalculation = backoffice_appDetailsPage_action_addFOIRCalculation;
    }

    public String getBackoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox() {
        return backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox;
    }

    public void setBackoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox(String backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox) {
        this.backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox = backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox;
    }

    public String getBackoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox() {
        return backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox;
    }

    public void setBackoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox(String backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox) {
        this.backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox = backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox;
    }

    public String getBackoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox() {
        return backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox;
    }

    public void setBackoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox(String backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox) {
        this.backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox = backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox;
    }

    public String getBackoffice_appDetailsPage_action_addFOIRCalculation_submitButton() {
        return backoffice_appDetailsPage_action_addFOIRCalculation_submitButton;
    }

    public void setBackoffice_appDetailsPage_action_addFOIRCalculation_submitButton(String backoffice_appDetailsPage_action_addFOIRCalculation_submitButton) {
        this.backoffice_appDetailsPage_action_addFOIRCalculation_submitButton = backoffice_appDetailsPage_action_addFOIRCalculation_submitButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup(String backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup = backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData() {
        return backoffice_appDetailsPage_action_updateAncilliaryData;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData(String backoffice_appDetailsPage_action_updateAncilliaryData) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData = backoffice_appDetailsPage_action_updateAncilliaryData;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown(String backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown = backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton(String backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton = backoffice_appDetailsPage_action_updateAncilliaryData_generalCastRadioButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton(String backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton = backoffice_appDetailsPage_action_updateAncilliaryData_scCastRadioButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton(String backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton = backoffice_appDetailsPage_action_updateAncilliaryData_stCastRadioButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton(String backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton = backoffice_appDetailsPage_action_updateAncilliaryData_obcCastRadioButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton(String backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton = backoffice_appDetailsPage_action_updateAncilliaryData_otherCastRadioButton;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar(String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar = backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox(String backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox = backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox;
    }

    public String getBackoffice_appDetailsPage_action_updateAncilliaryData_submitButton() {
        return backoffice_appDetailsPage_action_updateAncilliaryData_submitButton;
    }

    public void setBackoffice_appDetailsPage_action_updateAncilliaryData_submitButton(String backoffice_appDetailsPage_action_updateAncilliaryData_submitButton) {
        this.backoffice_appDetailsPage_action_updateAncilliaryData_submitButton = backoffice_appDetailsPage_action_updateAncilliaryData_submitButton;
    }

    public String getBackoffice_appDetailsPage_actions_updateBankDetails() {
        return backoffice_appDetailsPage_actions_updateBankDetails;
    }

    public void setBackoffice_appDetailsPage_actions_updateBankDetails(String backoffice_appDetailsPage_actions_updateBankDetails) {
        this.backoffice_appDetailsPage_actions_updateBankDetails = backoffice_appDetailsPage_actions_updateBankDetails;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton() {
        return backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton(String backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton) {
        this.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton = backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton;
    }

    public String getBackoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox() {
        return backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox(String backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox) {
        this.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox = backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox() {
        return backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox(String backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox) {
        this.backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox = backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox() {
        return backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox(String backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox) {
        this.backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox = backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_updateBankDetails_submitButton() {
        return backoffice_appDetailsPage_actions_updateBankDetails_submitButton;
    }

    public void setBackoffice_appDetailsPage_actions_updateBankDetails_submitButton(String backoffice_appDetailsPage_actions_updateBankDetails_submitButton) {
        this.backoffice_appDetailsPage_actions_updateBankDetails_submitButton = backoffice_appDetailsPage_actions_updateBankDetails_submitButton;
    }

    public String getBackoffice_appDetailsPage_bottomTabs_documents() {
        return backoffice_appDetailsPage_bottomTabs_documents;
    }

    public void setBackoffice_appDetailsPage_bottomTabs_documents(String backoffice_appDetailsPage_bottomTabs_documents) {
        this.backoffice_appDetailsPage_bottomTabs_documents = backoffice_appDetailsPage_bottomTabs_documents;
    }

    public String getBackoffice_appDetailsPage_bottomTabs_documents_documentsLabel() {
        return backoffice_appDetailsPage_bottomTabs_documents_documentsLabel;
    }

    public void setBackoffice_appDetailsPage_bottomTabs_documents_documentsLabel(String backoffice_appDetailsPage_bottomTabs_documents_documentsLabel) {
        this.backoffice_appDetailsPage_bottomTabs_documents_documentsLabel = backoffice_appDetailsPage_bottomTabs_documents_documentsLabel;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton() {
        return backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton(String backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton) {
        this.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton = backoffice_appDetailsPage_verificationDashboard_signedDocDetails_closeButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg(String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg = backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel(String backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel = backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot(String backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot = backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox(String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox = backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox(String backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox = backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton(String backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton = backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton() {
        return backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton(String backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton) {
        this.backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton = backoffice_appDetailsPage_verificationDashboard_addressDetails_closeButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg(String backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg = backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel(String backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel = backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_screenshot() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_screenshot(String backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot = backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox(String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox = backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox(String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox = backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox(String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox = backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox(String backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox = backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton(String backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton = backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton;
    }

    public String getBackoffice_appDetailsPage_verificationDashboard_panDetails_closeButton() {
        return backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton;
    }

    public void setBackoffice_appDetailsPage_verificationDashboard_panDetails_closeButton(String backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton) {
        this.backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton = backoffice_appDetailsPage_verificationDashboard_panDetails_closeButton;
    }

    public String getBackoffice_appDetailsPage_actions_uploadPendingDocument_closeButton() {
        return backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton;
    }

    public void setBackoffice_appDetailsPage_actions_uploadPendingDocument_closeButton(String backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton) {
        this.backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton = backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton;
    }

    public String getBackoffice_appDetailsPage_actions_uploadPendingDocument() {
        return backoffice_appDetailsPage_actions_uploadPendingDocument;
    }

    public void setBackoffice_appDetailsPage_actions_uploadPendingDocument(String backoffice_appDetailsPage_actions_uploadPendingDocument) {
        this.backoffice_appDetailsPage_actions_uploadPendingDocument = backoffice_appDetailsPage_actions_uploadPendingDocument;
    }

    public String getBackoffice_actionCenter() {
        return backoffice_actionCenter;
    }

    public void setBackoffice_actionCenter(String backoffice_actionCenter) {
        this.backoffice_actionCenter = backoffice_actionCenter;
    }

    public String getBackoffice_appDetailsPage_personalInfoTab_personalInfoLabel() {
        return backoffice_appDetailsPage_personalInfoTab_personalInfoLabel;
    }

    public void setBackoffice_appDetailsPage_personalInfoTab_personalInfoLabel(String backoffice_appDetailsPage_personalInfoTab_personalInfoLabel) {
        this.backoffice_appDetailsPage_personalInfoTab_personalInfoLabel = backoffice_appDetailsPage_personalInfoTab_personalInfoLabel;
    }

    public String getBorrower_finalOfferPage_readyForDisbursementMsg() {
        return borrower_finalOfferPage_readyForDisbursementMsg;
    }

    public void setBorrower_finalOfferPage_readyForDisbursementMsg(String borrower_finalOfferPage_readyForDisbursementMsg) {
        this.borrower_finalOfferPage_readyForDisbursementMsg = borrower_finalOfferPage_readyForDisbursementMsg;
    }

    public String getBorrower_loginPage_loginButton() {
        return borrower_loginPage_loginButton;
    }

    public void setBorrower_loginPage_loginButton(String borrower_loginPage_loginButton) {
        this.borrower_loginPage_loginButton = borrower_loginPage_loginButton;
    }

    public String getBorrower_socialPage_nextButton() {
        return borrower_socialPage_nextButton;
    }

    public void setBorrower_socialPage_nextButton(String borrower_socialPage_nextButton) {
        this.borrower_socialPage_nextButton = borrower_socialPage_nextButton;
    }

    public String getBorrower_socialPage_verificationDoneMsg() {
        return borrower_socialPage_verificationDoneMsg;
    }

    public void setBorrower_socialPage_verificationDoneMsg(String borrower_socialPage_verificationDoneMsg) {
        this.borrower_socialPage_verificationDoneMsg = borrower_socialPage_verificationDoneMsg;
    }

    public String getBorrower_loginPage_sendOtpLink() {
        return borrower_loginPage_sendOtpLink;
    }

    public void setBorrower_loginPage_sendOtpLink(String borrower_loginPage_sendOtpLink) {
        this.borrower_loginPage_sendOtpLink = borrower_loginPage_sendOtpLink;
    }

    public String getBorrower_loginPage_signInButton() {
        return borrower_loginPage_signInButton;
    }

    public void setBorrower_loginPage_signInButton(String borrower_loginPage_signInButton) {
        this.borrower_loginPage_signInButton = borrower_loginPage_signInButton;
    }

    public String getBorrower_finalOfferPage_appReceivedMsg() {
        return borrower_finalOfferPage_appReceivedMsg;
    }

    public void setBorrower_finalOfferPage_appReceivedMsg(String borrower_finalOfferPage_appReceivedMsg) {
        this.borrower_finalOfferPage_appReceivedMsg = borrower_finalOfferPage_appReceivedMsg;
    }

    public String getBorrower_finalOfferPage_loanRadioButton() {
        return borrower_finalOfferPage_loanRadioButton;
    }

    public void setBorrower_finalOfferPage_loanRadioButton(String borrower_finalOfferPage_loanRadioButton) {
        this.borrower_finalOfferPage_loanRadioButton = borrower_finalOfferPage_loanRadioButton;
    }

    public String getBorrower_finalOfferPage_loanAmountLabel() {
        return borrower_finalOfferPage_loanAmountLabel;
    }

    public void setBorrower_finalOfferPage_loanAmountLabel(String borrower_finalOfferPage_loanAmountLabel) {
        this.borrower_finalOfferPage_loanAmountLabel = borrower_finalOfferPage_loanAmountLabel;
    }

    public String getBorrower_finalOfferPage_durationLabel() {
        return borrower_finalOfferPage_durationLabel;
    }

    public void setBorrower_finalOfferPage_durationLabel(String borrower_finalOfferPage_durationLabel) {
        this.borrower_finalOfferPage_durationLabel = borrower_finalOfferPage_durationLabel;
    }

    public String getBorrower_finalOfferPage_emiLabel() {
        return borrower_finalOfferPage_emiLabel;
    }

    public void setBorrower_finalOfferPage_emiLabel(String borrower_finalOfferPage_emiLabel) {
        this.borrower_finalOfferPage_emiLabel = borrower_finalOfferPage_emiLabel;
    }

    public String getBorrower_finalOfferPage_processingFeeLabel() {
        return borrower_finalOfferPage_processingFeeLabel;
    }

    public void setBorrower_finalOfferPage_processingFeeLabel(String borrower_finalOfferPage_processingFeeLabel) {
        this.borrower_finalOfferPage_processingFeeLabel = borrower_finalOfferPage_processingFeeLabel;
    }

    public String getBorrower_finalOfferPage_nextButton() {
        return borrower_finalOfferPage_nextButton;
    }

    public void setBorrower_finalOfferPage_nextButton(String borrower_finalOfferPage_nextButton) {
        this.borrower_finalOfferPage_nextButton = borrower_finalOfferPage_nextButton;
    }

    public String getBorrower_loginPage_mobileNumberTextBox() {
        return borrower_loginPage_mobileNumberTextBox;
    }

    public void setBorrower_loginPage_mobileNumberTextBox(String borrower_loginPage_mobileNumberTextBox) {
        this.borrower_loginPage_mobileNumberTextBox = borrower_loginPage_mobileNumberTextBox;
    }

    public String getBorrower_loginPage_otpTextBox() {
        return borrower_loginPage_otpTextBox;
    }

    public void setBorrower_loginPage_otpTextBox(String borrower_loginPage_otpTextBox) {
        this.borrower_loginPage_otpTextBox = borrower_loginPage_otpTextBox;
    }

    public String getBackoffice_searchPage_profile_logoutButton() {
        return backoffice_searchPage_profile_logoutButton;
    }

    public void setBackoffice_searchPage_profile_logoutButton(String backoffice_searchPage_profile_logoutButton) {
        this.backoffice_searchPage_profile_logoutButton = backoffice_searchPage_profile_logoutButton;
    }

    public String getBackoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton() {
        return backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton;
    }

    public void setBackoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton(String backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton) {
        this.backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton = backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_termDropdown() {
        return backoffice_appDetailsPage_actions_addManualOffer_termDropdown;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_termDropdown(String backoffice_appDetailsPage_actions_addManualOffer_termDropdown) {
        this.backoffice_appDetailsPage_actions_addManualOffer_termDropdown = backoffice_appDetailsPage_actions_addManualOffer_termDropdown;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox() {
        return backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox(String backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox) {
        this.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox = backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox() {
        return backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox(String backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox) {
        this.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox = backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_emiTextBox() {
        return backoffice_appDetailsPage_actions_addManualOffer_emiTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_emiTextBox(String backoffice_appDetailsPage_actions_addManualOffer_emiTextBox) {
        this.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox = backoffice_appDetailsPage_actions_addManualOffer_emiTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox() {
        return backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox(String backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox) {
        this.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox = backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox() {
        return backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox(String backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox) {
        this.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox = backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOffer_submitButton() {
        return backoffice_appDetailsPage_actions_addManualOffer_submitButton;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOffer_submitButton(String backoffice_appDetailsPage_actions_addManualOffer_submitButton) {
        this.backoffice_appDetailsPage_actions_addManualOffer_submitButton = backoffice_appDetailsPage_actions_addManualOffer_submitButton;
    }

    public String getBackoffice_appDetailsPage_actions_addManualOfferButton() {
        return backoffice_appDetailsPage_actions_addManualOfferButton;
    }

    public void setBackoffice_appDetailsPage_actions_addManualOfferButton(String backoffice_appDetailsPage_actions_addManualOfferButton) {
        this.backoffice_appDetailsPage_actions_addManualOfferButton = backoffice_appDetailsPage_actions_addManualOfferButton;
    }

    public String getBackoffice_appDetailsPage_actions_presentFinalOfferButton() {
        return backoffice_appDetailsPage_actions_presentFinalOfferButton;
    }

    public void setBackoffice_appDetailsPage_actions_presentFinalOfferButton(String backoffice_appDetailsPage_actions_presentFinalOfferButton) {
        this.backoffice_appDetailsPage_actions_presentFinalOfferButton = backoffice_appDetailsPage_actions_presentFinalOfferButton;
    }

    public String getBackoffice_appDetailsPage_headerStatus() {
        return backoffice_appDetailsPage_headerStatus;
    }

    public void setBackoffice_appDetailsPage_headerStatus(String backoffice_appDetailsPage_headerStatus) {
        this.backoffice_appDetailsPage_headerStatus = backoffice_appDetailsPage_headerStatus;
    }

    public String getBackoffice_appDetailsPage_verificationDashboardTab() {
        return backoffice_appDetailsPage_verificationDashboardTab;
    }

    public void setBackoffice_appDetailsPage_verificationDashboardTab(String backoffice_appDetailsPage_verificationDashboardTab) {
        this.backoffice_appDetailsPage_verificationDashboardTab = backoffice_appDetailsPage_verificationDashboardTab;
    }

    public String getBackoffice_appDetailsPage_actionButton() {
        return backoffice_appDetailsPage_actionButton;
    }

    public void setBackoffice_appDetailsPage_actionButton(String backoffice_appDetailsPage_actionButton) {
        this.backoffice_appDetailsPage_actionButton = backoffice_appDetailsPage_actionButton;
    }

    public String getBackoffice_searchPage_profileCircle() {
        return backoffice_searchPage_profileCircle;
    }

    public void setBackoffice_searchPage_profileCircle(String backoffice_searchPage_profileCircle) {
        this.backoffice_searchPage_profileCircle = backoffice_searchPage_profileCircle;
    }

    public String getBackoffice_searchPage_applicationNumberTextBox() {
        return backoffice_searchPage_applicationNumberTextBox;
    }

    public void setBackoffice_searchPage_applicationNumberTextBox(String backoffice_searchPage_applicationNumberTextBox) {
        this.backoffice_searchPage_applicationNumberTextBox = backoffice_searchPage_applicationNumberTextBox;
    }

    public String getBackoffice_searchPage_searchButton() {
        return backoffice_searchPage_searchButton;
    }

    public void setBackoffice_searchPage_searchButton(String backoffice_searchPage_searchButton) {
        this.backoffice_searchPage_searchButton = backoffice_searchPage_searchButton;
    }

    public String getBackoffice_searchPage_resultData_appId() {
        return backoffice_searchPage_resultData_appId;
    }

    public void setBackoffice_searchPage_resultData_appId(String backoffice_searchPage_resultData_appId) {
        this.backoffice_searchPage_resultData_appId = backoffice_searchPage_resultData_appId;
    }

    public String getBackoffice_searchPage_resultData_appDate() {
        return backoffice_searchPage_resultData_appDate;
    }

    public void setBackoffice_searchPage_resultData_appDate(String backoffice_searchPage_resultData_appDate) {
        this.backoffice_searchPage_resultData_appDate = backoffice_searchPage_resultData_appDate;
    }

    public String getBackoffice_searchPage_resultData_applicantName() {
        return backoffice_searchPage_resultData_applicantName;
    }

    public void setBackoffice_searchPage_resultData_applicantName(String backoffice_searchPage_resultData_applicantName) {
        this.backoffice_searchPage_resultData_applicantName = backoffice_searchPage_resultData_applicantName;
    }

    public String getBackoffice_searchPage_resultData_phoneNumber() {
        return backoffice_searchPage_resultData_phoneNumber;
    }

    public void setBackoffice_searchPage_resultData_phoneNumber(String backoffice_searchPage_resultData_phoneNumber) {
        this.backoffice_searchPage_resultData_phoneNumber = backoffice_searchPage_resultData_phoneNumber;
    }

    public String getBackoffice_searchPage_resultData_loanAmount() {
        return backoffice_searchPage_resultData_loanAmount;
    }

    public void setBackoffice_searchPage_resultData_loanAmount(String backoffice_searchPage_resultData_loanAmount) {
        this.backoffice_searchPage_resultData_loanAmount = backoffice_searchPage_resultData_loanAmount;
    }

    public String getBackoffice_searchPage_resultData_employerName() {
        return backoffice_searchPage_resultData_employerName;
    }

    public void setBackoffice_searchPage_resultData_employerName(String backoffice_searchPage_resultData_employerName) {
        this.backoffice_searchPage_resultData_employerName = backoffice_searchPage_resultData_employerName;
    }

    public String getBackoffice_searchPage_resultData_expiryDate() {
        return backoffice_searchPage_resultData_expiryDate;
    }

    public void setBackoffice_searchPage_resultData_expiryDate(String backoffice_searchPage_resultData_expiryDate) {
        this.backoffice_searchPage_resultData_expiryDate = backoffice_searchPage_resultData_expiryDate;
    }

    public String getBackoffice_searchPage_resultData_source() {
        return backoffice_searchPage_resultData_source;
    }

    public void setBackoffice_searchPage_resultData_source(String backoffice_searchPage_resultData_source) {
        this.backoffice_searchPage_resultData_source = backoffice_searchPage_resultData_source;
    }

    public String getBackoffice_searchPage_resultData_detailsButton() {
        return backoffice_searchPage_resultData_detailsButton;
    }

    public void setBackoffice_searchPage_resultData_detailsButton(String backoffice_searchPage_resultData_detailsButton) {
        this.backoffice_searchPage_resultData_detailsButton = backoffice_searchPage_resultData_detailsButton;
    }

    public String getBorrower_socialPage_verifyMeImage() {
        return borrower_socialPage_verifyMeImage;
    }

    public void setBorrower_socialPage_verifyMeImage(String borrower_socialPage_verifyMeImage) {
        this.borrower_socialPage_verifyMeImage = borrower_socialPage_verifyMeImage;
    }

    public String getBorrower_logoutLink() {
        return borrower_logoutLink;
    }

    public void setBorrower_logoutLink(String borrower_logoutLink) {
        this.borrower_logoutLink = borrower_logoutLink;
    }

    public String getBorrower_appComplete_getUniqueReferralButton() {
        return borrower_appComplete_getUniqueReferralButton;
    }

    public void setBorrower_appComplete_getUniqueReferralButton(String borrower_appComplete_getUniqueReferralButton) {
        this.borrower_appComplete_getUniqueReferralButton = borrower_appComplete_getUniqueReferralButton;
    }

    public String getBorrower_appComplete_message() {
        return borrower_appComplete_message;
    }

    public void setBorrower_appComplete_message(String borrower_appComplete_message) {
        this.borrower_appComplete_message = borrower_appComplete_message;
    }

    public String getBorrower_socialPage_skipButton() {
        return borrower_socialPage_skipButton;
    }

    public void setBorrower_socialPage_skipButton(String borrower_socialPage_skipButton) {
        this.borrower_socialPage_skipButton = borrower_socialPage_skipButton;
    }

    public String getBorrower_employmentPage_designationTextBox() {
        return borrower_employmentPage_designationTextBox;
    }

    public void setBorrower_employmentPage_designationTextBox(String borrower_employmentPage_designationTextBox) {
        this.borrower_employmentPage_designationTextBox = borrower_employmentPage_designationTextBox;
    }

    public String getBorrower_employmentPage_workExperienceDropdown() {
        return borrower_employmentPage_workExperienceDropdown;
    }

    public void setBorrower_employmentPage_workExperienceDropdown(String borrower_employmentPage_workExperienceDropdown) {
        this.borrower_employmentPage_workExperienceDropdown = borrower_employmentPage_workExperienceDropdown;
    }

    public String getBorrower_employmentPage_address1TextBox() {
        return borrower_employmentPage_address1TextBox;
    }

    public void setBorrower_employmentPage_address1TextBox(String borrower_employmentPage_address1TextBox) {
        this.borrower_employmentPage_address1TextBox = borrower_employmentPage_address1TextBox;
    }

    public String getBorrower_employmentPage_localityTextBox() {
        return borrower_employmentPage_localityTextBox;
    }

    public void setBorrower_employmentPage_localityTextBox(String borrower_employmentPage_localityTextBox) {
        this.borrower_employmentPage_localityTextBox = borrower_employmentPage_localityTextBox;
    }

    public String getBorrower_employmentPage_pincodeTextBox() {
        return borrower_employmentPage_pincodeTextBox;
    }

    public void setBorrower_employmentPage_pincodeTextBox(String borrower_employmentPage_pincodeTextBox) {
        this.borrower_employmentPage_pincodeTextBox = borrower_employmentPage_pincodeTextBox;
    }

    public String getBorrower_employmentPage_nextButton() {
        return borrower_employmentPage_nextButton;
    }

    public void setBorrower_employmentPage_nextButton(String borrower_employmentPage_nextButton) {
        this.borrower_employmentPage_nextButton = borrower_employmentPage_nextButton;
    }

    public String getBorrower_employmentPage_employerName() {
        return borrower_employmentPage_employerName;
    }

    public void setBorrower_employmentPage_employerName(String borrower_employmentPage_employerName) {
        this.borrower_employmentPage_employerName = borrower_employmentPage_employerName;
    }

    public String getBorrower_educationPage_nextButton() {
        return borrower_educationPage_nextButton;
    }

    public void setBorrower_educationPage_nextButton(String borrower_educationPage_nextButton) {
        this.borrower_educationPage_nextButton = borrower_educationPage_nextButton;
    }

    public String getBorrower_educationPage_educationalInstituteTextBox() {
        return borrower_educationPage_educationalInstituteTextBox;
    }

    public void setBorrower_educationPage_educationalInstituteTextBox(String borrower_educationPage_educationalInstituteTextBox) {
        this.borrower_educationPage_educationalInstituteTextBox = borrower_educationPage_educationalInstituteTextBox;
    }

    public String getBorrower_educationPage_educationLevelDropdown() {
        return borrower_educationPage_educationLevelDropdown;
    }

    public void setBorrower_educationPage_educationLevelDropdown(String borrower_educationPage_educationLevelDropdown) {
        this.borrower_educationPage_educationLevelDropdown = borrower_educationPage_educationLevelDropdown;
    }

    public String getBorrower_bankVerificationPage_uploadBankStatement_nextButton() {
        return borrower_bankVerificationPage_uploadBankStatement_nextButton;
    }

    public void setBorrower_bankVerificationPage_uploadBankStatement_nextButton(String borrower_bankVerificationPage_uploadBankStatement_nextButton) {
        this.borrower_bankVerificationPage_uploadBankStatement_nextButton = borrower_bankVerificationPage_uploadBankStatement_nextButton;
    }

    public String getBorrower_bankVerificationPage_uploadBankStatement_uploadFileArea() {
        return borrower_bankVerificationPage_uploadBankStatement_uploadFileArea;
    }

    public void setBorrower_bankVerificationPage_uploadBankStatement_uploadFileArea(String borrower_bankVerificationPage_uploadBankStatement_uploadFileArea) {
        this.borrower_bankVerificationPage_uploadBankStatement_uploadFileArea = borrower_bankVerificationPage_uploadBankStatement_uploadFileArea;
    }

    public String getBorrower_bankVerificationPage_uploadBankStatement_bankTextBox() {
        return borrower_bankVerificationPage_uploadBankStatement_bankTextBox;
    }

    public void setBorrower_bankVerificationPage_uploadBankStatement_bankTextBox(String borrower_bankVerificationPage_uploadBankStatement_bankTextBox) {
        this.borrower_bankVerificationPage_uploadBankStatement_bankTextBox = borrower_bankVerificationPage_uploadBankStatement_bankTextBox;
    }

    public String getBorrower_bankVerificationPage_connectAccountRadioButton() {
        return borrower_bankVerificationPage_connectAccountRadioButton;
    }

    public void setBorrower_bankVerificationPage_connectAccountRadioButton(String borrower_bankVerificationPage_connectAccountRadioButton) {
        this.borrower_bankVerificationPage_connectAccountRadioButton = borrower_bankVerificationPage_connectAccountRadioButton;
    }

    public String getBorrower_bankVerificationPage_uploadFileRadioButton() {
        return borrower_bankVerificationPage_uploadFileRadioButton;
    }

    public void setBorrower_bankVerificationPage_uploadFileRadioButton(String borrower_bankVerificationPage_uploadFileRadioButton) {
        this.borrower_bankVerificationPage_uploadFileRadioButton = borrower_bankVerificationPage_uploadFileRadioButton;
    }

    public String getBorrower_bankVerificationPage_continueButton() {
        return borrower_bankVerificationPage_continueButton;
    }

    public void setBorrower_bankVerificationPage_continueButton(String borrower_bankVerificationPage_continueButton) {
        this.borrower_bankVerificationPage_continueButton = borrower_bankVerificationPage_continueButton;
    }

    public String getBorrower_eligibilityPage_greetLabel() {
        return borrower_eligibilityPage_greetLabel;
    }

    public void setBorrower_eligibilityPage_greetLabel(String borrower_eligibilityPage_greetLabel) {
        this.borrower_eligibilityPage_greetLabel = borrower_eligibilityPage_greetLabel;
    }

    public String getBorrower_eligibilityPage_applicationNumberLabel() {
        return borrower_eligibilityPage_applicationNumberLabel;
    }

    public void setBorrower_eligibilityPage_applicationNumberLabel(String borrower_eligibilityPage_applicationNumberLabel) {
        this.borrower_eligibilityPage_applicationNumberLabel = borrower_eligibilityPage_applicationNumberLabel;
    }

    public String getBorrower_eligibilityPage_nextButton() {
        return borrower_eligibilityPage_nextButton;
    }

    public void setBorrower_eligibilityPage_nextButton(String borrower_eligibilityPage_nextButton) {
        this.borrower_eligibilityPage_nextButton = borrower_eligibilityPage_nextButton;
    }

    public String getBorrower_identificationDetails_detailsLabel() {
        return borrower_identificationDetails_detailsLabel;
    }

    public void setBorrower_identificationDetails_detailsLabel(String borrower_identificationDetails_detailsLabel) {
        this.borrower_identificationDetails_detailsLabel = borrower_identificationDetails_detailsLabel;
    }

    public String getBorrower_expensesPage_expensesLabel() {
        return borrower_expensesPage_expensesLabel;
    }

    public void setBorrower_expensesPage_expensesLabel(String borrower_expensesPage_expensesLabel) {
        this.borrower_expensesPage_expensesLabel = borrower_expensesPage_expensesLabel;
    }

    public String getBorrower_expensesPage_residenseTypeDropdown() {
        return borrower_expensesPage_residenseTypeDropdown;
    }

    public void setBorrower_expensesPage_residenseTypeDropdown(String borrower_expensesPage_residenseTypeDropdown) {
        this.borrower_expensesPage_residenseTypeDropdown = borrower_expensesPage_residenseTypeDropdown;
    }

    public String getBorrower_expensesPage_nextButton() {
        return borrower_expensesPage_nextButton;
    }

    public void setBorrower_expensesPage_nextButton(String borrower_expensesPage_nextButton) {
        this.borrower_expensesPage_nextButton = borrower_expensesPage_nextButton;
    }

    public String getBorrower_employmentDetailspage_whatAboutWorkLabel() {
        return borrower_employmentDetailspage_whatAboutWorkLabel;
    }

    public void setBorrower_employmentDetailspage_whatAboutWorkLabel(String borrower_employmentDetailspage_whatAboutWorkLabel) {
        this.borrower_employmentDetailspage_whatAboutWorkLabel = borrower_employmentDetailspage_whatAboutWorkLabel;
    }

    public String getBorrower_reviewPage_reviewApplicationLabel() {
        return borrower_reviewPage_reviewApplicationLabel;
    }

    public void setBorrower_reviewPage_reviewApplicationLabel(String borrower_reviewPage_reviewApplicationLabel) {
        this.borrower_reviewPage_reviewApplicationLabel = borrower_reviewPage_reviewApplicationLabel;
    }

    public String getBorrower_reviewPage_nextButton() {
        return borrower_reviewPage_nextButton;
    }

    public void setBorrower_reviewPage_nextButton(String borrower_reviewPage_nextButton) {
        this.borrower_reviewPage_nextButton = borrower_reviewPage_nextButton;
    }

    public String getBorrower_reviewPage_agreementCheckbox() {
        return borrower_reviewPage_agreementCheckbox;
    }

    public void setBorrower_reviewPage_agreementCheckbox(String borrower_reviewPage_agreementCheckbox) {
        this.borrower_reviewPage_agreementCheckbox = borrower_reviewPage_agreementCheckbox;
    }

    public String getBorrower_identificationDetails_dateDropdown() {
        return borrower_identificationDetails_dateDropdown;
    }

    public void setBorrower_identificationDetails_dateDropdown(String borrower_identificationDetails_dateDropdown) {
        this.borrower_identificationDetails_dateDropdown = borrower_identificationDetails_dateDropdown;
    }

    public String getBorrower_identificationDetails_monthDropdown() {
        return borrower_identificationDetails_monthDropdown;
    }

    public void setBorrower_identificationDetails_monthDropdown(String borrower_identificationDetails_monthDropdown) {
        this.borrower_identificationDetails_monthDropdown = borrower_identificationDetails_monthDropdown;
    }

    public String getBorrower_identificationDetails_yearDropdown() {
        return borrower_identificationDetails_yearDropdown;
    }

    public void setBorrower_identificationDetails_yearDropdown(String borrower_identificationDetails_yearDropdown) {
        this.borrower_identificationDetails_yearDropdown = borrower_identificationDetails_yearDropdown;
    }

    public String getBorrower_identificationDetails_idDetailsLabel() {
        return borrower_identificationDetails_idDetailsLabel;
    }

    public void setBorrower_identificationDetails_idDetailsLabel(String borrower_identificationDetails_idDetailsLabel) {
        this.borrower_identificationDetails_idDetailsLabel = borrower_identificationDetails_idDetailsLabel;
    }

    public String getBorrower_identificationDetails_aadharNumberTextBox() {
        return borrower_identificationDetails_aadharNumberTextBox;
    }

    public void setBorrower_identificationDetails_aadharNumberTextBox(String borrower_identificationDetails_aadharNumberTextBox) {
        this.borrower_identificationDetails_aadharNumberTextBox = borrower_identificationDetails_aadharNumberTextBox;
    }

    public String getBorrower_identificationDetails_panNumberTextBox() {
        return borrower_identificationDetails_panNumberTextBox;
    }

    public void setBorrower_identificationDetails_panNumberTextBox(String borrower_identificationDetails_panNumberTextBox) {
        this.borrower_identificationDetails_panNumberTextBox = borrower_identificationDetails_panNumberTextBox;
    }

    public String getBorrower_identificationDetails_address1TextBox() {
        return borrower_identificationDetails_address1TextBox;
    }

    public void setBorrower_identificationDetails_address1TextBox(String borrower_identificationDetails_address1TextBox) {
        this.borrower_identificationDetails_address1TextBox = borrower_identificationDetails_address1TextBox;
    }

    public String getBorrower_identificationDetails_address2TextBox() {
        return borrower_identificationDetails_address2TextBox;
    }

    public void setBorrower_identificationDetails_address2TextBox(String borrower_identificationDetails_address2TextBox) {
        this.borrower_identificationDetails_address2TextBox = borrower_identificationDetails_address2TextBox;
    }

    public String getBorrower_identificationDetails_localityTextBox() {
        return borrower_identificationDetails_localityTextBox;
    }

    public void setBorrower_identificationDetails_localityTextBox(String borrower_identificationDetails_localityTextBox) {
        this.borrower_identificationDetails_localityTextBox = borrower_identificationDetails_localityTextBox;
    }

    public String getBorrower_identificationDetails_pincodeTextBox() {
        return borrower_identificationDetails_pincodeTextBox;
    }

    public void setBorrower_identificationDetails_pincodeTextBox(String borrower_identificationDetails_pincodeTextBox) {
        this.borrower_identificationDetails_pincodeTextBox = borrower_identificationDetails_pincodeTextBox;
    }

    public String getBorrower_identificationDetails_cityTextBox() {
        return borrower_identificationDetails_cityTextBox;
    }

    public void setBorrower_identificationDetails_cityTextBox(String borrower_identificationDetails_cityTextBox) {
        this.borrower_identificationDetails_cityTextBox = borrower_identificationDetails_cityTextBox;
    }

    public String getBorrower_identificationDetails_stateTextBox() {
        return borrower_identificationDetails_stateTextBox;
    }

    public void setBorrower_identificationDetails_stateTextBox(String borrower_identificationDetails_stateTextBox) {
        this.borrower_identificationDetails_stateTextBox = borrower_identificationDetails_stateTextBox;
    }

    public String getBorrower_identificationDetails_bothAddressSameButton() {
        return borrower_identificationDetails_bothAddressSameButton;
    }

    public void setBorrower_identificationDetails_bothAddressSameButton(String borrower_identificationDetails_bothAddressSameButton) {
        this.borrower_identificationDetails_bothAddressSameButton = borrower_identificationDetails_bothAddressSameButton;
    }

    public String getBorrower_identificationDetails_bothAddressDifferentButton() {
        return borrower_identificationDetails_bothAddressDifferentButton;
    }

    public void setBorrower_identificationDetails_bothAddressDifferentButton(String borrower_identificationDetails_bothAddressDifferentButton) {
        this.borrower_identificationDetails_bothAddressDifferentButton = borrower_identificationDetails_bothAddressDifferentButton;
    }

    public String getBorrower_identificationDetails_nextButton() {
        return borrower_identificationDetails_nextButton;
    }

    public void setBorrower_identificationDetails_nextButton(String borrower_identificationDetails_nextButton) {
        this.borrower_identificationDetails_nextButton = borrower_identificationDetails_nextButton;
    }

    public String getBorrower_employmentDetailspage_legalBusinessTextBox() {
        return borrower_employmentDetailspage_legalBusinessTextBox;
    }

    public void setBorrower_employmentDetailspage_legalBusinessTextBox(String borrower_employmentDetailspage_legalBusinessTextBox) {
        this.borrower_employmentDetailspage_legalBusinessTextBox = borrower_employmentDetailspage_legalBusinessTextBox;
    }

    public String getBorrower_employmentDetailspage_takeHomeSalaryTextBox() {
        return borrower_employmentDetailspage_takeHomeSalaryTextBox;
    }

    public void setBorrower_employmentDetailspage_takeHomeSalaryTextBox(String borrower_employmentDetailspage_takeHomeSalaryTextBox) {
        this.borrower_employmentDetailspage_takeHomeSalaryTextBox = borrower_employmentDetailspage_takeHomeSalaryTextBox;
    }

    public String getBorrower_employmentDetailspage_officialEmailIdTextBox() {
        return borrower_employmentDetailspage_officialEmailIdTextBox;
    }

    public void setBorrower_employmentDetailspage_officialEmailIdTextBox(String borrower_employmentDetailspage_officialEmailIdTextBox) {
        this.borrower_employmentDetailspage_officialEmailIdTextBox = borrower_employmentDetailspage_officialEmailIdTextBox;
    }

    public String getBorrower_employmentDetailspage_nextButton() {
        return borrower_employmentDetailspage_nextButton;
    }

    public void setBorrower_employmentDetailspage_nextButton(String borrower_employmentDetailspage_nextButton) {
        this.borrower_employmentDetailspage_nextButton = borrower_employmentDetailspage_nextButton;
    }

    public String getBorrower_accountCreationpage_otpSentLabel() {
        return borrower_accountCreationpage_otpSentLabel;
    }

    public void setBorrower_accountCreationpage_otpSentLabel(String borrower_accountCreationpage_otpSentLabel) {
        this.borrower_accountCreationpage_otpSentLabel = borrower_accountCreationpage_otpSentLabel;
    }

    public String getBorrower_accountCreationpage_otpTextBox() {
        return borrower_accountCreationpage_otpTextBox;
    }

    public void setBorrower_accountCreationpage_otpTextBox(String borrower_accountCreationpage_otpTextBox) {
        this.borrower_accountCreationpage_otpTextBox = borrower_accountCreationpage_otpTextBox;
    }

    public String getBorrower_accountCreationpage_otpNextButton() {
        return borrower_accountCreationpage_otpNextButton;
    }

    public void setBorrower_accountCreationpage_otpNextButton(String borrower_accountCreationpage_otpNextButton) {
        this.borrower_accountCreationpage_otpNextButton = borrower_accountCreationpage_otpNextButton;
    }

    public String getBorrower_accountCreationpage_mobileVerificationLabel() {
        return borrower_accountCreationpage_mobileVerificationLabel;
    }

    public void setBorrower_accountCreationpage_mobileVerificationLabel(String borrower_accountCreationpage_mobileVerificationLabel) {
        this.borrower_accountCreationpage_mobileVerificationLabel = borrower_accountCreationpage_mobileVerificationLabel;
    }

    public String getBorrower_accountCreationpage_mobileNumberTextBox() {
        return borrower_accountCreationpage_mobileNumberTextBox;
    }

    public void setBorrower_accountCreationpage_mobileNumberTextBox(String borrower_accountCreationpage_mobileNumberTextBox) {
        this.borrower_accountCreationpage_mobileNumberTextBox = borrower_accountCreationpage_mobileNumberTextBox;
    }

    public String getBorrower_accountCreationpage_nextButton() {
        return borrower_accountCreationpage_nextButton;
    }

    public void setBorrower_accountCreationpage_nextButton(String borrower_accountCreationpage_nextButton) {
        this.borrower_accountCreationpage_nextButton = borrower_accountCreationpage_nextButton;
    }

    public String getBorrower_profilePage_personalInfoLabel() {
        return borrower_profilePage_personalInfoLabel;
    }

    public void setBorrower_profilePage_personalInfoLabel(String borrower_profilePage_personalInfoLabel) {
        this.borrower_profilePage_personalInfoLabel = borrower_profilePage_personalInfoLabel;
    }

    public String getBorrower_profilePage_mrTitleButton() {
        return borrower_profilePage_mrTitleButton;
    }

    public void setBorrower_profilePage_mrTitleButton(String borrower_profilePage_mrTitleButton) {
        this.borrower_profilePage_mrTitleButton = borrower_profilePage_mrTitleButton;
    }

    public String getBorrower_profilePage_msTitleButton() {
        return borrower_profilePage_msTitleButton;
    }

    public void setBorrower_profilePage_msTitleButton(String borrower_profilePage_msTitleButton) {
        this.borrower_profilePage_msTitleButton = borrower_profilePage_msTitleButton;
    }

    public String getBorrower_profilePage_mrsTitleButton() {
        return borrower_profilePage_mrsTitleButton;
    }

    public void setBorrower_profilePage_mrsTitleButton(String borrower_profilePage_mrsTitleButton) {
        this.borrower_profilePage_mrsTitleButton = borrower_profilePage_mrsTitleButton;
    }

    public String getBorrower_profilePage_maritalStatusSingleButton() {
        return borrower_profilePage_maritalStatusSingleButton;
    }

    public void setBorrower_profilePage_maritalStatusSingleButton(String borrower_profilePage_maritalStatusSingleButton) {
        this.borrower_profilePage_maritalStatusSingleButton = borrower_profilePage_maritalStatusSingleButton;
    }

    public String getBorrower_profilePage_maritalStatusMarriedButton() {
        return borrower_profilePage_maritalStatusMarriedButton;
    }

    public void setBorrower_profilePage_maritalStatusMarriedButton(String borrower_profilePage_maritalStatusMarriedButton) {
        this.borrower_profilePage_maritalStatusMarriedButton = borrower_profilePage_maritalStatusMarriedButton;
    }

    public String getBorrower_profilePage_firstNameTextBox() {
        return borrower_profilePage_firstNameTextBox;
    }

    public void setBorrower_profilePage_firstNameTextBox(String borrower_profilePage_firstNameTextBox) {
        this.borrower_profilePage_firstNameTextBox = borrower_profilePage_firstNameTextBox;
    }

    public String getBorrower_profilePage_middleNameTextBox() {
        return borrower_profilePage_middleNameTextBox;
    }

    public void setBorrower_profilePage_middleNameTextBox(String borrower_profilePage_middleNameTextBox) {
        this.borrower_profilePage_middleNameTextBox = borrower_profilePage_middleNameTextBox;
    }

    public String getBorrower_profilePage_lastNameTextBox() {
        return borrower_profilePage_lastNameTextBox;
    }

    public void setBorrower_profilePage_lastNameTextBox(String borrower_profilePage_lastNameTextBox) {
        this.borrower_profilePage_lastNameTextBox = borrower_profilePage_lastNameTextBox;
    }

    public String getBorrower_profilePage_emailIdTextBox() {
        return borrower_profilePage_emailIdTextBox;
    }

    public void setBorrower_profilePage_emailIdTextBox(String borrower_profilePage_emailIdTextBox) {
        this.borrower_profilePage_emailIdTextBox = borrower_profilePage_emailIdTextBox;
    }

    public String getBorrower_profilePage_nextButton() {
        return borrower_profilePage_nextButton;
    }

    public void setBorrower_profilePage_nextButton(String borrower_profilePage_nextButton) {
        this.borrower_profilePage_nextButton = borrower_profilePage_nextButton;
    }

    public String getBackoffice_loginPage_usernameTextBox() {
        return backoffice_loginPage_usernameTextBox;
    }

    public String getBackoffice_loginPage_passwordTextBox() {
        return backoffice_loginPage_passwordTextBox;
    }

    public String getBackoffice_loginPage_loginButton() {
        return backoffice_loginPage_loginButton;
    }

    public String getBorrower_loanAmountpage_howMuchDoYouNeedLabel() {
        return borrower_loanAmountpage_howMuchDoYouNeedLabel;
    }

    public String getBorrower_loanAmountpage_loanDetailsMenu() {
        return borrower_loanAmountpage_loanDetailsMenu;
    }

    public String getBorrower_loanAmountpage_nextButton() {
        return borrower_loanAmountpage_nextButton;
    }

    public String getBorrower_loanAmountpage_amountTextBox() {
        return borrower_loanAmountpage_amountTextBox;
    }

    public String getBorrower_loanPurposepage_needPurposeLabel() {
        return borrower_loanPurposepage_needPurposeLabel;
    }

    public String getBorrower_loanPurposepage_travelButton() {
        return borrower_loanPurposepage_travelButton;
    }

    public String getBorrower_loanPurposepage_vehiclePurposeButton() {
        return borrower_loanPurposepage_vehiclePurposeButton;
    }

    public String getBorrower_loanPurposepage_medicalButton() {
        return borrower_loanPurposepage_medicalButton;
    }

    public String getBorrower_loanPurposepage_loanRefinanceButton() {
        return borrower_loanPurposepage_loanRefinanceButton;
    }

    public String getBorrower_loanPurposepage_weddingButton() {
        return borrower_loanPurposepage_weddingButton;
    }

    public String getBorrower_loanPurposepage_homeImprovementButton() {
        return borrower_loanPurposepage_homeImprovementButton;
    }

    public String getBorrower_loanPurposepage_nextButton() {
        return borrower_loanPurposepage_nextButton;
    }

    public void setBackoffice_loginPage_usernameTextBox(String backoffice_loginPage_usernameTextBox) {
        this.backoffice_loginPage_usernameTextBox = backoffice_loginPage_usernameTextBox;
    }

    public void setBackoffice_loginPage_passwordTextBox(String backoffice_loginPage_passwordTextBox) {
        this.backoffice_loginPage_passwordTextBox = backoffice_loginPage_passwordTextBox;
    }

    public void setBackoffice_loginPage_loginButton(String backoffice_loginPage_loginButton) {
        this.backoffice_loginPage_loginButton = backoffice_loginPage_loginButton;
    }

    public void setBorrower_loanAmountpage_howMuchDoYouNeedLabel(String borrower_loanAmountpage_howMuchDoYouNeedLabel) {
        this.borrower_loanAmountpage_howMuchDoYouNeedLabel = borrower_loanAmountpage_howMuchDoYouNeedLabel;
    }

    public void setBorrower_loanAmountpage_loanDetailsMenu(String borrower_loanAmountpage_loanDetailsMenu) {
        this.borrower_loanAmountpage_loanDetailsMenu = borrower_loanAmountpage_loanDetailsMenu;
    }

    public void setBorrower_loanAmountpage_nextButton(String borrower_loanAmountpage_nextButton) {
        this.borrower_loanAmountpage_nextButton = borrower_loanAmountpage_nextButton;
    }

    public void setBorrower_loanAmountpage_amountTextBox(String borrower_loanAmountpage_amountTextBox) {
        this.borrower_loanAmountpage_amountTextBox = borrower_loanAmountpage_amountTextBox;
    }

    public void setBorrower_loanPurposepage_needPurposeLabel(String borrower_loanPurposepage_needPurposeLabel) {
        this.borrower_loanPurposepage_needPurposeLabel = borrower_loanPurposepage_needPurposeLabel;
    }

    public void setBorrower_loanPurposepage_travelButton(String borrower_loanPurposepage_travelButton) {
        this.borrower_loanPurposepage_travelButton = borrower_loanPurposepage_travelButton;
    }

    public void setBorrower_loanPurposepage_vehiclePurposeButton(String borrower_loanPurposepage_vehiclePurposeButton) {
        this.borrower_loanPurposepage_vehiclePurposeButton = borrower_loanPurposepage_vehiclePurposeButton;
    }

    public void setBorrower_loanPurposepage_medicalButton(String borrower_loanPurposepage_medicalButton) {
        this.borrower_loanPurposepage_medicalButton = borrower_loanPurposepage_medicalButton;
    }

    public void setBorrower_loanPurposepage_loanRefinanceButton(String borrower_loanPurposepage_loanRefinanceButton) {
        this.borrower_loanPurposepage_loanRefinanceButton = borrower_loanPurposepage_loanRefinanceButton;
    }

    public void setBorrower_loanPurposepage_weddingButton(String borrower_loanPurposepage_weddingButton) {
        this.borrower_loanPurposepage_weddingButton = borrower_loanPurposepage_weddingButton;
    }

    public void setBorrower_loanPurposepage_homeImprovementButton(String borrower_loanPurposepage_homeImprovementButton) {
        this.borrower_loanPurposepage_homeImprovementButton = borrower_loanPurposepage_homeImprovementButton;
    }

    public void setBorrower_loanPurposepage_nextButton(String borrower_loanPurposepage_nextButton) {
        this.borrower_loanPurposepage_nextButton = borrower_loanPurposepage_nextButton;
    }

	public String getBackoffice_searchPage_applicationPANTextBox() {
		return backoffice_searchPage_applicationPANTextBox;
	}

	public void setBackoffice_searchPage_applicationPANTextBox(String backoffice_searchPage_applicationPANTextBox) {
		this.backoffice_searchPage_applicationPANTextBox = backoffice_searchPage_applicationPANTextBox;
	}

	public String getBackoffice_searchPage_applicationEmailTextBox() {
		return backoffice_searchPage_applicationEmailTextBox;
	}

	public void setBackoffice_searchPage_applicationEmailTextBox(String backoffice_searchPage_applicationEmailTextBox) {
		this.backoffice_searchPage_applicationEmailTextBox = backoffice_searchPage_applicationEmailTextBox;
	}

	public String getBackoffice_searchPage_applicationNameTextBox() {
		return backoffice_searchPage_applicationNameTextBox;
	}

	public void setBackoffice_searchPage_applicationNameTextBox(String backoffice_searchPage_applicationNameTextBox) {
		this.backoffice_searchPage_applicationNameTextBox = backoffice_searchPage_applicationNameTextBox;
	}

	public String getBackoffice_searchPage_applicationEmployerTextBox() {
		return backoffice_searchPage_applicationEmployerTextBox;
	}

	public void setBackoffice_searchPage_applicationEmployerTextBox(
			String backoffice_searchPage_applicationEmployerTextBox) {
		this.backoffice_searchPage_applicationEmployerTextBox = backoffice_searchPage_applicationEmployerTextBox;
	}

	public String getBackoffice_searchPage_resultDataFetch_appId() {
		return backoffice_searchPage_resultDataFetch_appId;
	}

	public void setBackoffice_searchPage_resultDataFetch_appId(String backoffice_searchPage_resultDataFetch_appId) {
		this.backoffice_searchPage_resultDataFetch_appId = backoffice_searchPage_resultDataFetch_appId;
	}

	public String getBackoffice_searchPage_resultDataFetch_mobile() {
		return backoffice_searchPage_resultDataFetch_mobile;
	}

	public void setBackoffice_searchPage_resultDataFetch_mobile(String backoffice_searchPage_resultDataFetch_mobile) {
		this.backoffice_searchPage_resultDataFetch_mobile = backoffice_searchPage_resultDataFetch_mobile;
	}

	public String getBackoffice_searchPage_resultDataFetch_pan() {
		return backoffice_searchPage_resultDataFetch_pan;
	}

	public void setBackoffice_searchPage_resultDataFetch_pan(String backoffice_searchPage_resultDataFetch_pan) {
		this.backoffice_searchPage_resultDataFetch_pan = backoffice_searchPage_resultDataFetch_pan;
	}

	public String getBackoffice_searchPage_resultDataFetch_email() {
		return backoffice_searchPage_resultDataFetch_email;
	}

	public void setBackoffice_searchPage_resultDataFetch_email(String backoffice_searchPage_resultDataFetch_email) {
		this.backoffice_searchPage_resultDataFetch_email = backoffice_searchPage_resultDataFetch_email;
	}

	public String getBackoffice_searchPage_resultDataFetch_name() {
		return backoffice_searchPage_resultDataFetch_name;
	}

	public void setBackoffice_searchPage_resultDataFetch_name(String backoffice_searchPage_resultDataFetch_name) {
		this.backoffice_searchPage_resultDataFetch_name = backoffice_searchPage_resultDataFetch_name;
	}

	public String getBackoffice_searchPage_resultDataFetch_employername() {
		return backoffice_searchPage_resultDataFetch_employername;
	}

	public void setBackoffice_searchPage_resultDataFetch_employername(
			String backoffice_searchPage_resultDataFetch_employername) {
		this.backoffice_searchPage_resultDataFetch_employername = backoffice_searchPage_resultDataFetch_employername;
	}

	public String getBackoffice_appDetailsPage_actions_assign_role() {
		return backoffice_appDetailsPage_actions_assign_role;
	}

	public void setBackoffice_appDetailsPage_actions_assign_role(String backoffice_appDetailsPage_actions_assign_role) {
		this.backoffice_appDetailsPage_actions_assign_role = backoffice_appDetailsPage_actions_assign_role;
	}

	public String getBackoffice_appDetailsPage_actions_assign_user() {
		return backoffice_appDetailsPage_actions_assign_user;
	}

	public void setBackoffice_appDetailsPage_actions_assign_user(String backoffice_appDetailsPage_actions_assign_user) {
		this.backoffice_appDetailsPage_actions_assign_user = backoffice_appDetailsPage_actions_assign_user;
	}

	public String getBackoffice_appDetailsPage_actions_assign_notes() {
		return backoffice_appDetailsPage_actions_assign_notes;
	}

	public void setBackoffice_appDetailsPage_actions_assign_notes(String backoffice_appDetailsPage_actions_assign_notes) {
		this.backoffice_appDetailsPage_actions_assign_notes = backoffice_appDetailsPage_actions_assign_notes;
	}

	public String getBackoffice_appDetailsPage_actions_assign_submitButton() {
		return backoffice_appDetailsPage_actions_assign_submitButton;
	}

	public void setBackoffice_appDetailsPage_actions_assign_submitButton(
			String backoffice_appDetailsPage_actions_assign_submitButton) {
		this.backoffice_appDetailsPage_actions_assign_submitButton = backoffice_appDetailsPage_actions_assign_submitButton;
	}

	public String getBackoffice_appDetailsPage_actions_assign_leftAssignMenu() {
		return backoffice_appDetailsPage_actions_assign_leftAssignMenu;
	}

	public void setBackoffice_appDetailsPage_actions_assign_leftAssignMenu(
			String backoffice_appDetailsPage_actions_assign_leftAssignMenu) {
		this.backoffice_appDetailsPage_actions_assign_leftAssignMenu = backoffice_appDetailsPage_actions_assign_leftAssignMenu;
	}

	public String getBackoffice_appDetailsPage_actions_assign_rightAssignMenu() {
		return backoffice_appDetailsPage_actions_assign_rightAssignMenu;
	}

	public void setBackoffice_appDetailsPage_actions_assign_rightAssignMenu(
			String backoffice_appDetailsPage_actions_assign_rightAssignMenu) {
		this.backoffice_appDetailsPage_actions_assign_rightAssignMenu = backoffice_appDetailsPage_actions_assign_rightAssignMenu;
	}

	public String getBackoffice_searchPage_assignedResultData_appId() {
		return backoffice_searchPage_assignedResultData_appId;
	}

	public void setBackoffice_searchPage_assignedResultData_appId(String backoffice_searchPage_assignedResultData_appId) {
		this.backoffice_searchPage_assignedResultData_appId = backoffice_searchPage_assignedResultData_appId;
	}

	public String getBackoffice_appDetailsPage_actions_claim_rightClaimMenu() {
		return backoffice_appDetailsPage_actions_claim_rightClaimMenu;
	}

	public void setBackoffice_appDetailsPage_actions_claim_rightClaimMenu(
			String backoffice_appDetailsPage_actions_claim_rightClaimMenu) {
		this.backoffice_appDetailsPage_actions_claim_rightClaimMenu = backoffice_appDetailsPage_actions_claim_rightClaimMenu;
	}

	public String getBackoffice_appDetailsPage_actions_claim_role() {
		return backoffice_appDetailsPage_actions_claim_role;
	}

	public void setBackoffice_appDetailsPage_actions_claim_role(String backoffice_appDetailsPage_actions_claim_role) {
		this.backoffice_appDetailsPage_actions_claim_role = backoffice_appDetailsPage_actions_claim_role;
	}

	public String getBackoffice_appDetailsPage_actions_claim_submitButton() {
		return backoffice_appDetailsPage_actions_claim_submitButton;
	}

	public void setBackoffice_appDetailsPage_actions_claim_submitButton(
			String backoffice_appDetailsPage_actions_claim_submitButton) {
		this.backoffice_appDetailsPage_actions_claim_submitButton = backoffice_appDetailsPage_actions_claim_submitButton;
	}

	public String getBackoffice_searchPage_assignedResultData_appIdDetailsBtn() {
		return backoffice_searchPage_assignedResultData_appIdDetailsBtn;
	}

	public void setBackoffice_searchPage_assignedResultData_appIdDetailsBtn(
			String backoffice_searchPage_assignedResultData_appIdDetailsBtn) {
		this.backoffice_searchPage_assignedResultData_appIdDetailsBtn = backoffice_searchPage_assignedResultData_appIdDetailsBtn;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_BottomMenu() {
		return backoffice_appDetailsPage_actions_assignments_BottomMenu;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_BottomMenu(
			String backoffice_appDetailsPage_actions_assignments_BottomMenu) {
		this.backoffice_appDetailsPage_actions_assignments_BottomMenu = backoffice_appDetailsPage_actions_assignments_BottomMenu;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_BottomValueOne() {
		return backoffice_appDetailsPage_actions_assignments_BottomValueOne;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_BottomValueOne(
			String backoffice_appDetailsPage_actions_assignments_BottomValueOne) {
		this.backoffice_appDetailsPage_actions_assignments_BottomValueOne = backoffice_appDetailsPage_actions_assignments_BottomValueOne;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_BottomValueTwo() {
		return backoffice_appDetailsPage_actions_assignments_BottomValueTwo;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_BottomValueTwo(
			String backoffice_appDetailsPage_actions_assignments_BottomValueTwo) {
		this.backoffice_appDetailsPage_actions_assignments_BottomValueTwo = backoffice_appDetailsPage_actions_assignments_BottomValueTwo;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_releaseBtn() {
		return backoffice_appDetailsPage_actions_assignments_releaseBtn;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_releaseBtn(
			String backoffice_appDetailsPage_actions_assignments_releaseBtn) {
		this.backoffice_appDetailsPage_actions_assignments_releaseBtn = backoffice_appDetailsPage_actions_assignments_releaseBtn;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_releaseSubBtn() {
		return backoffice_appDetailsPage_actions_assignments_releaseSubBtn;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_releaseSubBtn(
			String backoffice_appDetailsPage_actions_assignments_releaseSubBtn) {
		this.backoffice_appDetailsPage_actions_assignments_releaseSubBtn = backoffice_appDetailsPage_actions_assignments_releaseSubBtn;
	}

	public String getBackoffice_appDetailsPage_actions_assignments_releaseFinalBtn() {
		return backoffice_appDetailsPage_actions_assignments_releaseFinalBtn;
	}

	public void setBackoffice_appDetailsPage_actions_assignments_releaseFinalBtn(
			String backoffice_appDetailsPage_actions_assignments_releaseFinalBtn) {
		this.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn = backoffice_appDetailsPage_actions_assignments_releaseFinalBtn;
	}


	// Update failed submission application
	
	public String getBackoffice_searchPage_failedSubmission() {
		return backoffice_searchPage_failedSubmission;
	}

	public void setBackoffice_searchPage_failedSubmission(String backoffice_searchPage_failedSubmission) {
		this.backoffice_searchPage_failedSubmission = backoffice_searchPage_failedSubmission;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_selectDetailsApp() {
		return backoffice_appDetailsPage_failedSubmission_selectDetailsApp;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_selectDetailsApp(
			String backoffice_appDetailsPage_failedSubmission_selectDetailsApp) {
		this.backoffice_appDetailsPage_failedSubmission_selectDetailsApp = backoffice_appDetailsPage_failedSubmission_selectDetailsApp;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEduDetails() {
		return backoffice_appDetailsPage_failedSubmission_updateEduDetails;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEduDetails(
			String backoffice_appDetailsPage_failedSubmission_updateEduDetails) {
		this.backoffice_appDetailsPage_failedSubmission_updateEduDetails = backoffice_appDetailsPage_failedSubmission_updateEduDetails;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_eduQualification() {
		return backoffice_appDetailsPage_failedSubmission_eduQualification;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_eduQualification(
			String backoffice_appDetailsPage_failedSubmission_eduQualification) {
		this.backoffice_appDetailsPage_failedSubmission_eduQualification = backoffice_appDetailsPage_failedSubmission_eduQualification;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_eduInstitution() {
		return backoffice_appDetailsPage_failedSubmission_eduInstitution;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_eduInstitution(
			String backoffice_appDetailsPage_failedSubmission_eduInstitution) {
		this.backoffice_appDetailsPage_failedSubmission_eduInstitution = backoffice_appDetailsPage_failedSubmission_eduInstitution;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpDetails() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpDetails;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpDetails(
			String backoffice_appDetailsPage_failedSubmission_updateEmpDetails) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpDetails = backoffice_appDetailsPage_failedSubmission_updateEmpDetails;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpCompanyName() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpCompanyName(
			String backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName = backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpDesignation() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpDesignation;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpDesignation(
			String backoffice_appDetailsPage_failedSubmission_updateEmpDesignation) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpDesignation = backoffice_appDetailsPage_failedSubmission_updateEmpDesignation;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpTotalExp() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpTotalExp(
			String backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp = backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpEmail() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpEmail;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpEmail(
			String backoffice_appDetailsPage_failedSubmission_updateEmpEmail) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpEmail = backoffice_appDetailsPage_failedSubmission_updateEmpEmail;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpIncome() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpIncome;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpIncome(
			String backoffice_appDetailsPage_failedSubmission_updateEmpIncome) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpIncome = backoffice_appDetailsPage_failedSubmission_updateEmpIncome;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpAddress1() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpAddress1;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpAddress1(
			String backoffice_appDetailsPage_failedSubmission_updateEmpAddress1) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpAddress1 = backoffice_appDetailsPage_failedSubmission_updateEmpAddress1;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpAddress2() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpAddress2;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpAddress2(
			String backoffice_appDetailsPage_failedSubmission_updateEmpAddress2) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpAddress2 = backoffice_appDetailsPage_failedSubmission_updateEmpAddress2;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpLocation() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpLocation;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpLocation(
			String backoffice_appDetailsPage_failedSubmission_updateEmpLocation) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpLocation = backoffice_appDetailsPage_failedSubmission_updateEmpLocation;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpPincode() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpPincode;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpPincode(
			String backoffice_appDetailsPage_failedSubmission_updateEmpPincode) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpPincode = backoffice_appDetailsPage_failedSubmission_updateEmpPincode;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpCity() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpCity;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpCity(
			String backoffice_appDetailsPage_failedSubmission_updateEmpCity) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpCity = backoffice_appDetailsPage_failedSubmission_updateEmpCity;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpState() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpState;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpState(
			String backoffice_appDetailsPage_failedSubmission_updateEmpState) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpState = backoffice_appDetailsPage_failedSubmission_updateEmpState;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn() {
		return backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn(
			String backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn) {
		this.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn = backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updatePersonalDetails() {
		return backoffice_appDetailsPage_failedSubmission_updatePersonalDetails;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updatePersonalDetails(
			String backoffice_appDetailsPage_failedSubmission_updatePersonalDetails) {
		this.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails = backoffice_appDetailsPage_failedSubmission_updatePersonalDetails;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updatePersonalDOB() {
		return backoffice_appDetailsPage_failedSubmission_updatePersonalDOB;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updatePersonalDOB(
			String backoffice_appDetailsPage_failedSubmission_updatePersonalDOB) {
		this.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB = backoffice_appDetailsPage_failedSubmission_updatePersonalDOB;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updatePersonalPAN() {
		return backoffice_appDetailsPage_failedSubmission_updatePersonalPAN;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updatePersonalPAN(
			String backoffice_appDetailsPage_failedSubmission_updatePersonalPAN) {
		this.backoffice_appDetailsPage_failedSubmission_updatePersonalPAN = backoffice_appDetailsPage_failedSubmission_updatePersonalPAN;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber() {
		return backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber(
			String backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber) {
		this.backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber = backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn() {
		return backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn(
			String backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn) {
		this.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn = backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressDetails() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressDetails;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressDetails(
			String backoffice_appDetailsPage_failedSubmission_updateAddressDetails) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressDetails = backoffice_appDetailsPage_failedSubmission_updateAddressDetails;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressAdd1() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressAdd1;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressAdd1(
			String backoffice_appDetailsPage_failedSubmission_updateAddressAdd1) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressAdd1 = backoffice_appDetailsPage_failedSubmission_updateAddressAdd1;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressAdd2() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressAdd2;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressAdd2(
			String backoffice_appDetailsPage_failedSubmission_updateAddressAdd2) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressAdd2 = backoffice_appDetailsPage_failedSubmission_updateAddressAdd2;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressLoc() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressLoc;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressLoc(
			String backoffice_appDetailsPage_failedSubmission_updateAddressLoc) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressLoc = backoffice_appDetailsPage_failedSubmission_updateAddressLoc;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressPincode() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressPincode;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressPincode(
			String backoffice_appDetailsPage_failedSubmission_updateAddressPincode) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressPincode = backoffice_appDetailsPage_failedSubmission_updateAddressPincode;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressChkBox() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressChkBox;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressChkBox(
			String backoffice_appDetailsPage_failedSubmission_updateAddressChkBox) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox = backoffice_appDetailsPage_failedSubmission_updateAddressChkBox;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn() {
		return backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn(
			String backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn) {
		this.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn = backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeDetails() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeDetails;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeDetails(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeDetails) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails = backoffice_appDetailsPage_failedSubmission_updateIncomeDetails;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeResidency() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeResidency;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeResidency(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeResidency) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeResidency = backoffice_appDetailsPage_failedSubmission_updateIncomeResidency;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent = backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI = backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance = backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn() {
		return backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn(
			String backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn) {
		this.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn = backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn;
	}

	public String getBackoffice_appDetailsPage_failedSubmission_eduSubmitBtn() {
		return backoffice_appDetailsPage_failedSubmission_eduSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_failedSubmission_eduSubmitBtn(
			String backoffice_appDetailsPage_failedSubmission_eduSubmitBtn) {
		this.backoffice_appDetailsPage_failedSubmission_eduSubmitBtn = backoffice_appDetailsPage_failedSubmission_eduSubmitBtn;
	}

	public String getBackoffice_searchPage_pendinginitialoffer() {
		return backoffice_searchPage_pendinginitialoffer;
	}

	public void setBackoffice_searchPage_pendinginitialoffer(String backoffice_searchPage_pendinginitialoffer) {
		this.backoffice_searchPage_pendinginitialoffer = backoffice_searchPage_pendinginitialoffer;
	}

	public String getBackoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp() {
		return backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp;
	}

	public void setBackoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp(
			String backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp) {
		this.backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp = backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp;
	}

	public String getBackoffice_appDetailsPage_pendingInitialoffer_selectDropOffer() {
		return backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer;
	}

	public void setBackoffice_appDetailsPage_pendingInitialoffer_selectDropOffer(
			String backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer) {
		this.backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer = backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer;
	}

	public String getBackoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer() {
		return backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer;
	}

	public void setBackoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer(
			String backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer) {
		this.backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer = backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer;
	}

	public String getBackoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn() {
		return backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn;
	}

	public void setBackoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn(
			String backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn) {
		this.backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn = backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn;
	}

	public String getBackoffice_searchPage_pendingbankstmt() {
		return backoffice_searchPage_pendingbankstmt;
	}

	public void setBackoffice_searchPage_pendingbankstmt(String backoffice_searchPage_pendingbankstmt) {
		this.backoffice_searchPage_pendingbankstmt = backoffice_searchPage_pendingbankstmt;
	}

	public String getBackoffice_appDetailsPage_pendingBankstmt_selectDetailsApp() {
		return backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp;
	}

	public void setBackoffice_appDetailsPage_pendingBankstmt_selectDetailsApp(
			String backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp) {
		this.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp = backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp;
	}
	
    
}
