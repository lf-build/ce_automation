package net.sigmainfo.lf.automation.portal.pages.BankPortal;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 07-03-2018.
 */
public class BankPortalDashboardPage extends AbstractTests {

    private Logger logger = LoggerFactory.getLogger(BankPortalDashboardPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    public BankPortalDashboardPage(WebDriver driver) throws Exception {
        try {
            this.driver = driver;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_profileCircle));
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_pendingBankFraudReviewTab));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_pendingBankFraudReviewTab));
            logger.info("=========== BankPortalDashboardPage is loaded============");
        }catch (Exception e)
        {
            throw new Exception("BankPortalDashboardPage could not be loaded within time");
        }
    }
    public BankPortalDashboardPage(){}

    public BankPortalLoginPage approveFunding(String applicationId) throws Exception {
        approveApp(applicationId);
        if(!portalParam.numberOfAppsWithStatus.contains("Bank Credit Review")) {
            approveCreditApp(applicationId);
            if (!portalParam.numberOfAppsWithStatus.contains("Approved")) {
                updateDisbursementDetails(applicationId);
            }
        }
        logout();
        return new BankPortalLoginPage(driver);
    }

    private void logout() throws Exception {
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_profileCircle));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_profileCircle),"Settings");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_profile_logoutButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_profile_logoutButton),"Logout");
    }

    private void updateDisbursementDetails(String applicationId) throws Exception {
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_pendingFunding));
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
        selectAndNavigateApp(applicationId);
        PortalFuncUtils.scrollOnTopOfThePage(driver,"BankPortal");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails),"Update Disbursement Details");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox));
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_loanAccountNumberTextbox), PortalParam.loanaccountNumber);
        PortalFuncUtils.selectDropdownByNonDivValue(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_paymentStatusDropdown),PortalParam.paymentStatus);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_utrNumberTextbox), PortalParam.utrNumber);
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedAmountTextbox), PortalParam.loanAmount);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        PortalFuncUtils.selectDateFromVaadinCalendar(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_fundedDateCalendar),new SimpleDateFormat("dd-MMMM-yyyy").format(new Date(cal.getTimeInMillis())).replaceAll("^0*", ""),"Funding Date");
        PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_emiAmountTextbox), PortalParam.emiAmount);
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DATE, 26);
        PortalFuncUtils.selectDateFromVaadinCalendar(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_firstEmiDateCalendar),new SimpleDateFormat("dd-MMMM-yyyy").format(new Date(cal1.getTimeInMillis())).replaceAll("^0*", ""),"First EMI");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_updateDisbursementDetails_submitButton),"Submit");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_headerStatus),"Funded");
        logger.info("Status changd to ================>:  Funded");
        PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Application status changed from Approved to Funded')]"));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");
    }

    private void approveCreditApp(String applicationId) throws Exception {
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_pendingBankCreditReviewTab));
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
        selectAndNavigateApp(applicationId);
        PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_headerStatus),"Bank Credit Review");
        logger.info("Status changd to ================>:  Bank Credit Review");
        PortalFuncUtils.scrollOnTopOfThePage(driver,"BankPortal");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication),"Approve Credit Application");
        Thread.sleep(4000);
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveCreditApplication_approveAppButton),"Approve Application");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_headerStatus),"Approved");
        logger.info("Status changd to ================>:  Approved");
        PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Application status changed from Bank Credit Review to Approved')]"));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");
    }

    private void approveApp(String applicationId) throws Exception {
        PortalFuncUtils.scrollToElementandClick(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_pendingBankFraudReviewTab));
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
        selectAndNavigateApp(applicationId);
        PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_headerStatus),"Bank Fraud Review");
        logger.info("Status changd to ================>:  Bank Fraud Review");
        PortalFuncUtils.scrollOnTopOfThePage(driver,"BankPortal");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");
        PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveApplication));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveApplication));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveApplication),"Approve Application");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveApplication_approveAppButton));
        Thread.sleep(3000);
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_action_approveApplication_approveAppButton),"Approve Application");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
        PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_headerStatus),"Bank Credit Review");
        logger.info("Status changd to ================>:  Bank Credit Review");
        PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Application status changed from Bank Fraud Review to Bank Credit Review')]"));
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton));
        PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.bankPortal_appDetailsPage_actionButton),"Actions");

    }

    private void selectAndNavigateApp(String applicationId) throws Exception {
        List<WebElement> appsInFraudReview = driver.findElements(By.xpath("//*[@id='queue_wrapper']//*[@id='queue']//tbody/tr"));
        for (int i = 1; i <= appsInFraudReview.size(); i++) {
            if (driver.findElement(By.xpath("//*[@id='queue_wrapper']//*[@id='queue']//tbody/tr["+i+"]/td[1]")).getText().contains(applicationId)) {
                new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='queue']/tbody/tr["+i+"]//button[contains(text(),'Details')]")));
                driver.findElement(By.xpath("//*[@id='queue']/tbody/tr["+i+"]//button[contains(text(),'Details')]")).click();
                break;
            }
        }
        PortalFuncUtils.waitForPageToLoad(driver);
    }
}
