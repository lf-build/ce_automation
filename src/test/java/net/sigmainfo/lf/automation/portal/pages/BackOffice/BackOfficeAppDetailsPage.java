package net.sigmainfo.lf.automation.portal.pages.BackOffice;

import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;

import junit.framework.Assert;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.portal.constant.PortalParam;
import net.sigmainfo.lf.automation.portal.constant.UIObjParam;
import net.sigmainfo.lf.automation.portal.function.PortalFuncUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by shaishav.s on 23-02-2018.
 */
public class BackOfficeAppDetailsPage extends AbstractTests {
    private Logger logger = LoggerFactory.getLogger(BackOfficeAppDetailsPage.class);
    WebDriverWait wait = new WebDriverWait(driver,60);

    @Autowired
    BackOfficeSearchPage backOfficeSearchPage;

    public BackOfficeAppDetailsPage(WebDriver driver) throws Exception {
        try {
            this.driver = driver;
            PortalFuncUtils.waitForPageToLoad(driver);
            //PortalFuncUtils.scrollOnTopOfThePage(driver,"Backoffice");
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            logger.info("=========== BackOfficeAppDetailsPage is loaded============");
        }catch (Exception e)
        {
            throw new Exception("BackOfficeAppDetailsPage could not be loaded within time");
        }
    }
    public BackOfficeAppDetailsPage(){}

    public void openActionMenu() throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton), "Action");
            PortalFuncUtils.waitForElementToBeClickable(By.xpath("//loan-activity[1]//div[contains(@class,'ibox-title')]"));
        }catch (Exception e)
        {
            throw new Exception("Action menu can not be opened properly.");
        }
    }

    public void closeActionMenu() throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton), "Action");
            PortalFuncUtils.waitForElementToBeClickable(By.xpath("//*[@id='activity-search']"));
            PortalFuncUtils.waitForPageToLoad(driver);
        }catch (Exception e)
        {
            throw new Exception("Action menu can not be closed properly.");
        }
    }

    public BackOfficeLoginPage moveApplication(String appId) throws Exception {
        try {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")),"Credit Review status message not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Credit Review"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            addManualOffer();
            presentFinalOffer();
            logout();
            PortalFuncUtils.waitForPageToLoad(driver);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void logout() throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profileCircle), "Profile Setting");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_profile_logoutButton), "Log out");
            Thread.sleep(5000);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }

    private void presentFinalOffer() throws Exception {
        try {
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_presentFinalOfferButton), "Present Final Offer");
            verifyManuallyAddedOffer();
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_presentFinalOffer_presentFinalOfferButton), "Present Final Offer");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Final Offer Made");
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void verifyManuallyAddedOffer() throws Exception {
        try {
            new WebDriverWait(driver, 60).until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//*[@id='presentFinalOffer']//tbody/tr[2]/td[3]//p")), "Months"));
            List<WebElement> options = driver.findElements(By.xpath("//*[@id='presentFinalOffer']//tbody/tr"));
            for (int i = options.size(); i > 0; i--) {
                if (driver.findElement(By.xpath("//*[@id='presentFinalOffer']//tbody/tr[" + i + "]/td[3]//p[contains(.,'Manual')]")).isDisplayed()) {
                    new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='presentFinalOffer']//tbody/tr[" + i + "]/td[1]//paper-checkbox[@id='offer.offerId']")));
                    driver.findElement(By.xpath("//*[@id='presentFinalOffer']//tbody/tr[" + i + "]/td[1]//paper-checkbox[@id='offer.offerId']")).click();
                    break;
                }
            }
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void addManualOffer() throws Exception {
        try {
            openActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOfferButton)), "Add Manual Offer button is not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOfferButton), "Add Manual Offer");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown)), "Term dropdown is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown)), "Term dropdown is not clickable.");
            PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown), PortalParam.term);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox), PortalParam.loanAmount);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox), PortalParam.processingFee);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox), PortalParam.emi);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox), PortalParam.interestRate);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox), PortalParam.processingFeePercentage);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            String x = driver.findElement(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")).getText();
            System.out.println(x);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }

    public BackOfficeLoginPage verifyPostAcceptanceStages(String applicationId) throws Exception {
        try {
            navigateVerificationDashboard(applicationId);
            verifyEmail(PortalParam.emailID, applicationId);
            verifyEmail(PortalParam.officialEmailId, applicationId);
            verify("Pan Card");
            verify("Kyc Address");
            verify("Signed Document");
            verify("Bank");
            updateAncilliaryData();
            updatePerfiosSalaryDetail();
            addFOIRCalculation();
            addDeviation();
            //addDocumentCollectedFromSource();
            completeFulfillment();
            if(!portalParam.numberOfAppsWithStatus.contains("CE Approved")) {
                sendToBank();
            }
            logout();
            PortalFuncUtils.waitForPageToLoad(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
        return new BackOfficeLoginPage(driver);
    }

    private void sendToBank() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank), "Send to Bank");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_sendToBank_bankFraudReviewButton), "Bank Fraud Review");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("/*//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from CE Approved to Bank Fraud Review')]")),"Bank Fraud Review status message not seen in activity history.");
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Bank Fraud Review");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void completeFulfillment() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment), "Complete Fulfillment");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_completeFulfillment_approveApplicationButton), "Approve Application");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("/*//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from Final offer Accepted to CE Approved')]")),"CE Approved status message not seen in activity history.");
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "CE Approved");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void addDocumentCollectedFromSource() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_documentCollectionSource));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_documentCollectionSource));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_documentCollectionSource), "Document Collected Source");
            PortalFuncUtils.selectRadioButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_documentCollectionSource_sourceRadio), PortalParam.docCollectionSource);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_documentCollectionSource_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void addDeviation() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addDeviation));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addDeviation));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addDeviation), "Add Deviation");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addDeviation_deviationTextbox), PortalParam.deviation);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addDeviation_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void addFOIRCalculation() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation), "Add FOIR/DTI Calculation");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_typeOfLoanTextBox), PortalParam.typeOfLoan);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_obligatedAmountTextBox), PortalParam.obligatedAmount);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_commentTextBox), PortalParam.comments);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_addFOIRCalculation_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void updatePerfiosSalaryDetail() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail), "Update Perfios Salary Detail");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_amountTextBox), PortalParam.salary);
            PortalFuncUtils.selectDateFromVaadinCalendar(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_salaryDateDropdown), PortalParam.salaryDate, "Perfios Salary");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_narrationTextBox), PortalParam.narration);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updatePerfiosSalaryDetail_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public void updateAncilliaryData() throws Exception {
        try {
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData), "Update Ancillary Data");
            PortalFuncUtils.selectDropdownByNonDivValue(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_selectCommunityDropdown), PortalParam.community);
            PortalFuncUtils.selectRadioButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_casteRadioGroup), PortalParam.caste);
            PortalFuncUtils.selectDateFromVaadinCalendar(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentExpiryCalendar), PortalParam.kycExpiryDate, "Document");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentNumberTextBox), PortalParam.kycDocNumber);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_motherNameTextBox), PortalParam.motherName);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_residenceYearsTextBox), PortalParam.currentResidenceYears);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_cityYearsTextBox), PortalParam.currentCityYears);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_workExperienceTextBox), PortalParam.workExperience);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_kycDocumentTypeTextBox), PortalParam.kycDocType);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_dependantsTextBox), PortalParam.dependants);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_guardianNameTextBox), PortalParam.gurdianName);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_action_updateAncilliaryData_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void verify(String check) throws Exception {
        try {
            if (check.contains("Bank")) {
                PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
                openActionMenu();
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails));
                PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails), "Update-Bank-Detail");
                PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox));
                PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox));
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_accountNumberTextBox), PortalParam.accountNumber);
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_confirmAccountNumberTextBox), PortalParam.accountNumber);
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_ifscCodeTextBox), PortalParam.ifscCode);
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_updateBankDetails_submitButton), "Submit");
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
                wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
                closeActionMenu();
            }
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument), "Upload Pendindg Documents");
            uploadDocs(check);
            logger.info("Verifying " + check + " status.");
            logger.info("=============================");
            List<WebElement> rows = driver.findElements(By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr/td[1]"));
            if (check.contains("Pan")) {
                for (int i = 1; i < rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("KYC-PAN")) {
                        assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText().equalsIgnoreCase("InProgress"), "Status is not InProgress");
                        PortalFuncUtils.clickButton(driver, By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button"), check + "Details");
                        break;
                    }
                }
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycStartedMsg)),"KYC Started message not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_kycLabel)),"KYC label is not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_screenshot)),"Screenshot of the PAN is not visible");
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantNameCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPhotoCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantDoBCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_applicantPanCheckBox));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_panDetails_verifyButton), "Verify");
                for (int i = 1; i < rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("KYC-PAN")) {
                        PortalFuncUtils.waitForTextToBe(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]"), "Completed");
                        break;
                    }
                }
                PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact KYC-PAN done by KYCPanForm with type Manual and result was Passed')]"));
                assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact KYC-PAN done by KYCPanForm with type Manual and result was Passed')]")).isDisplayed(), "Activity history for " + check + " is not seen.");
            }
            if (check.contains("Address")) {
                for (int i = 1; i < rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("KYC-Address")) {
                        assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText().equalsIgnoreCase("InProgress"), "Status is not InProgress");
                        PortalFuncUtils.clickButton(driver, By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button"), check + "Details");
                        break;
                    }
                }
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycStartedMsg)),"KYC Started message not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_kycLabel)),"KYC Label not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_screenshot)),"Screenshot is not seen for address");
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantNameCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_applicantPhotoCheckBox));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_addressDetails_verifyButton), "Verify");
                for (int i = 1; i < rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("KYC-Address")) {
                        PortalFuncUtils.waitForTextToBe(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]"), "Completed");
                        break;
                    }
                }
                PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact KYC-Address done by KYCAddressForm with type Manual and result was Passed')]"));
                assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact KYC-Address done by KYCAddressForm with type Manual and result was Passed')]")).isDisplayed(), "Activity history for " + check + " is not seen.");
            }
            if (check.contains("Signed")) {
                for (int i = 1; i <= rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Signed Document")) {
                        assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText().equalsIgnoreCase("InProgress"), "Status is not InProgress");
                        PortalFuncUtils.clickButton(driver, By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button"), check + "Details");
                        break;
                    }
                }
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycStartedMsg)),"KYC Started message not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycLabel)),"KYC Label is not seen");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_screenshot)),"Screenshot for signed document is not seen");
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_appSignedCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_kycVerifiedCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_loanAgreementSignedCheckBox));
                PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_ecsSignedCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_signatureMatchCheckBox));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_signedDocDetails_verifyButton), "Verify");
                for (int i = 1; i <= rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Signed Document")) {
                        PortalFuncUtils.waitForTextToBe(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]"), "Completed");
                        break;
                    }
                }
                PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact SignedDocumentVerification done by SignedDocuments with type Manual and result was Passed')]"));
                assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact SignedDocumentVerification done by SignedDocuments with type Manual and result was Passed')]")).isDisplayed(), "Activity history for " + check + " is not seen.");
            }
            if (check.contains("Bank")) {
                for (int i = 1; i <= rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Bank")) {
                        assertTrue(driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]")).getText().equalsIgnoreCase("InProgress"), "Status is not InProgress");
                        PortalFuncUtils.clickButton(driver, By.xpath("//*[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[7]/button"), check + "Details");
                        break;
                    }
                }
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationLabel)),"Bank verification label is not seen");
                /*assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationNotInitiatedMsg)));
                PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_initiateNowButton),"Initiate Now");
                assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankVerificationStartedMsg)));*/
                PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_accountNumberTextBox), PortalParam.accountNumber);
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_nameMatchCheckBox));
                PortalFuncUtils.selectCheckbox(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_bankNameMatchCheckBox));
                PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboard_bankVerificationDetails_verifyButton), "Verify");
                for (int i = 1; i <= rows.size(); i++) {
                    if (driver.findElement(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[2]")).getText().contains("Bank Verification")) {
                        PortalFuncUtils.waitForTextToBe(By.xpath("//verification-details[@id='veriDetails']/table[1]/tbody/tr[" + i + "]/td[3]"), "Completed");
                        break;
                    }
                }
                PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact SignedDocumentVerification done by SignedDocuments with type Manual and result was Passed')]"));
                assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[starts-with(@class,'activity-stream')]//div[starts-with(@class,'stream-panel') and contains(.,'Fact SignedDocumentVerification done by SignedDocuments with type Manual and result was Passed')]")).isDisplayed(), "Activity history for " + check + " is not seen.");
            }
            PortalFuncUtils.scrollToElement(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_bottomTabs_documents));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_bottomTabs_documents));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_bottomTabs_documents), "Documents");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_bottomTabs_documents_documentsLabel));
            List<WebElement> docs = driver.findElements(By.xpath("//*[@id='listOfDocs']//div/small[contains(.,'Category')]"));
            for (int i = 1; i <= docs.size(); i++) {
                if (driver.findElement(By.xpath("//*[@id='listOfDocs']//div/small[contains(.,'Category')]")).getText().toLowerCase().contains(check.toLowerCase())) {
                    assertTrue(driver.findElement(By.xpath("//*[@id='listOfDocs']//div/small[contains(.,'Category')]")).getText().toLowerCase().contains(check.toLowerCase()), check + " is not found under documents");
                    logger.info(check + " is available under documents.");
                    break;
                }
            }
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
     }

    private void verifyEmail(String mail,String applicationId) throws Exception {
        try {
            String bearerToken = ApiParam.token;
            String endPoint = ApiParam.verificationService + getEmailVerificationCode(mail, applicationId);
            logger.info("=======================================================================================");
            logger.info("Triggering a post request to :" + endPoint);
            logger.info("=======================================================================================");

            Header header = new Header("Authorization", "Bearer " + bearerToken);
            Response response = given()
                    .header(header)
                    .contentType("application/json")
                    .log().all()
                    .when()
                    .post(endPoint);


            logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
            assertTrue(response.getStatusCode() == 200, "Email verification failed.");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void uploadDocs(String check) throws Exception {
        try {
            logger.info("Uploading document for " + check);
            int sections = driver.findElements(By.xpath("//div[@id='uploadDocument']/div")).size();
            for (int i = sections; i > 0; i--) {
                if ((driver.findElement(By.xpath("//div[@id='uploadDocument']/div[" + i + "]")).getText().contains(check))) {
                    driver.findElement(By.xpath("//div[@id='uploadDocument']/div[" + i + "]/vaadin-upload/div[@id='buttons']/div/paper-button")).click();
                    Thread.sleep(2000);
                    fileupload(check);
                    driver.findElement(By.xpath(".//*[@id='uploadDocument']/div[" + i + "]//*[contains(text(),'UPLOAD')]")).click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Document Uploaded Successfully')]")));
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Document Uploaded Successfully')]")));
                    logger.info(check + " :Document uploaded successfully");
                    //Put some Sleep for file uploading
                    Thread.sleep(2000);
                }
            }
            PortalFuncUtils.scrollToElementandClick(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_uploadPendingDocument_closeButton));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void fileupload(String doc) throws Exception {
        try {
            String userDirectory = System.getProperty("user.dir");
            userDirectory = userDirectory.replaceAll("/", "\\\\/");

            String filepath = null;
            Thread.sleep(3000);
            if ((doc.contains("Pan")) || (doc.contains("Address")) || (doc.contains("Signed")) || (doc.contains("Bank"))) {
                try {
                    String command = PortalParam.upload_file_location + "upload.exe";
                    File file = new File(PortalParam.upload_file_location + "PayStub.pdf");
                    Runtime.getRuntime().exec(command + " " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                Thread.sleep(3000);
                try {
                    String command = PortalParam.upload_file_location + "upload.exe";
                    File file = new File(PortalParam.upload_file_location + "image.jpg");
                    Runtime.getRuntime().exec(command + " " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Thread.sleep(3000);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void navigateVerificationDashboard(String applicationId) throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
        /*assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_personalInfoTab_personalInfoLabel)));*/
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("/*//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from Final offer made to Final offer Accepted')]")),"Final offer Accepted status message not seen in activity history.");
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Final Offer Accepted");
        /*PortalFuncUtils.clickButton(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboardTab),"Verification Dashboard");*/
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_actionCenter));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_actionCenter), "Action center");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNumberTextBox), applicationId);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_searchButton), "Search");
            Thread.sleep(5000);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton), "Details");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("/*//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from Final offer made to Final offer Accepted')]")));
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Final Offer Accepted");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboardTab)));
            Thread.sleep(5000);
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboardTab)),"Verification dashboard is not clickable");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_verificationDashboardTab), "Verification Dashboard");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public BackOfficeLoginPage verifyErrorMessage(String sTestID) throws Exception {
        PortalFuncUtils.waitForPageToLoad(driver);
        if(sTestID.contains("RejectAppCibilReport")) {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Application Submitted to Rejected')]")),"Rejected message not seen in activity history");
            assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Application Submitted to Rejected')]")).getText().contains("\"Delinquencies-CE 30\", \"Delinquencies-RBL 60\", \"Delinquencies-RBL 90\", \"Delinquencies-RBL 180\""),"RBL details not seen in activity history");
            logger.info("RBL details seen in activity history log.");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus)).getText(), "Rejected", "Status is not \"Rejected\"");
        }
        else if(sTestID.contains("RejectAppBureauScore")) {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")));
            assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")).getText().contains("Thick File Low App and Bureau Score"));
            logger.info("\"Thick File Low App and Bureau Score\" is seen in activity history log.");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus)).getText(), "Credit Review", "Status is not \"Credit Review\"");
        }
        else if(sTestID.contains("unclassifiedCompany_manualreview")) {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")));
            assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")).getText().contains("Company Unclassified"));
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus)).getText(), "Credit Review", "Status is not \"Credit Review\"");
        }
        else if(sTestID.contains("VerifyPerfiosfailedFlow")) {
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")),"Status change to Credit Review is not seen in activity history.");
            assertTrue(driver.findElement(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")).getText().contains("Perfios Name not Matched with application"),"Perfios not match message not seen in activity history");
            assertEquals(driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus)).getText(), "Credit Review", "Status is not \"Credit Review\"");
        }
        PortalFuncUtils.scrollOnTopOfThePage(driver,"Backoffice");
        logout();
        return new BackOfficeLoginPage(driver);
    }

    public BackOfficeLoginPage rejectApp(String applicationId) throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application status changed from Initial Offer Selected to Credit Review')]")),"Credit Review status message not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Credit Review"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            triggerReject(applicationId);
            logout();
            PortalFuncUtils.waitForPageToLoad(driver);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void triggerReject(String applicationId) throws Exception {
        try
        {
            openActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton)), "Not Interested button is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton)), "Not Interested button is not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton), "Not Interested");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown)), "Not Interested reason is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown)), "Not Interested reason is not clickable.");
            PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton_reasonDropdown), portalParam.notInterestedReason);
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton)), "Not Interested button is not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_notInterestedButton_notInterestedButton), "Not Interested");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            PortalFuncUtils.scrollOnTopOfThePage(driver,"Backoffice");
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Not Interested");
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    public BackOfficeLoginPage manualCashFlowGenerateOffer(String appId) throws Exception {
        try {
            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            updateCashFlow();
            getFinalOffer();
            getFindOffer2addManualOffer();
            presentFinalOffer();
            selectFinalOffer();
            logout();
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    
    public BackOfficeLoginPage AssignApplicationtoSingleUser(String appId) throws Exception {
        try {
        	Boolean single = true;
            PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            AssignApplication(appId, single);
            logout();
            loginBackOffice();
            verifyAssignApplication(appId);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    
    public BackOfficeLoginPage AssignApplicationtoMultiUser(String appId) throws Exception {
        try {
        	Boolean single = false;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeClickable((By.xpath("//*[@id='bottomTabs']/li[3]/a")));
            
           assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            AssignApplication(appId, single);
            Thread.sleep(5000);
            verifyMultiAssignApplication();
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    public BackOfficeLoginPage UnAssignApplicationMultiUser(String appId) throws Exception {
        try {
        	Boolean single = false;
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForElementToBeClickable((By.xpath("//*[@id='bottomTabs']/li[3]/a")));
            
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            AssignApplication(appId, single);
            Thread.sleep(5000);
            verifyUnassignApplicationStatus(appId);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    
    public BackOfficeLoginPage ClaimApplicationToSingleUser(String appId) throws Exception {
        try {
        	Boolean single = true;
        	PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            ClaimApplication(appId, single);
            Thread.sleep(5000);
            verifySingleClaimApplication(appId);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    public BackOfficeLoginPage ClaimApplicationToMultiUser(String appId) throws Exception {
        try {
        	Boolean single = false;
        	PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            ClaimApplication(appId, single);
            Thread.sleep(5000);
            verifyMultiClaimApplication();
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    
    public BackOfficeLoginPage verifyReleaseClaimApplication(String appId) throws Exception {
        try {        	
        	PortalFuncUtils.waitForPageToLoad(driver);
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Initial Offer Selected')]")),"Initial offer selected status is not seen in activity history.");
            assertTrue(PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Initial Offer Selected"), "Application status does not match");
            PortalFuncUtils.scrollOnTopOfThePage(driver, "Backoffice");
            PortalFuncUtils.waitForPresencfElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actionButton));
            ClaimApplication(appId,true);
            ReleaseClaimApplication(appId);
            return new BackOfficeLoginPage(driver);
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    private void ClaimApplication(String appId, Boolean single) throws Exception {
		try{
    	 if(single==true)
         {
            openActionMenu();
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu), "Claimed right side menu");
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role));
            Thread.sleep(3000);
            //PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role), "CEO");
            PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role), "CEO");
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton), "Submit claim application");
         }
         else
         {        	
        	  openActionMenu();
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu), "Claimed right side menu");
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role));
              Thread.sleep(3000);
              //PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role), "CEO");
              PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role), "CEO");
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton));
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton), "Submit claim application");
              Thread.sleep(5000);
              logout();
              Thread.sleep(3000);  	         
              PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox));
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox));
              PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), "thangaraj");
              PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox),"thangaraj");
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
              Thread.sleep(2000);
              PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_applicationNumberTextBox), appId);
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_searchButton), "Search");
              Thread.sleep(5000);
              PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.bankPortal_dashboard_resultAppsMsg));
              PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton));
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_resultData_detailsButton), "Details");
              Thread.sleep(5000);
              openActionMenu();
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_rightClaimMenu), "Claimed right side menu");
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role));
              Thread.sleep(3000);
              PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_role), "Verification Manager");
              PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton));
              PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_claim_submitButton), "Submit claim application");
          
         }
           //Assert.assertEquals("Application "+appId+" claimed successfully", driver.findElement(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Application "+appId+" claimed successfully')]")).getText());
		 }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
	}
	private void loginBackOffice() throws Exception{
    	
    	 PortalFuncUtils.waitForPageToLoad(driver);
         PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox));
         PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox));
         PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_usernameTextBox), "thangaraj");
         PortalFuncUtils.insertText(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_passwordTextBox),"thangaraj");
         PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_loginPage_loginButton), "SignIn");
         
    }
    
    private void verifyAssignApplication(String appId) throws Exception{    	
    	 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Assign Menu");
    	 Assert.assertEquals(appId, driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appId)).getText());
         
    }
    
    public void verifyMultiAssignApplication() throws Exception{
    	/*//PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu));
        //PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments buttom Menu");
    	Thread.sleep(8000);
    	driver.findElement(By.xpath("//*[@id='bottomTabs']/li[3]/a")).click();
    	Thread.sleep(8000);
    	driver.navigate().refresh();
    	PortalFuncUtils.waitForPageToLoad(driver);
    	//Assert.assertEquals("Thangaraj", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
    	//Assert.assertEquals("Shivani 1", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo)).getText());
    	PortalFuncUtils.waitForElementToBeClickable((By.xpath("//*[@id='assignmentView']/div/div/div/table/tbody/tr[1]/td[1]/label")));
    	Assert.assertEquals("Thangaraj", driver.findElement(By.xpath("//*[@id='assignmentView']/div/div/div/table/tbody/tr[1]/td[1]/label")).getText());
    	PortalFuncUtils.waitForElementToBeClickable((By.xpath("//*[@id='assignmentView']/div/div/div/table/tbody/tr[2]/td[1]/label")));
    	Assert.assertEquals("Shivani 1", driver.findElement(By.xpath("//*[@id='assignmentView']/div/div/div/table/tbody/tr[2]/td[1]/label")).getText());
   */   
    	
    	PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Clicked Assigned Menu");
    	Thread.sleep(3000);
    	driver.navigate().back();
    	 PortalFuncUtils.waitForPageToLoad(driver);
    	 Thread.sleep(5000);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments bottom Menu");
       	Thread.sleep(3000);
     	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne));
     	Assert.assertEquals("Thangaraj", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
    	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo));
     	Assert.assertEquals("Shivani 1", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo)).getText());

    }
    
    public void verifyMultiClaimApplication() throws Exception{
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments bottom Menu");
       	Thread.sleep(3000);
     	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne));
     	Assert.assertEquals("Shaishav", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
    	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo));
     	Assert.assertEquals("Thangaraj", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo)).getText());
    }
    
    public void verifySingleClaimApplication(String appId) throws Exception{
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Assign Menu");
        Assert.assertEquals(appId, driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appId)).getText());
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn), "Application details button");
        Thread.sleep(10000);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments bottom Menu");
       	Thread.sleep(3000);
     	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne));
     	Assert.assertEquals("Shaishav", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
    }
    
    public void ReleaseClaimApplication(String appId) throws Exception{
     	
    	Thread.sleep(3000);
    	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Assign Menu");
        Assert.assertEquals(appId, driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appId)).getText());
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn), "Application details button");
        Thread.sleep(10000);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments bottom Menu");
       	Thread.sleep(3000);
     	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne));
     	Assert.assertEquals("Shaishav", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
     	//Release
     	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn), "Release button");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn), "Release sub button");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn), "Release final button");
        // Application is Released 
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath(".//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application is Released')]")),"Application is Released");
        
    }
    
    public void verifyUnassignApplicationStatus(String appId) throws Exception{
    
    	/*Thread.sleep(3000);
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Assign Menu");
        Assert.assertEquals(appId, driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appId)).getText());
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_searchPage_assignedResultData_appIdDetailsBtn), "Application details button");
      */  
    	
    	Thread.sleep(3000);
    	PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_leftAssignMenu), "Clicked Assigned Menu");
    	Thread.sleep(3000);
    	driver.navigate().back();
    	 PortalFuncUtils.waitForPageToLoad(driver);
    	 Thread.sleep(8000);
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomMenu), "Assignments bottom Menu");
        Thread.sleep(3000);
      	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne));
      	Assert.assertEquals("Thangaraj", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueOne)).getText());
     	PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo));
      	Assert.assertEquals("Shivani 1", driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_BottomValueTwo)).getText());

    	//Release
     	PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseBtn), "Release button");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseSubBtn), "Release sub button");
        PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn));
        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assignments_releaseFinalBtn), "Release final button");
        // Application is Released 
        assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath(".//*[@id='activityListTab']//div[contains(@class,'activity-stream')]//div/div[2][contains(.,'Application is unassigned')]")),"Application is unassigned");
    
    }
    
     
    
   
    

    private void AssignApplication(String appId, Boolean single) throws Exception {
        try {           
            if(single==true)
            {
               openActionMenu();
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu), "Assigned right side menu");
               PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role));
               Thread.sleep(3000);
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role), "Credit Manager");
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_user), "thangaraj");
               PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_notes), "Testing comments");            
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_submitButton), "Submit assign application");
            }
            else
            {
               openActionMenu();
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu), "Assigned right side menu");
               PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role));
               Thread.sleep(3000);
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role), "Credit Manager");
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_user), "thangaraj");
               PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_notes), "Testing comments");            
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_submitButton), "Submit assign application");
               Thread.sleep(8000);
               //openActionMenu();
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_rightAssignMenu), "Assigned right side menu");
               PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role));
               Thread.sleep(3000);
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_role), "Quality Assurance");
               PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_user), "shivani1");
               PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_notes), "Testing comments");            
               PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_assign_submitButton), "Submit assign application");

            }
            //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Application"+appId+" is assigned successfully')]")));
           // wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'CashFlow Updated Successfully')]")));
          //  Assert.assertEquals("Application "+appId+" assigned successfully", driver.findElement(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Application "+appId+" assigned successfully')]")).getText());
            
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
    private void updateCashFlow() throws Exception {
        try {
            openActionMenu();
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow), "Update cash flow");
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome));
            Thread.sleep(5000);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyIncome), "100000");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyInvestment), "20000");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_dailyAverageBalance), "10000");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_mothlyEMI), "60000");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_monthlyExpense), "30000");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_noOfECSbounce), "3");
            PortalFuncUtils.selectDateFromVaadinCalendar(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_salarayDate), PortalParam.salaryDate, "salaryDate");
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_maximumCreditAmount), "75000");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_updateCashFlow_updateCashFlow_cashFlowSubmit), "Submit update cash flow");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'CashFlow Updated Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'CashFlow Updated Successfully')]")));
            closeActionMenu();
        }
        catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void getFinalOffer() throws Exception {
        try {
            openActionMenu();
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_getFinalOffer));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_getFinalOffer));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_getFinalOffer), "Get Final Offer");
            Thread.sleep(4500);
            PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_submitGetFinalOffer));
            PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_submitGetFinalOffer));
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_submitGetFinalOffer), "Generate Final Offer");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Offer Generated, Application is in Manual Review.')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Offer Generated, Application is in Manual Review.')]")));
            closeActionMenu();
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Credit Review");

        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void getFindOffer2addManualOffer() throws Exception {
        try {
            openActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOfferButton)), "Add Manual Offer button is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOfferButton)), "Add Manual Offer button is not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOfferButton), "Add Manual Offer");
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown)), "Term dropdown is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown)), "Term dropdown is not clickable.");
            PortalFuncUtils.selectFromVadinDropdown(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_termDropdown), PortalParam.term);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_loanAmountTextBox), PortalParam.loanAmount);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeeTextBox), PortalParam.processingFee);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_emiTextBox), PortalParam.emi);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_interestRateTextBox), PortalParam.interestRate);
            PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_processingFeePersentTextBox), PortalParam.processingFeePercentage);
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_actions_addManualOffer_submitButton), "Submit");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//paper-toast[@id='Toast']/span[contains(text(),'Operation Completed Successfully')]")));
            closeActionMenu();
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }

    }

    private void selectFinalOffer() throws Exception {
        try {
            openActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_selectFinalOffer)), "Select final Offer button is not visible.");
            assertTrue(PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_selectFinalOffer)), "Select final Offer button is not clickable.");
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_selectFinalOffer), "Select Final Offer");
            verifyManuallySelectFinalOffer();
            PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetails_submitSelectFinalOffer), "Choosing final offer");
            PortalFuncUtils.waitForPageToLoad(driver);
            PortalFuncUtils.waitForTextToBe(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_headerStatus), "Final Offer Accepted");
            closeActionMenu();
            assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Final offer made to Final offer Accepted')]")),"Final offer accepted not found in activity history.");
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    private void verifyManuallySelectFinalOffer() throws Exception {
        try {
            new WebDriverWait(driver, 60).until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath("//*[@id='offerSelectionDiv']//tbody/tr[2]/td[3]//p")), "Months"));
            List<WebElement> options = driver.findElements(By.xpath("//*[@id='offerSelectionDiv']//tbody/tr"));
            for (int i = options.size(); i > 0; i--) {
                if (driver.findElement(By.xpath("//*[@id='offerSelectionDiv']//tbody/tr[" + i + "]/td[3]//p[contains(.,'Manual')]")).isDisplayed()) {
                    new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='offerSelectionDiv']//tbody/tr[" + i + "]/td[1]/div/label/input[@id='blankRadio1']")));
                    driver.findElement(By.xpath("//*[@id='offerSelectionDiv']//tbody/tr[" + i + "]/td[1]/div/label/input[@id='blankRadio1']")).click();
                    break;
                }
            }
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }
    
	public BackOfficeLoginPage updateFailedSubmissionApp() throws Exception {
		try {
		String DOB = PortalParam.dob+"-"+ "May" +"-"+PortalParam.yob;
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_selectDetailsApp), "Clicked failedsubmission link");
	    PortalFuncUtils.waitForPageToLoad(driver);
	    Thread.sleep(10000);
	    openActionMenu();
	    
	    // Update education details		
//	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEduDetails));
//        PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEduDetails), "Clicked update education  link");
//	    PortalFuncUtils.selectDropdownByNonDivValue(driver,PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_eduQualification),PortalParam.education);
//        PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_eduInstitution),PortalParam.institutionName);
//	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_eduSubmitBtn), "Clicked update education  button");
//		  
		// Update Employment details  
	   /* PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDetails));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDetails), "Clicked update employment  link");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpCompanyName),"SIGMA INFOSOLUTION PVT LTD");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpDesignation),PortalParam.designation);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpTotalExp),PortalParam.totalExperience);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpEmail),PortalParam.emailID);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpIncome),PortalParam.salary);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress1),PortalParam.employerAddress);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpAddress2),"Testing");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpLocation),PortalParam.empLocality);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpPincode),PortalParam.empPincode);
	    driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpPincode)).sendKeys(Keys.TAB); 
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateEmpSubmitBtn), "Clicked update employment button");
		*/
	    //  Update personal details
	    /*PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDetails), "Clicked update personal link");
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB));
		PortalFuncUtils.selectDateFromVaadinCalendar(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalDOB),"2-March-2028", "DOB");
        
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalPAN),PortalParam.panNumber);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalAadhaarNumber),PortalParam.aadharNumber);
	    //PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn));
	    //PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updatePersonalSubmitBtn), "Clicked update personal button");
	    driver.findElement(By.xpath("//*[@id='personaldetails']/div/div/form/paper-button")).click();
		
	    // Update address details
	    PortalFuncUtils.scrollToBottomOfThePage(driver);
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressDetails));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressDetails), "Clicked update address  link");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd1),PortalParam.employerAddress);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressAdd2),"Testing");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressLoc),PortalParam.empLocality);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressPincode),PortalParam.empPincode);
	    driver.findElement(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressPincode)).sendKeys(Keys.TAB); 
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressChkBox), "Clicked check box same address link");
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateAddressSubmitBtn), "Clicked update address button");
	*/	      
	    // Update income details
	    PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails));
	    PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeDetails), "Clicked update address  link");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeResidency),PortalParam.residenceType);
	    Thread.sleep(5000);
	    //PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeMonthlyRent),"15000");
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeOtherEMI),PortalParam.emiAmount);
	    PortalFuncUtils.insertText(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeCCBalance),"5000");
	    Thread.sleep(5000);
	    //PortalFuncUtils.waitForElementToBeClickable(PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn));
	    //PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_failedSubmission_updateIncomeSubmitBtn), "Clicked update income button");
	    driver.findElement(By.xpath("//*[@id='updateincomeexpense']/div/div/form/paper-button")).click();
	    
	     assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status set to Lead Created')]")),"Application status set to Lead Created");
			
	    } catch (Exception e) {
            throw new Exception(" ERROR---> xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        }
		return new BackOfficeLoginPage(driver);
	}
	
	
	public BackOfficeLoginPage updatePendinginitialOffer() throws Exception {
		try {
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDetailsApp), "Clicked pending initial offer link");
			 PortalFuncUtils.waitForPageToLoad(driver);
			 Thread.sleep(10000);
			 openActionMenu();
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectDropOffer), "Select initial offer link");
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectFirstOffer), "Select offer menu in left action link");
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingInitialoffer_selectOfferSubmitBtn), "Select initial offer submit button");
			 assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Given to Initial Offer Selected')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
			
	    } catch (Exception e) {
                 throw new Exception(" ERROR---> xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        }
		return new BackOfficeLoginPage(driver);
	 }
	
	public BackOfficeLoginPage updatePendingBankstmt() throws Exception {
		try {
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp), "Clicked pending bank statement application link");
			 PortalFuncUtils.waitForPageToLoad(driver);
			 Thread.sleep(10000);
			 //openActionMenu();
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDropUploadBank), "Select upload bank statement menu");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectFirstOffer), "Select offer menu in left action link");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectBankSubmitBtn), "Select initial offer submit button");
			 // assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Bank verification Done')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
			 assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Given to Initial Offer Selected')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
				
			
	    } catch (Exception e) {
                 throw new Exception(" ERROR---> xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
        }
		return new BackOfficeLoginPage(driver);
	 }
	public BackOfficeLoginPage updateGetFinalOffer() throws Exception {
		try {
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp), "Clicked pending bank statement application link");
			 PortalFuncUtils.waitForPageToLoad(driver);
			 Thread.sleep(10000);
			 //openActionMenu();
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDropUploadBank), "Select upload bank statement menu");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectFirstOffer), "Select offer menu in left action link");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectBankSubmitBtn), "Select initial offer submit button");
			 // assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Bank verification Done')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
			 assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Given to Initial Offer Selected')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
							
	    } catch (Exception e) {
                throw new Exception(" ERROR---> xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
       }
		return new BackOfficeLoginPage(driver);
	}
	public BackOfficeLoginPage updateManualReviewOffer() throws Exception {
		try {
			 PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDetailsApp), "Clicked pending bank statement application link");
			 PortalFuncUtils.waitForPageToLoad(driver);
			 Thread.sleep(10000);
			 //openActionMenu();
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectDropUploadBank), "Select upload bank statement menu");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectFirstOffer), "Select offer menu in left action link");
			 // PortalFuncUtils.clickButton(driver, PortalFuncUtils.getLocator(UIObjParam.backoffice_appDetailsPage_pendingBankstmt_selectBankSubmitBtn), "Select initial offer submit button");
			 // assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Selected to Bank verification Done')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
			 assertTrue(PortalFuncUtils.waitForElementToBeVisible(By.xpath("//*[@id='activityListTab']//div[contains(@class,'activity-stream')]/div[2]/*[contains(.,'Application status changed from Initial Offer Given to Initial Offer Selected')]")),"Application status changed from Initial Offer Given to Initial Offer Selected");
							
	    } catch (Exception e) {
               throw new Exception(" ERROR---> xxxxxxxxxxxxxxxxxxxxxx" + e.getMessage());
      }
		return new BackOfficeLoginPage(driver);
	}
		

}
